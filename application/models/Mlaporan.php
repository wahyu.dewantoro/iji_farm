<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mlaporan extends CI_Model
{
 
    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
 
    function getLabelserti($id=19){
        return $this->db->query("SELECT d.id_inc,nama handling,kota,COUNT(*) jumlah
                                FROM tb_juara_kontes a
                                JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc
                                JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id
                                JOIN ms_handling d ON d.id_inc=ms_handling_id
                                WHERE a.ms_kontes_id=$id AND ms_juara_id NOT IN (4,5)
                                GROUP BY handling,kota ORDER BY nama ASC")->result();
    }

    function getdatasertifikatdua(){
        return $this->db->query("SELECT d.id_inc,nama handling,kota,nm_ikan,ukuran ,CASE WHEN ms_juara_id=1 THEN f.alias WHEN ms_juara_id=2 THEN  f.alias WHEN ms_juara_id=3 THEN  f.alias ELSE  CONCAT( f.alias,' ',e.kat_ikan) END juara,gambar_ikan,
                    e.kat_ikan alias
                                FROM tb_juara_kontes a
                                JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc
                                JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id
                                JOIN ms_handling d ON d.id_inc=ms_handling_id
                                JOIN ms_kategoriikan e ON e.id_inc=b.ms_kat_id
                                JOIN ms_juara f ON f.id_inc=a.ms_juara_id
                                WHERE a.ms_kontes_id=19  AND d.nama!='Dua D Team' AND ms_juara_id NOT IN (4,5)
                                ORDER BY handling ASC")->result();
    }

    function getdatasertifikat($id,$hand){
        return $this->db->query("SELECT d.id_inc,nama handling,kota,nm_ikan, ukuran    ,
            CASE WHEN ms_juara_id=1 THEN f.alias 
        WHEN ms_juara_id=2 THEN  f.alias 
        WHEN ms_juara_id=3 THEN  f.alias 
        WHEN ms_juara_id=10 THEN f.alias
        WHEN ms_juara_id=11 THEN 
            CONCAT(nama_champion(ukuran),' ',e.alias)
        ELSE  CONCAT( f.alias,' ',e.alias) END juara
        ,gambar_ikan,
                    e.kat_ikan alias
                                FROM tb_juara_kontes a
                                JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc
                                JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id
                                JOIN ms_handling d ON d.id_inc=ms_handling_id
                                JOIN ms_kategoriikan e ON e.id_inc=b.ms_kat_id
                                JOIN ms_juara f ON f.id_inc=a.ms_juara_id
                                WHERE a.ms_kontes_id=$id  AND d.nama='$hand' AND ms_juara_id NOT IN (4,5)
                                ORDER BY handling ASC")->result();
        /*return $this->db->query("SELECT d.id_inc,nama handling,kota,nm_ikan,concat(ukuran ,' CM') ukuran,f.alias    juara,gambar_ikan,
                    e.kat_ikan alias
                                FROM tb_juara_kontes a
                                JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc
                                JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id
                                JOIN ms_handling d ON d.id_inc=ms_handling_id
                                JOIN ms_kategoriikan e ON e.id_inc=b.ms_kat_id
                                JOIN ms_juara f ON f.id_inc=a.ms_juara_id
                                WHERE a.ms_kontes_id=$id  AND d.nama!='$hand' AND ms_juara_id NOT IN (4,5)
                                ORDER BY handling ASC")->result();*/
    }

    function getBayarbyhandling($kontes){
        return $this->db->query("SELECT a.id_handling,handling,kota_handling, owner,ikan,biaya biaya_ikan
                                    FROM (
                                        SELECT b.id_inc id_handling,b.nama handling,b.kota kota_handling,COUNT(d.`tb_peserta_id`) ikan,SUM( biayabykontes(ukuran,a.ms_kontes_id)) biaya
                                            FROM tb_peserta a
                                            JOIN ms_handling b ON a.`ms_handling_id`=b.id_inc
                                            LEFT JOIN tb_ikan d ON d.tb_peserta_id=a.id_inc
                                            WHERE a.ms_kontes_id=$kontes AND checkout='1'
                                            GROUP BY handling,kota_handling ) a
                                    JOIN  (
                                        SELECT  ms_handling_id id_handling,COUNT(ms_peserta_id) owner
                                        FROM tb_peserta 
                                        GROUP BY id_handling
                                        ) b ON a.id_handling=b.id_handling
                                        ORDER BY handling ASC")->result();
    }

    function getdetailpoinByowner($kontes,$id){
        return $this->db->query("SELECT f.id_inc,f.nama,f.kota , no_ikan,nm_ikan,ukuran ,nama_juara, getpoin(ukuran,ms_juara_id) poin
                                    FROM tb_juara_kontes a
                                    JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc
                                    JOIN ms_juara c ON a.ms_juara_id=c.id_inc
                                    JOIN ms_kategoriikan d ON d.id_inc=b.ms_kat_id
                                    JOIN tb_peserta e ON e.id_inc=b.tb_peserta_id
                                    JOIN `ms_handling` f ON f.id_inc=e.ms_handling_id
                                    WHERE a.ms_kontes_id=$kontes  AND e.ms_peserta_id=$id and getpoin(ukuran,ms_juara_id) >0
                                    ORDER BY poin DESC")->result();
    }

    function getdetailpoinByhandling($kontes,$id){
        return $this->db->query("SELECT f.id_inc,f.nama,f.kota , no_ikan,nm_ikan,ukuran ,nama_juara,getpoin(ukuran,ms_juara_id) poin
                                    FROM tb_juara_kontes a
                                    JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc
                                    JOIN ms_juara c ON a.ms_juara_id=c.id_inc
                                    JOIN ms_kategoriikan d ON d.id_inc=b.ms_kat_id
                                    JOIN tb_peserta e ON e.id_inc=b.tb_peserta_id
                                    JOIN `ms_peserta` f ON f.id_inc=e.ms_peserta_id
                                    WHERE a.ms_kontes_id=$kontes  AND e.ms_handling_id=$id and getpoin(ukuran,ms_juara_id)  >0
                                    ORDER BY poin DESC")->result();
    }

    function getRekapukuran($kontes){
        return $this->db->query("SELECT range_ukuran,COUNT(ukuran) jumlah
                                    FROM tb_ikan a
                                    JOIN tb_peserta b ON a.tb_peserta_id=b.id_inc
                                    RIGHT JOIN (SELECT CONCAT(MIN,'|',MAX) iduk, IF(MIN=0,CONCAT('Up to ',MAX), CONCAT(MIN,' - ',MAX)) range_ukuran FROM ms_biayakontes WHERE ms_kontes_id=$kontes) c ON c.iduk=getRangeUkuran(ms_kontes_id,a.ukuran)
                                    WHERE checkout=1 AND ms_kontes_id=$kontes
                                    GROUP BY range_ukuran
                                    ORDER BY iduk ASC")->result();
    }

    function getRekapVariety($kontes){
        return $this->db->query("SELECT nm_ikan variety,COUNT(a.id_inc) jumlah
                                FROM tb_ikan a
                                JOIN tb_peserta b ON a.tb_peserta_id=b.id_inc
                                JOIN ms_kategoriikan c ON c.id_inc=a.ms_kat_id
                                WHERE ms_kontes_id=$kontes
                                GROUP BY nm_ikan
                                ORDER BY c.id_inc ASC")->result();
    }

    function getRekapikanhandling($kontes){
        return $this->db->query("SELECT nama,kota,COUNT(a.id_inc) jumlah
                                FROM tb_ikan a
                                JOIN tb_peserta b ON a.tb_peserta_id=b.id_inc
                                JOIN ms_handling c ON c.id_inc=b.ms_handling_id
                                WHERE b.ms_kontes_id=$kontes
                                GROUP BY nama,kota
                                ORDER BY jumlah DESC")->result();
    }

    function getRekapikanowner($kontes){
        return $this->db->query("SELECT nama,kota,COUNT(a.id_inc) jumlah
                                    FROM tb_ikan a
                                    JOIN tb_peserta b ON a.tb_peserta_id=b.id_inc
                                    JOIN ms_peserta c ON c.id_inc=b.ms_peserta_id
                                    WHERE b.ms_kontes_id=$kontes
                                    GROUP BY nama,kota
                                    ORDER BY jumlah DESC")->result();
    }
}
 