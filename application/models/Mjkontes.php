<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mjkontes extends CI_Model
{

    public $table = 'tb_juara_kontes';
    public $id = 'id_inc';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all($kontes,$kat_id='',$uk=''){
        return $this->db->query("SELECT a.id_inc ,b.id_inc id_ikan,no_ikan,gambar_ikan,getAliasBu(ukuran) ukuran,asal,gender,nm_ikan,nama_juara,kat_ikan
                                FROM tb_juara_kontes a
                                JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc
                                JOIN ms_juara c ON a.ms_juara_id=c.id_inc
                                JOIN ms_kategoriikan d ON d.id_inc=b.ms_kat_id
                                WHERE a.ms_kontes_id=$kontes 
                                ORDER BY  b.id_inc ASC, c.sort ASC")->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_inc', $q);
	$this->db->or_like('ms_kontes_id', $q);
	$this->db->or_like('tb_ikan_id', $q);
	$this->db->or_like('ms_juara_id', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_inc', $q);
	$this->db->or_like('ms_kontes_id', $q);
	$this->db->or_like('tb_ikan_id', $q);
	$this->db->or_like('ms_juara_id', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $rk=$this->db->insert($this->table, $data);
        if($rk) {
                $this->session->set_flashdata('msg', 
                        '<div class="note note-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Berhasil </h4>
                            <p>Data telah disimpan.</p>
                        </div>');                
            } else {    
                $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal disimpan.</p>
                        </div>');    
            }
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $rk=$this->db->update($this->table, $data);
        if($rk) {
                $this->session->set_flashdata('msg', 
                        '<div class="note note-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Berhasil </h4>
                            <p>Data telah disimpan.</p>
                        </div>');                
            } else {    
                $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal disimpan.</p>
                        </div>');    
            }
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $rk=$this->db->delete($this->table);
        if($rk) {
                $this->session->set_flashdata('msg', 
                        '<div class="note note-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Berhasil </h4>
                            <p>Data telah dihapus.</p>
                        </div>');                
            } else {    
                $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal dihapus.</p>
                        </div>');    
            }
    }

    function getnoikan($kontes){
        return $this->db->query("SELECT b.id_inc,no_ikan,CASE WHEN  REPLACE(getRangeUkuran(ms_kontes_id,ukuran),'|',' - ') ='0 - 15 ' THEN 'Up to 15' ELSE REPLACE(getRangeUkuran(ms_kontes_id,ukuran),'|',' - ') END range_uk,nm_ikan
                                FROM tb_peserta a
                                JOIN tb_ikan b ON a.id_inc=b.tb_peserta_id
                                JOIN ms_kategoriikan c ON c.id_inc=b.ms_kat_id
                                WHERE ms_kontes_id=$kontes AND checkout='1'
                                ORDER BY no_ikan ASC")->result();
    }

     function juarabyikan($id){
        $rk=$this->db->query("SELECT alias
                                FROM tb_juara_kontes  a
                                JOIN ms_juara c ON c.id_inc=ms_juara_id
                                WHERE tb_ikan_id=$id")->row();
        if($rk){
            echo  '<div class="ribbon"><span>'.$rk->alias.'</span></div>';
        }else{
            echo  null;
        }
    }

    function getnoikanjuara($kontes=19,$and=''){
        return $this->db->query("SELECT no_ikan,b.id_inc id_ikan
                                    FROM tb_juara_kontes a
                                    JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc
                                    JOIN ms_juara c ON c.`id_inc`=a.ms_juara_id
                                    WHERE ms_kontes_id='$kontes' 
                                    $and
                                    order by no_ikan asc ")->result();   
    }
}

/* End of file Mjkontes.php */
/* Location: ./application/models/Mjkontes.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-12-18 12:48:36 */
/* http://harviacode.com */