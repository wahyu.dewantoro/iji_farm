<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('acak'))
{
	function acak($str) {
		    $kunci = '==$#$$@#$jsdjGHFVGHSDF&*672367235HHGDVHVDyhagsvfhg^%$#@#@#&^*(*^&^&}{P{P&*^&*()(';
		    $hasil='';
		    for ($i = 0; $i < strlen($str); $i++) {
		        $karakter = substr($str, $i, 1);
		        $kuncikarakter = substr($kunci, ($i % strlen($kunci))-1, 1);
		        $karakter = chr(ord($karakter)+ord($kuncikarakter));
		        $hasil .= $karakter;
		        
		    }
		    return urlencode(base64_encode($hasil));
		}

}

if( ! function_exists('rapikan'))
{
	function rapikan($str) {
		    $str = base64_decode(urldecode($str));
		    $hasil = '';
		    $kunci = '==$#$$@#$jsdjGHFVGHSDF&*672367235HHGDVHVDyhagsvfhg^%$#@#@#&^*(*^&^&}{P{P&*^&*()(';
		    for ($i = 0; $i < strlen($str); $i++) {
		        $karakter = substr($str, $i, 1);
		        $kuncikarakter = substr($kunci, ($i % strlen($kunci))-1, 1);
		        $karakter = chr(ord($karakter)-ord($kuncikarakter));
		        $hasil .= $karakter;
		        
		    }
		    return $hasil;
		}
}

    if(!function_exists('terbilang')){
        function terbilang ($angka) {
            $angka = (float)$angka;
            $bilangan = array('','Satu','Dua','Tiga','Empat','Lima','Enam','Tujuh','Delapan','Sembilan','Sepuluh','Sebelas');
            if ($angka < 12) {
                return $bilangan[$angka];
            } else if ($angka < 20) {
                return $bilangan[$angka - 10] . ' Belas';
            } else if ($angka < 100) {
                $hasil_bagi = (int)($angka / 10);
                $hasil_mod = $angka % 10;
                return trim(sprintf('%s Puluh %s', $bilangan[$hasil_bagi], $bilangan[$hasil_mod]));
            } else if ($angka < 200) { return sprintf('Seratus %s', terbilang($angka - 100));
            } else if ($angka < 1000) { $hasil_bagi = (int)($angka / 100); $hasil_mod = $angka % 100; return trim(sprintf('%s Ratus %s', $bilangan[$hasil_bagi], terbilang($hasil_mod)));
            } else if ($angka < 2000) { return trim(sprintf('Seribu %s', terbilang($angka - 1000)));
            } else if ($angka < 1000000) { $hasil_bagi = (int)($angka / 1000); $hasil_mod = $angka % 1000; return sprintf('%s Ribu %s', terbilang($hasil_bagi), terbilang($hasil_mod));
            } else if ($angka < 1000000000) { $hasil_bagi = (int)($angka / 1000000); $hasil_mod = $angka % 1000000; return trim(sprintf('%s Juta %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
            } else if ($angka < 1000000000000) { $hasil_bagi = (int)($angka / 1000000000); $hasil_mod = fmod($angka, 1000000000); return trim(sprintf('%s Milyar %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
            } else if ($angka < 1000000000000000) { $hasil_bagi = $angka / 1000000000000; $hasil_mod = fmod($angka, 1000000000000); return trim(sprintf('%s Triliun %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
            } else {
                return 'Data Salah';
            }
        }
    }


      //Long date indo Format
    if ( ! function_exists('longdate_indo'))
    {
        function longdate_indo($tanggal)
        {
            $ubah = gmdate($tanggal, time()+60*60*8);
            $pecah = explode("-",$ubah);
            $tgl = $pecah[2];
            $bln = $pecah[1];
            $thn = $pecah[0];
            $bulan = bulan($pecah[1]);
      
            $nama = date("l", mktime(0,0,0,$bln,$tgl,$thn));
            $nama_hari = "";
            if($nama=="Sunday") {$nama_hari="Minggu";}
            else if($nama=="Monday") {$nama_hari="Senin";}
            else if($nama=="Tuesday") {$nama_hari="Selasa";}
            else if($nama=="Wednesday") {$nama_hari="Rabu";}
            else if($nama=="Thursday") {$nama_hari="Kamis";}
            else if($nama=="Friday") {$nama_hari="Jumat";}
            else if($nama=="Saturday") {$nama_hari="Sabtu";}
            return $nama_hari.','.$tgl.' '.$bulan.' '.$thn;
        }
    }

     if ( ! function_exists('bulan'))
    {
        function bulan($bln)
        {
            switch ($bln)
            {
                case 1:
                    return "Januari";
                    break;
                case 2:
                    return "Februari";
                    break;
                case 3:
                    return "Maret";
                    break;
                case 4:
                    return "April";
                    break;
                case 5:
                    return "Mei";
                    break;
                case 6:
                    return "Juni";
                    break;
                case 7:
                    return "Juli";
                    break;
                case 8:
                    return "Agustus";
                    break;
                case 9:
                    return "September";
                    break;
                case 10:
                    return "Oktober";
                    break;
                case 11:
                    return "November";
                    break;
                case 12:
                    return "Desember";
                    break;
            }
        }
    }

     if ( ! function_exists('date_indo'))
    {
        function date_indo($tgl)
        {
            if(!empty($tgl)){
                $ubah    = gmdate($tgl, time()+60*60*8);
                $pecah   = explode("-",$ubah);
                $tanggal = $pecah[2];
                $bulan   = bulan($pecah[1]);
                $tahun   = $pecah[0];
                return $tanggal.' '.$bulan.' '.$tahun;
            }else{
                return null;
            }
        }
    }