<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Refbak extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mbak');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $refbak = $this->Mbak->get_all();

        $data = array(
            'refbak_data' => $refbak
        );

        $this->template->load('blank','refbak/Refbak_list', $data);
    }

    

    public function create() 
    {
        $data = array(
            'button' => 'Form Tambah',
            'action' => site_url('refbak/create_action'),
	    'id_inc' => set_value('id_inc'),
	    'nama_bak' => set_value('nama_bak'),
	);
        $this->template->load('blank','refbak/Refbak_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_bak' => $this->input->post('nama_bak',TRUE),
	    );

           $this->Mbak->insert($data);
            
            redirect(site_url('refbak'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Mbak->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Form Edit',
                'action' => site_url('refbak/update_action'),
		'id_inc' => set_value('id_inc', $row->id_inc),
		'nama_bak' => set_value('nama_bak', $row->nama_bak),
	    );
            $this->template->load('blank','refbak/Refbak_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('refbak'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_inc', TRUE));
        } else {
            $data = array(
		'nama_bak' => $this->input->post('nama_bak',TRUE),
	    );

            $this->Mbak->update($this->input->post('id_inc', TRUE), $data);
            
            redirect(site_url('refbak'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Mbak->get_by_id($id);

        if ($row) {
            $this->Mbak->delete($id);
            
            
            redirect(site_url('refbak'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('refbak'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_bak', 'nama bak', 'trim|required');

	$this->form_validation->set_rules('id_inc', 'id_inc', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Refbak.php */
/* Location: ./application/controllers/Refbak.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-12-02 14:33:48 */
/* http://harviacode.com */