<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Pendaftaran extends CI_Controller {

     function __construct(){

        parent::__construct();

        $this->load->model('Mkatikan');

        $this->load->library('form_validation');
        $this->pengguna=$this->session->userdata('wob_pengguna');
        $this->kontes_id=$this->session->userdata('kontes_id');
        $this->handling_id=$this->session->userdata('handling_id');

    }



	function index(){

        $this->load->model('Mkontes');
           $rk=$this->Mkontes->getKontesId(19);

        // $data['kontes'] =$rk['kontes'];
                $katikan = $this->db->query("SELECT GROUP_CONCAT(a) a,GROUP_CONCAT(b) b,GROUP_CONCAT(b) b,GROUP_CONCAT(c) c,GROUP_CONCAT(d) d,GROUP_CONCAT(e) e

                                    FROM (

                                    SELECT CASE WHEN kat_ikan='A' THEN GROUP_CONCAT(nm_ikan) END A,CASE WHEN kat_ikan='B' THEN GROUP_CONCAT(nm_ikan) END B,CASE WHEN kat_ikan='C' THEN GROUP_CONCAT(nm_ikan) END C,CASE WHEN kat_ikan='D' THEN GROUP_CONCAT(nm_ikan) END D,CASE WHEN kat_ikan='E' THEN GROUP_CONCAT(nm_ikan) END E

                                    FROM ms_kategoriikan

                                    GROUP BY kat_ikan

                                    ORDER BY nm_ikan ASC

                                    ) qw")->row();

        $biaya=$this->db->query("SELECT id_inc,CASE WHEN MIN='0' THEN  CONCAT('Up to ',MAX,' cm') ELSE  CONCAT(MIN,' cm - ',MAX,' cm') END ukuran,biaya FROM ms_biayakontes WHERE ms_kontes_id=19 order by min")->result();

        $data=array(

            'title' => '',

            'ikan'  => $katikan,

            'biaya'=>$biaya,
            'kontes'=>$rk['kontes']

        );

        $this->load->view('blankdf',$data);

    }



    function handling(){

        $data=array(

            'title'=>'',

            'nama'=>set_value('nama'),

            'kota'=>set_value('kota'),

            'telp'=>set_value('telp'),

        );

        $this->template->load('blankd','regishandling',$data);   

    }



    function regishandingshow(){

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {

            $this->handling();

            die();

        }else{

          


            if($this->input->post('password',true)==$this->input->post('ulangi_password',true)){

               
                $this->db->where('telp',$this->input->post('telp',true));

                $this->db->or_where('nama',$this->input->post('nama',true));

                $this->db->select("count(1) asd");

                $rr=$this->db->get('ms_handling')->row();

                

                if($rr->asd == 1){

                 
                    $this->session->set_flashdata('msg', 

                        '<div class="alert alert-warning">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                            <p>nama/nomor Handling sudah terdaftar.</p>

                        </div>');             

                     redirect('pendaftaran/handling');

                   
                }else{

                    $this->db->trans_start();

                    $nama=$this->input->post('nama',true);

                    $kota=$this->input->post('kota',true);

                    $telp=$this->input->post('telp',true);

                    $password=$this->input->post('password',true);

                    $ulangi_password=$this->input->post('ulangi_password',true);



                    $this->db->set('nama',$nama);

                    $this->db->set('kota',$kota);

                    $this->db->set('telp',$telp);

                    $this->db->set('ms_kontes_id','19');

                    $this->db->insert('ms_handling');

                    // echo $this->db->last_query();

                    // cari id

                    $this->db->where('telp',$this->input->post('telp',true));

                    $this->db->where('nama',$this->input->post('nama',true));

                    $this->db->select("max(id_inc) id");

                    $rid=$this->db->get('ms_handling')->row();



                    $this->db->set('nama',$this->input->post('nama',true));

                    $this->db->set('username',$this->input->post('telp',true));

                    $this->db->set('ms_handling_id',$rid->id);

                    $this->db->set('ms_role_id','4');

                    $this->db->set('ms_Kontes_id','19');

                    $this->db->set('password',sha1($password));

                    $this->db->insert('ms_pengguna');
 

                    $this->db->trans_complete();



                    if ($this->db->trans_status() === FALSE)

                    {

                        $this->session->set_flashdata('msg', 

                        '<div class="alert alert-warning">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                            <p>Pendaftaran gagal, silahkan coba lagi atau hubungi custumer service di +62 856-2147-776</p>

                        </div>');                    

                    }else{

                        $this->session->set_flashdata('msg', 

                        '<div class="alert alert-success">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                            <p>Pendaftaran berhasil, silahkan login menggunakan nomor telepon yang sudah di daftar kan sebagai username.</p>

                        </div>');                    

                    }

                    redirect('auth');

                }

            }else{

                    // set_flashdata('warning','password tidak sesuai');

                 $this->session->set_flashdata('msg', 

                        '<div class="alert alert-warning">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                            <p>Password tidak sesuai.</p>

                        </div>');             

                      redirect('pendaftaran/handling');

            }

        }

    }





    public function _rules() 

    {

        $this->form_validation->set_rules('nama','nama','trim|required');

        $this->form_validation->set_rules('kota','kota','trim|required');

        $this->form_validation->set_rules('telp','telp','trim|required');



    /*$this->form_validation->set_rules('nama_bak', 'nama bak', 'trim|required');



    $this->form_validation->set_rules('id_inc', 'id_inc', 'trim');*/

    $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

    }


    //  redirect('pendaftaran/ownerbyhandling');

    function updateDataHandling(){
        $id_inc =$this->input->post('id_inc',true);
        $nama   =$this->input->post('nama',true);
        $kota   =$this->input->post('kota',true);
    
        $data=array(
            'nama'=>$nama,
            'kota'=>$kota
        );
    
        $wh=array('id_inc'=>$id_inc);
        $this->db->update('ms_handling',$data,$wh);
        redirect('pendaftaran/ownerbyhandling');

    }

    function ownerbyhandling(){
        $this->load->model('Mhandling');
        $id=$this->session->userdata('handling_id');
        $rk=$this->Mhandling->get_by_id($id);

        if($rk){
            $data['hand']  =$rk;
            $data['owner'] =$this->Mhandling->getownerbyhandling($id);
            $data['title'] ='';
            $this->template->load('blanke','pendaftaran/ownerbyhandling',$data);
        }   
    }   

      function prosesaddowner(){
        $nama=$_POST['nama'];
        $kota=$_POST['kota'];
        // cek dulu
        $kontes=$this->kontes_id;
        $rk=$this->db->query("SELECT id_inc FROM ms_peserta WHERE nama='$nama' and ms_kontes_id='$kontes' AND kota='$kota'")->row();
        $this->db->trans_start();

        if(count($rk)>0){
            // wes enek
            $ms_peserta_id=$rk->id_inc;
        }else{
            // masuk baru

            $this->db->set('nama',$nama);
            $this->db->set('kota',$kota);
            $this->db->set('pengguna_id',$this->pengguna);
            $this->db->set('ms_kontes_id',$kontes);
            $this->db->insert('ms_peserta');
            // get id
            $rn=$this->db->query("SELECT MAX(id_inc) id_inc FROM ms_peserta WHERE pengguna_id='".$this->pengguna."'")->row();
            $ms_peserta_id=$rn->id_inc;
        }  

        // entri ke handling owner
        $this->db->set('ms_handling_id',$_POST['ms_handling_id']);
        $this->db->set('ms_peserta_id',$ms_peserta_id);
        $this->db->insert('ms_handling_owner');
        $this->db->trans_complete();

       if ($this->db->trans_status() === TRUE){
                $this->session->set_flashdata('msg', 
                        '<div class="note note-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Berhasil </h4>
                            <p>Data telah disimpan.</p>
                        </div>');                
            } else {    
                $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal disimpan.</p>
                        </div>');    
            }
 
            redirect('pendaftaran/ownerbyhandling');
       
        
    }

    function daftarfromkontes($id){
       // $this->load->model('Mdaftar');
        $rk=$this->db->query("SELECT a.*,CONCAT(b.nama,' - ',b.kota) peserta, CONCAT(c.nama,' - ',c.kota) handling
                                FROM ms_handling_owner a
                                JOIN ms_peserta b ON a.ms_peserta_id=b.id_inc
                                JOIN ms_handling c ON c.id_inc=a.ms_handling_id
                                WHERE a.id_inc='$id'")->row();
        
        
        if($rk){
            $pp=$this->db->query("SELECT a.id_inc,nm_ikan nm_jenis,getAliasBu(ukuran) ukuran,ukuran ukuran_cm,gambar_ikan,asal,biayabykontes(ukuran,19) biaya,gender,nama owner
                                    FROM tb_ikan a
                                    JOIN ms_kategoriikan b ON a.ms_kat_id=b.id_inc
                                    JOIN tb_peserta c ON c.id_inc=a.tb_peserta_id 
                                    JOIN ms_peserta d ON d.`id_inc`=c.`ms_peserta_id`
                                    WHERE  checkout IS NULL AND ms_handling_id='".$rk->ms_handling_id."'
                                    AND ms_peserta_id='".$rk->ms_peserta_id."'
                                    ORDER BY a.id_inc DESC")->result();
            $data['rk']     =$rk;
            $data['ikan']   =$this->db->query("SELECT id_inc,nm_ikan,kat_ikan FROM ms_kategoriikan ORDER BY nm_ikan ASC")->result();
            $data['kat']=$this->db->query("SELECT DISTINCT kat_ikan FROM ms_kategoriikan")->result();
            $data['title']  ='';
            $data['listikan']=$pp;
           
            $this->template->load('blanke','pendaftaran/daftarfromkontes',$data);
        }
    }


   function prosessimpanikan(){
        // $_FILES['gambar_ikan']
        if(isset($_FILES['gambar_ikan']['name'])){
           
            $this->db->trans_start();
        // cek
            $ee=$this->db->query("SELECT MAX(id_inc) tb_peserta_id FROM tb_peserta WHERE ms_handling_id='".$this->input->post('ms_handling_id',TRUE)."' AND ms_peserta_id='".$this->input->post('ms_peserta_id',TRUE)."' AND ms_kontes_id ='".$this->kontes_id."' AND checkout is null ")->row();
            
            if(!empty($ee->tb_peserta_id)){
                $tb_peserta_id =$ee->tb_peserta_id;             
            }else{

             $data = array(
                'ms_kontes_id'   => $this->kontes_id,
                'ms_handling_id' => $this->input->post('ms_handling_id',TRUE),
                'ms_peserta_id'  => $this->input->post('ms_peserta_id',TRUE),
                'pengguna_id'    => $this->pengguna,
                );

                 $this->db->insert('tb_peserta',$data);
                 $rn            =$this->db->query("SELECT MAX(id_inc) tb_peserta_id FROM tb_peserta WHERE pengguna_id='". $this->pengguna."'")->row();
                 $tb_peserta_id =$rn->tb_peserta_id; 
            }

           
                $name =$_FILES['gambar_ikan']['name'];
                $tmp  =$_FILES['gambar_ikan']['tmp_name'];
                
                $exp    =explode('.',$name);
                $gambar ='ikan/'.rand().$this->pengguna.date('YmdHis').'.'.$exp[count($exp)-1];
                
                $this->db->set('tb_peserta_id',$tb_peserta_id);
                $this->db->set('ms_kat_id',$this->input->post('ms_kat_id',true));
                $this->db->set('ukuran',$this->input->post('ukuran',true));
                $this->db->set('gambar_ikan',$gambar);
                $this->db->set('gender',$this->input->post('gender',true));
                $this->db->set('asal',$this->input->post('asal',true));
                $rk=$this->db->insert('tb_ikan');
                if($rk){
                     move_uploaded_file($tmp,$gambar);
                }
           
                    

            $this->db->trans_complete();

                if ($this->db->trans_status() === true){
                  $this->session->set_flashdata('msg', 
                            '<div class="note note-success">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4>Berhasil </h4>
                                <p>Data telah disimpan.</p>
                            </div>');                
                }else{
                    $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal disimpan.</p>
                        </div>');        
                }
            } else {    
                $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Anda belum mengisi data.</p>
                        </div>');    
            }

             redirect( $_SERVER['HTTP_REFERER']);
    }

    function formeditikan($id){
        $data['rk']   =$this->db->query("SELECT * FROM tb_ikan WHERE id_inc='$id'")->row();
        $data['ikan'] =$this->db->query("SELECT id_inc,nm_ikan FROM ms_kategoriikan")->result();
        $this->load->view('pendaftaran/editikan',$data);
    }

    function proseseditikan(){
          $id_inc    =$this->input->post('id_inc',true);
          $ms_kat_id =$this->input->post('ms_kat_id',true);
          $ukuran    =$this->input->post('ukuran',true);
          $asal      =$this->input->post('asal',true);
          $gender    =$this->input->post('gender',true);


    if(!empty($_FILES['gambar_ikan']['name'])){
        $name =$_FILES['gambar_ikan']['name'];
        $tmp  =$_FILES['gambar_ikan']['tmp_name'];
        $exp    =explode('.',$name);
        $gambar ='ikan/'.date('YmdHis').'.'.$exp[count($exp)-1];
    }else{
        $gambar=$this->input->post('lawas',true);
    }

        $this->db->where('id_inc',$id_inc);
        $this->db->set('ms_kat_id',$ms_kat_id);
        $this->db->set('ukuran',$ukuran);
        $this->db->set('gambar_ikan',$gambar);
        $this->db->set('asal',$asal);
        $this->db->set('gender',$gender);
        $rk=$this->db->update('tb_ikan');

        if($rk){
            if(!empty($_FILES['gambar_ikan']['name'])){
                move_uploaded_file($tmp,$gambar);
            }
                $this->session->set_flashdata('msg', 
                        '<div class="note note-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Berhasil </h4>
                            <p>Data telah disimpan.</p>
                        </div>');                
            } else {    
                $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal disimpan.</p>
                        </div>');    
            }

          redirect( $_SERVER['HTTP_REFERER']);

    }

    function hapusikan($id){
        $rk=$this->db->query("SELECT tb_peserta_id,gambar_ikan FROM tb_ikan WHERE id_inc='$id'")->row();
        if($rk){
            $rn=$this->db->query("DELETE from tb_ikan where id_inc='$id'");
            if($rn){
                @unlink($rk->gambar_ikan);
                 $this->session->set_flashdata('msg', 
                        '<div class="note note-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Berhasil </h4>
                            <p>Data telah dihapus.</p>
                        </div>');                
            } else {    
                $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal dihapus.</p>
                        </div>');    
            }
            // redirect('daftar/listcartbydaftar/'.$rk->tb_peserta_id);            
        }
        redirect( $_SERVER['HTTP_REFERER']);
    }

    function Tagihan(){
        $this->load->model('Mdaftar');
        $hand   =$this->handling_id;
        $kontes =19;

        $pp=$this->db->query("SELECT a.id_inc,nm_ikan nm_jenis,getAliasBu(ukuran) ukuran,ukuran ukuran_cm,gambar_ikan,asal,biayabykontes(ukuran,19) biaya,gender,nama owner
                                    FROM tb_ikan a
                                    JOIN ms_kategoriikan b ON a.ms_kat_id=b.id_inc
                                    JOIN tb_peserta c ON c.id_inc=a.tb_peserta_id 
                                    JOIN ms_peserta d ON d.`id_inc`=c.`ms_peserta_id`
                                    WHERE  checkout IS NULL AND ms_handling_id='$hand'
                                    ORDER BY nama asc")->result();
        #cek status kontes
        $cc     =$this->db->query("SELECT status_kontes FROM ms_kontes WHERE id_inc='$kontes'")->row();
        $where  =" AND a.ms_handling_id=$hand";
        $st     =$cc->status_kontes;
        $daftar = $this->Mdaftar->get_all($kontes,$where);
        $data   = array(
            'daftar_data' => $daftar,
            'st'          =>$st,
            'pp'=>$pp,
            'title'       =>''
        );

        $this->template->load('blanke','pendaftaran/Daftar_list', $data);
    }

    function dataIkan(){
        $this->load->model('Mkontes');

        $kontes        =19;
        $hand          =$this->handling_id;
        $where         =" AND ms_handling_id=$hand";
        $data['hand']  =true;
        $data['title'] ='';
        $data['rk']    =$this->Mkontes->ikanbykontes($kontes,$where);
        $ss= $this->db->last_query();
        $data['label']=$this->db->query("SELECT DISTINCT pemilik from ($ss) asd")->result();
        
        $this->template->load('blanke','pendaftaran/ikanbykontes',$data);

    }
}



