<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Refhandling extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mhandling');
        $this->load->library('form_validation');
        $this->pengguna    =$this->session->userdata('wob_pengguna');
        $this->kontes_id   =19;
        $this->handling_id =$this->session->userdata('handling_id');
    }

    public function index(){
        $kontes      =$this->kontes_id;
        $refhandling = $this->Mhandling->byKontes($kontes);
        $data = array(
            'refhandling_data' => $refhandling,
            'pengguna_id'     =>$this->handling_id
        );
        $this->template->load('blank','refhandling/Refhandling_list', $data);
    }

    

    public function create() 
    {
        $data = array(
            'button' => 'Form Tambah',
            'action' => site_url('refhandling/create_action'),
            'id_inc' => set_value('id_inc'),
            'nama'   => set_value('nama'),
            'kota'   => set_value('kota'),
            'telp'   => set_value('telp'),
	);
        $this->template->load('blank','refhandling/Refhandling_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
            'nama'         => $this->input->post('nama',TRUE),
            'kota'         => $this->input->post('kota',TRUE),
            'telp'         => $this->input->post('telp',TRUE),
	    );

           $this->Mhandling->insert($data);
            
            redirect(site_url('refhandling'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Mhandling->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Form Edit',
                'action' => site_url('refhandling/update_action'),
                'id_inc' => set_value('id_inc', $row->id_inc),
                'nama'   => set_value('nama', $row->nama),
                'kota'   => set_value('kota', $row->kota),
                'telp'   => set_value('telp', $row->telp),
	    );
            $this->template->load('blank','refhandling/Refhandling_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('refhandling'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_inc', TRUE));
        } else {
            $data = array(
            		'nama' => $this->input->post('nama',TRUE),
            		'kota' => $this->input->post('kota',TRUE),
            		'telp' => $this->input->post('telp',TRUE),
            	    );

            $this->Mhandling->update($this->input->post('id_inc', TRUE), $data);
            
            redirect(site_url('refhandling'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Mhandling->get_by_id($id);

        if ($row) {
            $this->Mhandling->delete($id);
            
            
            redirect(site_url('refhandling'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('refhandling'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
	$this->form_validation->set_rules('kota', 'Kota', 'trim|required');
	$this->form_validation->set_rules('id_inc', 'id_inc', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    function detail($id){
        $rk=$this->Mhandling->get_by_id($id);
        if($rk){
            $data['hand']  =$rk;
            $data['owner'] =$this->Mhandling->getownerbyhandling($id);
            $data['uh']    =false;
            $this->template->load('blank','refhandling/detail',$data);
        }
    }


    function ownerbyhandling(){
        $id=$this->handling_id;
        $rk=$this->Mhandling->get_by_id($id);
        if($rk){
            $data['hand']  =$rk;
            $data['owner'] =$this->Mhandling->getownerbyhandling($id);
            $data['uh']    =true;
            $this->template->load('blank','refhandling/detail',$data);
        }   
    }

    function  jsonowner(){
        $kontes =$this->kontes_id;
        $rk     =$this->db->query("SELECT DISTINCT nama FROM ms_peserta  where ms_kontes_id='$kontes'")->result();
        $data   ='[';
        foreach($rk as $rk){
            $data.='"'.$rk->nama.'",';
        }
        $asd=substr_replace($data, '', -1);
        $jsontext=$asd.']';

        echo $jsontext;
    }


    function prosesaddowner(){
        $nama=$this->input->post('nama',true);
        $kota=$this->input->post('kota',true);
        $this->db->trans_start();
            // masuk baru

            $this->db->set('nama',$nama);
            $this->db->set('kota',$kota);
            $this->db->set('pengguna_id',$this->pengguna);
            $this->db->set('tgl_insert','now()',false);
            $this->db->insert('ms_peserta');
            // get id
            $rn            =$this->db->query("SELECT MAX(id_inc) id_inc FROM ms_peserta WHERE pengguna_id='".$this->pengguna."'")->row();
            $ms_peserta_id =$rn->id_inc;
        

        // entri ke handling owner
        $this->db->set('ms_handling_id',$this->input->post('ms_handling_id',true));
        $this->db->set('ms_peserta_id',$ms_peserta_id);
        $this->db->insert('ms_handling_owner');
        $this->db->trans_complete();

       if ($this->db->trans_status() === TRUE){
                $this->session->set_flashdata('msg', 
                        '<div class="note note-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Berhasil </h4>
                            <p>Data telah disimpan.</p>
                        </div>');                
            } else {    
                $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal disimpan.</p>
                        </div>');    
            }

       if(!empty($this->handling_id)){
            redirect('refhandling/ownerbyhandling');
        }else{
            redirect('refhandling/detail/'.$this->input->post('ms_handling_id'));
        }

        
    }

    function hapushandlingowner($id,$idh){
        $this->db->where('id_inc',$id);
        $rk=$this->db->delete('ms_handling_owner');
         if($rk) {
                $this->session->set_flashdata('msg', 
                        '<div class="note note-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Berhasil </h4>
                            <p>Data telah dihapus.</p>
                        </div>');                
            } else {    
                $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal dihapus.</p>
                        </div>');    
            }

            if(!empty($this->handling_id)){
             redirect('refhandling/ownerbyhandling');
            }else{
                redirect('refhandling/detail/'.$idh);
            }
    }

}

/* End of file Refhandling.php */
/* Location: ./application/controllers/Refhandling.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-12-02 15:07:58 */
/* http://harviacode.com */