<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Landing extends CI_Controller {

     function __construct(){

        parent::__construct();
    	$this->kode_role=$this->session->userdata('wob_role');
    	$this->kontes_id=$this->session->userdata('kontes_id');
    	$this->handling=$this->session->userdata('handling_id');
    	$this->pengguna=$this->session->userdata('wob_pengguna');
    	
    	// $this->load->model('Mkontes');

    }

	function index(){
		// SELECT status_kontes FROM ms_kontes
		$stk=$this->db->get('ms_kontes')->row()->status_kontes;
		$this->template->load('depan/halaman_masuk','depan/landing_page',array('stk'=>$stk));
	}
 	
 	function carapembayaran(){
 		$this->template->load('depan/halaman_masuk','depan/landing_carbayar');	
 	}

	function getOwner($handling){
		return $this->db->query("SELECT  a.*,cek_daftar(a.id_inc) jum
								FROM ms_peserta a
								JOIN ms_handling_owner b ON a.id_inc=b.ms_peserta_id
								WHERE  ms_handling_id=? and a.pengguna_id=?",array($handling,$this->pengguna))->result();
	}

	function getVariety(){
		return $this->db->query("SELECT * FROM ms_kategoriikan ORDER BY sort ASC")->result();
	}

	function owner(){
		$handling=$this->handling;
		$res=$this->getOwner($handling);
		$data=array(
			'owner'=>$res
		);
		$this->template->load('depan/halaman_masuk','depan/landing_owner',$data);	
	}

	function tambahOwner(){
		$data=array(
			'action'      =>base_url().'landing/tambahOwnerproses',
			'id_inc'      =>null,
			'nama'        =>null,
			'kota'        =>null,
			'pengguna_id' =>$this->pengguna
		);
		
		$this->template->load('depan/halaman_masuk','depan/landing_formowner',$data);
	}

	function editowner($id){
		$this->db->where('id_inc',$id);
		$rk=$this->db->get('ms_peserta')->row();
		if($rk){
			$data=array(
			'action'      =>base_url().'landing/editOwnerproses',
			'id_inc'      =>$rk->id_inc,
			'nama'        =>$rk->nama,
			'kota'        =>$rk->kota,
			'pengguna_id' =>$rk->pengguna_id
		);
		$this->template->load('depan/halaman_masuk','depan/landing_formowner',$data);
		}else{	
			redirect('landing');
		}
	}

	function editOwnerproses(){
		$nama=$this->input->post('nama',true);
		$kota=$this->input->post('kota',true);
		$id_inc=$this->input->post('id_inc',true);

		$this->db->trans_start();			
			$this->db->set('nama',$nama);
			$this->db->set('kota',$kota);
			$this->db->where('id_inc',$id_inc);
			$res=$this->db->update('ms_peserta');
		$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
			        // generate an error... or use the log_message() function to log your error
				echo '<script type="text/javascript">'; 
				echo 'alert("Edit owner gagal!");'; 
				echo 'window.location.href = "'.base_url('landing/tambahOwner').'";';
				echo '</script>';
			}else{
				echo '<script type="text/javascript">'; 
				echo 'alert("Edit owner berhasil!");'; 
				echo 'window.location.href = "'.base_url('landing/owner').'";';
				echo '</script>';
			}


	}

	function tambahOwnerproses(){
		/*echo "<pre>";
		print_r($this->input->post());*/
			$nama=$this->input->post('nama',true);
			$kota=$this->input->post('kota',true);
			$pengguna_id=$this->input->post('pengguna_id',true);

			$this->db->trans_start();

			$this->db->set('nama',$nama);
			$this->db->set('kota',$kota);
			$this->db->set('pengguna_id',$pengguna_id);
			$this->db->insert('ms_peserta');

			// SELECT MAX(id_inc) id FROM ms_peserta WHERE pengguna_id=15
			$this->db->where('pengguna_id',$pengguna_id);
			$this->db->select('MAX(id_inc) id',false);
			$peserta_id=$this->db->get('ms_peserta')->row()->id;

			$this->db->set('ms_handling_id',$this->handling);
			$this->db->set('ms_peserta_id',$peserta_id);
			$this->db->insert('ms_handling_owner');


			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
			        // generate an error... or use the log_message() function to log your error
				echo '<script type="text/javascript">'; 
				echo 'alert("Tambah owner gagal!");'; 
				echo 'window.location.href = "'.base_url('landing/tambahOwner').'";';
				echo '</script>';
			}else{
				echo '<script type="text/javascript">'; 
				echo 'alert("Tambah owner berhasil!");'; 
				echo 'window.location.href = "'.base_url('landing/owner').'";';
				echo '</script>';
			}
	}


	function hapusowner($ide){
		$id=urldecode($ide);
		$this->db->where('id_inc',$id);
		$res=$this->db->delete('ms_peserta');
		if($res){
			echo '<script type="text/javascript">'; 
				echo 'alert("Hapus owner berhasil!");'; 
				echo 'window.location.href = "'.base_url('landing/owner').'";';
				echo '</script>';
		}else{
				echo '<script type="text/javascript">'; 
				echo 'alert("Hapus owner gagal!");'; 
				echo 'window.location.href = "'.base_url('landing/owner').'";';
				echo '</script>';
		}

		
	}



	function regitrasi(){
		$handling =$this->handling;
		$pengguna =$this->pengguna;
		$res      =$this->getOwner($handling);
		$ikan     =$this->db->query("SELECT b.*,getVariety(ms_kat_id) variety,getOwner(ms_peserta_id) owner
									FROM tb_peserta a
									JOIN tb_ikan b ON a.id_inc=b.tb_peserta_id
									WHERE pengguna_id=$pengguna
									AND ms_handling_id=$handling  AND checkout IS NULL")->result();
		$data=array(
			'owner'    =>$res,
			'action'   =>base_url().'landing/prosesdaftar',
			'variety'  =>$this->getVariety(),
			'ikan'     =>$ikan,
			'handling' =>$handling
		);

		$this->template->load('depan/halaman_masuk','depan/landing_formregistrasi',$data);
	}

	function prosesdaftar(){
	
		$handling=$this->handling;
		$pengguna=$this->pengguna;
		$ms_peserta_id=$this->input->post('ms_peserta_id',true);
		$this->db->trans_start();

			// cek 
			$cek=$this->db->query("SELECT max(id_inc)  id_inc FROM tb_peserta WHERE pengguna_id=? AND ms_handling_id=? AND ms_peserta_id=? AND checkout IS NULL",array($pengguna,$handling,$ms_peserta_id))->row();


			if(!empty($cek->id_inc)){
				$tb_peserta_id=$cek->id_inc;
			}else{
				
				// insert baru
				$this->db->set('ms_handling_id',$handling);
				$this->db->set('ms_peserta_id',$ms_peserta_id);
				$this->db->set('pengguna_id',$pengguna);
				$this->db->insert('tb_peserta');

				$cekb=$this->db->query("SELECT MAX(id_inc)  tb_peserta_id FROM tb_peserta WHERE pengguna_id=? AND ms_handling_id=? AND ms_peserta_id=? AND checkout IS NULL",array($pengguna,$handling,$ms_peserta_id))->row();
				$tb_peserta_id=$cekb->tb_peserta_id;
			}

			
			$config['upload_path']   = 'ikan/';
			$config['allowed_types'] = 'jpg|png';
			$config['encrypt_name']  =TRUE;

			$this->load->library('upload', $config);
            if ($this->upload->do_upload('gambar'))
            {
            	$res= $this->upload->data();
            	$path=$config['upload_path'].$res['file_name'];
				$this->db->set('tb_peserta_id',$tb_peserta_id );
				$this->db->set('ms_kat_id', $this->input->post('ms_kat_id'));
				$this->db->set('ukuran',$this->input->post('ukuran') );
				$this->db->set('gambar_ikan',$path );
				$this->db->set('gender',$this->input->post('gender') );
				$this->db->set('asal',$this->input->post('asal') );
				$this->db->insert('tb_ikan');
            }else{
						echo '<script type="text/javascript">'; 
						echo 'alert("Tambah ikan gagal!");'; 
						echo 'window.location.href = "'.base_url('landing/regitrasi').'";';
						echo '</script>';
            }
            
         $this->db->trans_complete();

		if ($this->db->trans_status() ==1)
		{
		        echo '<script type="text/javascript">'; 
				echo 'alert("Tambah ikan berhasil !");'; 
				echo 'window.location.href = "'.base_url('landing/regitrasi').'";';
				echo '</script>';
		}else{
			// generate an error... or use the log_message() function to log your error
				echo '<script type="text/javascript">'; 
				echo 'alert("Tambah ikan gagal!");'; 
				echo 'window.location.href = "'.base_url('landing/regitrasi').'";';
				echo '</script>';				
		}
	}

	function hapusikanhandling($id){
		$this->db->trans_start();
		$sd=$this->db->query("SELECT * FROM tb_ikan WHERE id_inc=$id")->row();

		$this->db->where('id_inc',$id);
		$this->db->delete('tb_ikan');



		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
		        // generate an error... or use the log_message() function to log your error
				echo '<script type="text/javascript">'; 
				echo 'alert("hapus ikan gagal!");'; 
				echo 'window.location.href = "'.base_url('landing/regitrasi').'";';
				echo '</script>';
		}else{

			@unlink($sd->gambar_ikan);

				echo '<script type="text/javascript">'; 
				echo 'alert("hapus ikan berhasil !");'; 
				echo 'window.location.href = "'.base_url('landing/regitrasi').'";';
				echo '</script>';
		}
	}


	function selesaicart($id){
		$handling=$this->handling;
		$pengguna=$this->pengguna;
		 // $kontes=$this->kontes_id;
        $this->db->trans_start();
        
        // update peserta
            // $this->db->set('no_kwitansi','ambilnokwitansi()',false);
            $this->db->set('checkout',1);
            $this->db->where('ms_handling_id',$handling);
            $this->db->where('pengguna_id',$pengguna);
            $this->db->update('tb_peserta');

            $this->db->set('no_ikan','ambilnoikan()',false);
            $this->db->where(" tb_peserta_id IN (
								SELECT id_inc 
								FROM tb_peserta 
								WHERE ms_handling_id=$handling  and pengguna_id=$pengguna)",'',false);
            $this->db->update('tb_ikan');            



        $this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){
  				echo '<script type="text/javascript">'; 
				echo 'alert("checkout ikan gagal!");'; 
				echo 'window.location.href = "'.base_url('landing/regitrasi').'";';
				echo '</script>';
            } else {    
            	echo '<script type="text/javascript">'; 
				echo 'alert("checkout ikan berhasil!");'; 
				echo 'window.location.href = "'.base_url('landing/regitrasi').'";';
				echo '</script>';
            }
	}

	function dataikan(){
		$handling =$this->handling;
		$pengguna =$this->pengguna;
		$ikan     =$this->db->query("SELECT b.*,getVariety(ms_kat_id) variety,getOwner(ms_peserta_id) owner
									FROM tb_peserta a
									JOIN tb_ikan b ON a.id_inc=b.tb_peserta_id
									WHERE pengguna_id=$pengguna
									AND ms_handling_id=$handling  AND checkout=1")->result();
		$data=array('ikan'=>$ikan);
		$this->template->load('depan/halaman_masuk','depan/landing_dataikan',$data);
	}

	function tagihan(){
		$handling =$this->handling;
		$pengguna =$this->pengguna;

		$lunas=$this->db->query("SELECT b.*,getVariety(ms_kat_id) variety,getOwner(ms_peserta_id) owner,biayabykontes(ukuran) biaya,kd_bayar
								FROM tb_peserta a
								JOIN tb_ikan b ON a.id_inc=b.tb_peserta_id
								WHERE pengguna_id=$pengguna
								AND ms_handling_id=$handling  AND checkout=1  AND kd_bayar=1
								ORDER BY owner ASC")->result();
		$grg=$this->db->query("SELECT no_pendaftaran,d.nama nama_handling,d.kota kota_handling,c.nama nama_owner,c.kota kota_owner,COUNT(1) ikan,SUM(biayabykontes(ukuran)) jumlah,get_status_entri(a.pengguna_id) st_entri
                FROM tb_peserta a
                JOIN tb_ikan b ON a.id_inc=b.tb_peserta_id
                JOIN ms_peserta c ON c.id_inc=a.ms_peserta_id
                JOIN ms_handling d ON d.id_inc=a.ms_handling_id
                WHERE kd_bayar IS NULL AND checkout=1 AND ( kd_bayar IS NULL OR kd_bayar=0) AND  a.pengguna_id=$pengguna
                GROUP BY no_pendaftaran,ms_handling_id,ms_peserta_id
                ORDER BY a.id_inc DESC")->result();

		$data=array(
			'lunas'=>$lunas,
			'grg'=>$grg
		);
		$this->template->load('depan/halaman_masuk','depan/landing_tagihan',$data);
	}

	function detailBAyar($no_pendaftaran){
        $this->load->model('Mdaftar');
        $pp=$this->db->query("SELECT c.id_inc id_handling,d.id_inc id_peserta,a.id_inc,nama_kontes,c.nama handling,c.kota kota_handling,d.nama pemilik_ikan,d.kota kota_pemilik,DATE_FORMAT(a.tanggal_insert,'%d %M %Y, %H:%i:%s') tanggal_daftar,getNamaPengguna(a.pengguna_id) nama_daftar,no_pendaftaran kode,checkout,ms_handling_id
                                    FROM tb_peserta a
                                    JOIN ms_kontes b ON a.ms_kontes_id=b.id_inc
                                    JOIN ms_handling c ON c.id_inc=a.ms_handling_id
                                    join ms_peserta d on d.id_inc=a.ms_peserta_id
                                    WHERE a.no_pendaftaran=?",array($no_pendaftaran))->row();
        if($pp){
            $data=array(
                'rp'       =>$pp,
                'ikan'     =>$this->db->query("SELECT id_inc,nm_ikan FROM ms_kategoriikan order by sort asc")->result(),
                'listikan' =>$this->Mdaftar->listikanbypeserta($pp->id_inc),
                );
            $this->template->load('depan/halaman_masuk','depan/landing_tagihan_detail',$data);
        }else{
            redirect('landing');
        }
    }
}



