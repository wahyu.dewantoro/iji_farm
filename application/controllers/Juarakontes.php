<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Juarakontes extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
           $this->load->model('Mjkontes');
           $this->load->library('form_validation');
           $this->kontes_id   =$this->session->userdata('kontes_id');
           $this->pengguna_id =$this->session->userdata('wob_pengguna');
    }

    public function index()
    {
        $juarakontes = $this->Mjkontes->get_all($this->kontes_id);

        $data = array(
            'juarakontes_data' => $juarakontes
        );

        $this->template->load('blank','juarakontes/Juarakontes_list', $data);
    }

    

    public function create() 
    {
        $data = array(
            'button'       => 'Manual Judging',
            'action'       => site_url('juarakontes/create_action'),
            'id_inc'       => set_value('id_inc'),
            'ms_kontes_id' => set_value('ms_kontes_id',$this->kontes_id),
            'tb_ikan_id'   => set_value('tb_ikan_id'),
            'ms_juara_id'  => set_value('ms_juara_id'),
            'no_ikan'      => $this->Mjkontes->getnoikan($this->kontes_id),
            'juara'        => $this->db->query("SELECT id_inc,nama_juara FROM ms_juara ")->result()
	);
        /*echo "<pre>";
        print_r($data['no_ikan']);
        echo "</pre>";*/
        $this->template->load('blank','juarakontes/Juarakontes_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'ms_kontes_id' => $this->input->post('ms_kontes_id',TRUE),
		'tb_ikan_id' => $this->input->post('tb_ikan_id',TRUE),
		'ms_juara_id' => $this->input->post('ms_juara_id',TRUE),
	    );

           $this->Mjkontes->insert($data);
            
            redirect(site_url('juarakontes/create'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Mjkontes->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Form Edit',
                'action' => site_url('juarakontes/update_action'),
		'id_inc' => set_value('id_inc', $row->id_inc),
		'ms_kontes_id' => set_value('ms_kontes_id', $row->ms_kontes_id),
		'tb_ikan_id' => set_value('tb_ikan_id', $row->tb_ikan_id),
		'ms_juara_id' => set_value('ms_juara_id', $row->ms_juara_id),
	    );
            $this->template->load('blank','juarakontes/Juarakontes_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('juarakontes'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_inc', TRUE));
        } else {
            $data = array(
		'ms_kontes_id' => $this->input->post('ms_kontes_id',TRUE),
		'tb_ikan_id' => $this->input->post('tb_ikan_id',TRUE),
		'ms_juara_id' => $this->input->post('ms_juara_id',TRUE),
	    );

            $this->Mjkontes->update($this->input->post('id_inc', TRUE), $data);
            
            redirect(site_url('juarakontes'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Mjkontes->get_by_id($id);

        if ($row) {
            $this->Mjkontes->delete($id);
            
            
            redirect(site_url('juarakontes'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('juarakontes'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('ms_kontes_id', 'ms kontes id', 'trim|required');
	$this->form_validation->set_rules('tb_ikan_id', 'tb ikan id', 'trim|required');
	$this->form_validation->set_rules('ms_juara_id', 'ms juara id', 'trim|required');

	$this->form_validation->set_rules('id_inc', 'id_inc', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }


    function priviewikan(){
        $id= $_POST['id'];
        $data['rk']=$this->db->query("SELECT gambar_ikan,gender,asal, CONCAT(ukuran,' cm') ukuran ,nm_ikan
                                        FROM tb_ikan  a
                                        JOIN ms_kategoriikan b ON b.`id_inc`=a.ms_kat_id
                                        WHERE a.id_inc=$id")->row();
        if(count($data['rk'])>0){
            $this->load->view('juarakontes/preview',$data);
        }else{
            echo "";
        }
        
    }

    function noikan(){
        $this->load->model('Mkontes');
        $kontes_id=$this->kontes_id;

        // $role   =$this->role;
        $where="";
        $data['cariikan']=$this->Mkontes->ikanbykontes($kontes_id,$where);


        // ukuran 
        $data['ukuran']=$this->db->query("SELECT MIN,MAX FROM ms_biayakontes WHERE ms_kontes_id=$kontes_id")->result();
        $data['jenis']=$this->db->query("SELECT * FROM ms_kategoriikan order by sort asc")->result();

        if(isset($_POST['cek'])){
            $uku=$_POST['ukuran'];
            $kat=$_POST['ms_kat_id'];

            $data['vk']=$uku;
            $data['vj']=$kat;

            $exp=explode('_',$uku);
            $min=$exp[0];
            $max=$exp[1];
            $data['ikan']=$this->db->query("SELECT a.id_inc,no_ikan,nm_ikan,gambar_ikan,gender,asal ,ukuran
                                            FROM tb_ikan a
                                            JOIN tb_peserta b ON a.tb_peserta_id=b.id_inc
                                            JOIN ms_kategoriikan c ON c.id_inc=a.ms_kat_id
                                            WHERE ms_kontes_id=$kontes_id AND ms_kat_id='$kat' and checkout='1' AND ukuran BETWEEN '$min' AND '$max' order by  no_ikan asc")->result();
            // 
            $ars=array(
                'vmin'=>$min,
                'vmax'=>$max,
                'vikan'=>$kat
                );

            $this->session->set_userdata($ars);
        }else if(isset($_POST['reset'])){
            $uku=$_POST['ukuran'];
            $kat=$_POST['ms_kat_id'];

            $data['vk']=$uku;
            $data['vj']=$kat;

            $exp=explode('_',$uku);
            $min=$exp[0];
            $max=$exp[1];
            $data['ikan']=$this->db->query("SELECT a.id_inc,no_ikan,nm_ikan,gambar_ikan,gender,asal ,ukuran
                                            FROM tb_ikan a
                                            JOIN tb_peserta b ON a.tb_peserta_id=b.id_inc
                                            JOIN ms_kategoriikan c ON c.id_inc=a.ms_kat_id
                                            WHERE ms_kontes_id=$kontes_id AND ms_kat_id='$kat' and checkout='1' AND ukuran BETWEEN '$min' AND '$max' ")->result();
            // 
            $ars=array(
                'vmin'=>$min,
                'vmax'=>$max,
                'vikan'=>$kat
                );

            $this->session->set_userdata($ars);

            $this->db->query("DELETE FROM tb_juara_kontes WHERE tb_ikan_id IN ( select * from (
                                SELECT tb_ikan_id
                                FROM tb_juara_kontes a
                                JOIN tb_ikan b ON a.`tb_ikan_id`=b.id_inc
                                WHERE ms_kontes_id=".$this->kontes_id." AND ms_kat_id=$kat AND ukuran BETWEEN '$min' AND '$max' and ms_juara_id in (1,2,3,4,5) ) asd ) ");
        }else{
            $ars=array(
                'vmin'  =>null,
                'vmax'  =>null,
                'vikan' =>null,
                );

            $this->session->set_userdata($ars);
            $data['vk']=null;
            $data['vj']=null;
        }

        
        $this->template->load('blank','juarakontes/penjurianbykategori',$data);
    }

    function aksipenjurianbykategori(){
          $id      =$this->input->post('id');
          // die();
          $kontes  =19;
          $katikan =$this->session->userdata('vikan');  
          $min     =$this->session->userdata('vmin');
          $max     =$this->session->userdata('vmax');
          
     // cek slot juara
        $cc=$this->db->query("SELECT min(id_inc)  ms_juara_id,alias FROM (
                                SELECT id_inc,alias
                                FROM ms_juara WHERE id_inc IN (1,2,3,4,5)
                                ) asd WHERE id_inc 
                                 NOT IN ( 
                                SELECT ms_juara_id
                                FROM tb_juara_kontes a
                                JOIN tb_ikan b ON a.`tb_ikan_id`=b.id_inc
                                WHERE ms_kontes_id=$kontes AND ms_kat_id=$katikan AND ukuran BETWEEN '$min' AND '$max')")->row();
      // echo $this->db->last_query();
         $rr=$this->db->query("SELECT alias
                                    FROM tb_juara_kontes  a
                                    JOIN ms_juara c ON c.id_inc=ms_juara_id
                                    WHERE tb_ikan_id=$id")->row();

         
         // exit;
        if($cc->ms_juara_id!=''){
            // cek id ikan s
            if(count($rr)==0 ){
                // grg enek & insert
                $this->db->query("INSERT INTO tb_juara_kontes (ms_kontes_id,tb_ikan_id,ms_juara_id)
                                  VALUE ('$kontes','$id','".$cc->ms_juara_id."')");
                echo '<div class="ribbon"><span>'.$cc->alias.'</span></div>';
            }else{
                // wes enek
                echo '<div class="ribbon"><span>'.$rr->alias.'</span></div>';
            }

        }else{
            /*echo "SELECT alias
                                    FROM tb_juara_kontes  a
                                    JOIN ms_juara c ON c.id_inc=ms_juara_id
                                    WHERE tb_ikan_id=$id";
                                    exit;*/
            if(count($rr)>0 ){
                echo '<div class="ribbon"><span>'.$rr->alias.'</span></div>';
            }else{
                echo "";
            }
        }
    }


    function champion(){
         $data = array(
            'button'       => 'Pemilihan BIS',
            'action'       => site_url('juarakontes/prosesupjuara'),
            'ms_kontes_id' => set_value('ms_kontes_id',$this->kontes_id),
            'tb_ikan_id'   => set_value('tb_ikan_id'),
            'ms_juara_id'  => set_value('ms_juara_id'),
            'no_ikan'      => $this->Mjkontes->getnoikanjuara($this->kontes_id,'AND c.`id_inc`=1'),
            'juara'        => $this->db->query("SELECT id_inc,CONCAT(upper(kode_juara),' - ', nama_juara) nama_juara FROM ms_juara WHERE id_inc NOT IN (1,2,3,4,5)")->result()
            );
         $this->template->load('blank','juarakontes/formchampion',$data);
    }


    function chosechampion(){
        $data = array(
            'button'       => 'Pemilihan champion',
            'action'       => site_url('juarakontes/proseschampion'),
            'ms_kontes_id' => set_value('ms_kontes_id',$this->kontes_id),
            'tb_ikan_id'   => set_value('tb_ikan_id'),
            'ms_juara_id'  => set_value('ms_juara_id'),
            'no_ikan'      => $this->Mjkontes->getnoikanjuara($this->kontes_id,'AND c.`id_inc`=6'),
            'juara'        => $this->db->query("SELECT id_inc,CONCAT(UPPER(kode_juara),' - ', nama_juara) nama_juara FROM ms_juara WHERE id_inc NOT IN (1,2,3,4,5,6,9)")->result()
            );

        
         $this->template->load('blank','juarakontes/formchampion',$data);   
    }

    function proseschampion(){
          $tb_ikan_id  =$this->input->post('tb_ikan_id');
        $ms_juara_id =$this->input->post('ms_juara_id');
        $kontes      =19;

        $this->db->trans_start();
        #get data ikan
        $rk=$this->db->query("SELECT  getRangeUkuran($kontes,ukuran) expukuran,ms_kat_id,ms_juara_id
                                FROM tb_ikan  a
                                JOIN tb_juara_kontes b ON a.id_inc=b.tb_ikan_id
                                WHERE a.id_inc=?",array($tb_ikan_id))->row();
        $r12345=array(1,2,3,4,5,6);

        #cek juara di bawahnya
        
        
        switch ($rk->ms_juara_id) {
            case '6':
                # code...
            $inarray="(1,2,3,4,5)";
                break;

            case '1':
                # code...
                $inarray="(2,3,4,5)";
                break;
            case '2':
                # code...
                $inarray="(3,4,5)";
                break;
            case '3':
                # code...
                $inarray="(4,5)";
                break;
            case '4':
                # code...
                $inarray="(5)";
                break;
            default:
                # code...
            $inarray='';
                break;
        }

       /* echo $inarray;
        exit;*/
        $kat =$rk->ms_kat_id;
        $exp =explode('|',$rk->expukuran);
        $min =$exp[0];
        $max =$exp[1];

        #update pertama
        $this->db->query("UPDATE tb_juara_kontes SET ms_juara_id=$ms_juara_id WHERE tb_ikan_id=$tb_ikan_id");

            if(in_array($rk->ms_juara_id,$r12345)){
            #update kedua
            $this->db->query("UPDATE tb_juara_kontes a
                            JOIN tb_ikan b ON a.`tb_ikan_id`=b.id_inc
                            JOIN ms_juara c ON c.id_inc=a.ms_juara_id
                            SET ms_juara_id=kode_up
                            WHERE ms_kontes_id=$kontes AND ms_kat_id=$kat AND ukuran BETWEEN '$min' AND '$max' AND tb_ikan_id!=$tb_ikan_id AND ms_juara_id IN $inarray");
            }
        $this->db->trans_complete();
         if($this->db->trans_status() === TRUE) {
                $this->session->set_flashdata('msg', 
                        '<div class="note note-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Berhasil </h4>
                            <p>Data telah diupdate.</p>
                        </div>');                
            } else {    
                $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal diupdate.</p>
                        </div>');    
            }
        redirect('juarakontes/chosechampion');
    }

    function prosesupjuara(){
        
        $tb_ikan_id  =$this->input->post('tb_ikan_id');
        $ms_juara_id =$this->input->post('ms_juara_id');
        $kontes      =19;

        $this->db->trans_start();
        #get data ikan
        $rk=$this->db->query("SELECT  getRangeUkuran($kontes,ukuran) expukuran,ms_kat_id,ms_juara_id
                                FROM tb_ikan  a
                                JOIN tb_juara_kontes b ON a.id_inc=b.tb_ikan_id
                                WHERE a.id_inc=?",array($tb_ikan_id))->row();
        $r12345=array(1,2,3,4,5);
        #cek juara di bawahnya
/*        
        echo $rk->ms_juara_id;
        die();*/
        switch ($rk->ms_juara_id) {
            case '1':
                # code...
                $inarray="(2,3,4,5)";
                break;
            case '2':
                # code...
                $inarray="(3,4,5)";
                break;
            case '3':
                # code...
                $inarray="(4,5)";
                break;
            case '4':
                # code...
                $inarray="(5)";
                break;
            default:
                # code...
            $inarray='';
                break;
        }

       /* echo $inarray;
        exit;*/
        $kat =$rk->ms_kat_id;
        $exp =explode('|',$rk->expukuran);
        $min =$exp[0];
        $max =$exp[1];

        #update pertama
        $this->db->query("UPDATE tb_juara_kontes SET ms_juara_id=$ms_juara_id WHERE tb_ikan_id=$tb_ikan_id");

            if(in_array($rk->ms_juara_id,$r12345)){
            #update kedua
            $this->db->query("UPDATE tb_juara_kontes a
                            JOIN tb_ikan b ON a.`tb_ikan_id`=b.id_inc
                            JOIN ms_juara c ON c.id_inc=a.ms_juara_id
                            SET ms_juara_id=kode_up
                            WHERE ms_kontes_id=$kontes AND ms_kat_id=$kat AND ukuran BETWEEN '$min' AND '$max' AND tb_ikan_id!=$tb_ikan_id AND ms_juara_id IN $inarray");
            }
        $this->db->trans_complete();
         if($this->db->trans_status() === TRUE) {
                $this->session->set_flashdata('msg', 
                        '<div class="note note-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Berhasil </h4>
                            <p>Data telah diupdate.</p>
                        </div>');                
            } else {    
                $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal diupdate.</p>
                        </div>');    
            }
        redirect('juarakontes/champion');
    }

    function priviewikanjuara(){
        $idj=$_POST['id'];
        $kontes=$this->kontes_id;
        $rk=$this->db->query("SELECT no_ikan,gambar_ikan,ukuran,asal,gender,nm_ikan,nama_juara,kat_ikan
                                FROM tb_juara_kontes a
                                JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc
                                JOIN ms_juara c ON a.ms_juara_id=c.id_inc
                                JOIN ms_kategoriikan d ON d.id_inc=b.ms_kat_id
                                WHERE a.ms_kontes_id=$kontes AND b.id_inc='$idj'")->row();
        if($rk){
            $this->load->view('juarakontes/previewjuara',array('rk'=>$rk));
        }else{
            echo "";
        }
    }

    function cariikan(){
        $this->load->model('Mkontes');
        $id= $_POST['id'];
        $data['idikan']=$id;
        $data['rk']=$this->Mkontes->dataIkanById($id);
        // print_r($data);
        // exit;
        $this->load->view('juarakontes/viewikandetail',$data);
    }

     function editikan($id){
        $data['rk']   =$this->db->query("SELECT * FROM tb_ikan WHERE id_inc='$id'")->row();
        $data['ikan'] =$this->db->query("SELECT id_inc,nm_ikan FROM ms_kategoriikan")->result();
        $this->load->view('juarakontes/editikan',$data);
    }

     function proseseditikan(){
        $id_inc          =$_POST['id_inc'];
        $ms_kat_id =$_POST['ms_kat_id'];
        $ukuran          =$_POST['ukuran'];
        $asal            =$_POST['asal'];
        $gender          =$_POST['gender'];

    if(!empty($_FILES['gambar_ikan']['name'])){
        $name =$_FILES['gambar_ikan']['name'];
        $tmp  =$_FILES['gambar_ikan']['tmp_name'];
        $exp    =explode('.',$name);
        $gambar ='ikan/'.date('YmdHis').'.'.$exp[count($exp)-1];
    }else{
        $gambar=$_POST['lawas'];
    }

        $this->db->where('id_inc',$id_inc);
        $this->db->set('ms_kat_id',$ms_kat_id);
        $this->db->set('ukuran',$ukuran);
        $this->db->set('gambar_ikan',$gambar);
        $this->db->set('asal',$asal);
        $this->db->set('gender',$gender);
        $rk=$this->db->update('tb_ikan');

        if($rk){
            if(!empty($_FILES['gambar_ikan']['name'])){
                move_uploaded_file($tmp,$gambar);
            }
                $this->session->set_flashdata('msg', 
                        '<div class="note note-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Berhasil </h4>
                            <p>Data telah disimpan.</p>
                        </div>');                
            } else {    
                $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal disimpan.</p>
                        </div>');    
            }

            redirect('juarakontes/noikan');
    }


    function pilihbiv(){
        $kontes =$this->kontes_id;
        $ikan   =$this->db->query("SELECT DISTINCT a.tb_ikan_id id,no_ikan,nm_jenis,getAliasBu(ukuran) ukuran
                                FROM tb_juara_kontes a
                                JOIN tb_ikan b ON a.`tb_ikan_id`=b.`id_inc`
                                JOIN ms_jenisikan c ON c.`id_inc`=b.`ms_kat_id`
                                WHERE ms_kontes_id=$kontes AND a.tb_ikan_id NOT IN (SELECT tb_ikan_id FROM tb_pilih_biv)")->result();
        $biv=$this->db->query("select * from tb_pilih_biv")->result();

          $data = array(
            'button'  => 'Pemilihan BIV',
            'action'  => site_url('juarakontes/saveBiv'),
            'no_ikan' => $ikan,
            'biv'     => $biv
            );

        $this->template->load('blank','juarakontes/vjuara_biv', $data);
    }


    function saveBiv(){
        $tb_ikan_id=$this->input->post('tb_ikan_id');
        $kontes =$this->kontes_id;
        // proses
        $this->db->query("INSERT INTO tb_pilih_biv 
                            SELECT DISTINCT a.tb_ikan_id ,no_ikan,nm_jenis,getAliasBu(ukuran) ukuran,gambar_ikan,gender,asal
                            FROM tb_juara_kontes a
                            JOIN tb_ikan b ON a.`tb_ikan_id`=b.`id_inc`
                            JOIN ms_jenisikan c ON c.`id_inc`=b.`ms_kat_id`
                            WHERE ms_kontes_id=$kontes AND tb_ikan_id=$tb_ikan_id");
        redirect('juarakontes/pilihbiv');
    }

    function simpanBiv(){
        $tb_ikan_id=$this->input->get("tb_ikan_id");
        $kontes =$this->kontes_id;
        $data=array(
            'tb_ikan_id'   =>$tb_ikan_id,
            'ms_juara_id'  =>30,
            'ms_kontes_id' =>$kontes
        );

        $this->db->insert('tb_juara_kontes',$data);
        $this->db->empty_table('tb_pilih_biv');
        redirect('juarakontes/pilihbiv');
    }

    function resetBiv(){
        $this->db->empty_table('tb_pilih_biv');
        redirect('juarakontes/pilihbiv');   
    }

    function tablejuara(){
        $ukuran=$this->db->query("SELECT min,max FROM ms_biayakontes")->result();
        $data=array('ukuran'=>$ukuran);
        $this->template->load('blank','juarakontes/table_juara', $data);
    }

}

/* End of file Juarakontes.php */
/* Location: ./application/controllers/Juarakontes.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-12-18 12:48:36 */
/* http://harviacode.com */