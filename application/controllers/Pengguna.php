<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengguna extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mpengguna');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $pengguna = $this->Mpengguna->get_all();

        $data = array(
            'pengguna_data' => $pengguna
        );

        $this->template->load('blank','pengguna/Pengguna_list', $data);
    }

    

    public function create() 
    {
        $data = array(
            'button'       => 'Form Tambah',
            'action'       => site_url('pengguna/create_action'),
            'id_inc'       => set_value('id_inc'),
            'nama'         => set_value('nama'),
            'username'     => set_value('username'),
            'password'     => set_value('password'),
            'ms_role_id'   => set_value('ms_role_id',2),
            'ms_kontes_id' => set_value('ms_kontes_id'),
            'kontes'       =>$this->db->query("SELECT id_inc,nama_kontes FROM ms_kontes")->result()
	);
        $this->template->load('blank','pengguna/Pengguna_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'nama'           => $this->input->post('nama',TRUE),
                'username'       => $this->input->post('username',TRUE),
                'password'       => sha1($this->input->post('password',TRUE)),
                'ms_role_id'     => 2,  //$this->input->post('ms_role_id',TRUE),
                'ms_kontes_id'   => $this->input->post('ms_kontes_id',TRUE),
                'tanggal_insert' => date('y-m-d H:i:s'),
	    );

           $this->Mpengguna->insert($data);
            
            redirect(site_url('pengguna'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Mpengguna->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Form Edit',
                'action' => site_url('pengguna/update_action'),
		'id_inc' => set_value('id_inc', $row->id_inc),
		'nama' => set_value('nama', $row->nama),
		'username' => set_value('username', $row->username),
		'password' => set_value('password', $row->password),
		'ms_role_id' => set_value('ms_role_id', $row->ms_role_id),
		'ms_kontes_id' => set_value('ms_kontes_id', $row->ms_kontes_id),
		'ms_handling_id' => set_value('ms_handling_id', $row->ms_handling_id),
		'tanggal_insert' => set_value('tanggal_insert', $row->tanggal_insert),
		'deleted' => set_value('deleted', $row->deleted),
		'tanggal_delete' => set_value('tanggal_delete', $row->tanggal_delete),
	    );
            $this->template->load('blank','pengguna/Pengguna_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pengguna'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_inc', TRUE));
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
		'username' => $this->input->post('username',TRUE),
		'password' => $this->input->post('password',TRUE),
		'ms_role_id' => $this->input->post('ms_role_id',TRUE),
		'ms_kontes_id' => $this->input->post('ms_kontes_id',TRUE),
		'ms_handling_id' => $this->input->post('ms_handling_id',TRUE),
		'tanggal_insert' => $this->input->post('tanggal_insert',TRUE),
		'deleted' => $this->input->post('deleted',TRUE),
		'tanggal_delete' => $this->input->post('tanggal_delete',TRUE),
	    );

            $this->Mpengguna->update($this->input->post('id_inc', TRUE), $data);
            
            redirect(site_url('pengguna'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Mpengguna->get_by_id($id);

        if ($row) {
            $this->Mpengguna->delete($id);
            
            
            redirect(site_url('pengguna'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pengguna'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('username', 'username', 'trim|required');
	$this->form_validation->set_rules('password', 'password', 'trim|required');
	$this->form_validation->set_rules('ms_role_id', 'ms role id', 'trim|required');
	$this->form_validation->set_rules('ms_kontes_id', 'ms kontes id', 'trim|required');
	

	$this->form_validation->set_rules('id_inc', 'id_inc', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Pengguna.php */
/* Location: ./application/controllers/Pengguna.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-12-06 11:25:20 */
/* http://harviacode.com */