<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Welcome extends CI_Controller {

     function __construct(){

        parent::__construct(); 
        $this->kode_role=$this->session->userdata('wob_role'); 
        $this->kontes_id=$this->session->userdata('kontes_id'); 
        $this->load->model('Mkontes');
    }

	function index(){

		if($this->kode_role==4){
			redirect('pendaftaran/ownerbyhandling');			
		}else{
			$this->rekapkontes();
		}
	}



	function rekapkontes(){
		 $kontes=19;
		$data['nama_kontes']="2<sup>ND</sup> ZNA Tulungagung Koi Show 2019";
		$data['fish']=$this->db->query("SELECT keterangan status_bayar,COUNT(a.id_inc) jum
										FROM tb_ikan a
										JOIN (SELECT a.*,IFNULL(kd_bayar,'a') kd_bayara FROM tb_peserta a where checkout=1) b  ON a.tb_peserta_id=b.id_inc
										RIGHT JOIN (SELECT IFNULL(kode,'a') kode,keterangan FROM ref_bayar) c ON c.kode=b.kd_bayara
										GROUP BY keterangan
										ORDER BY status_bayar  ASC")->result();
		$data['rekap']=$this->db->query("SELECT kat_ikan,nm_ikan,sort,SUM(10_BU) A_BU, SUM(15_BU) B_BU, SUM(20_BU) C_BU, SUM(25_BU) D_BU, SUM(30_BU) E_BU, SUM(35_BU) F_BU, SUM(40_BU) G_BU, SUM(45_BU) H_BU, SUM(50_BU) I_BU, SUM(55_BU) J_BU, SUM(60_BU) K_BU, SUM(65_BU) L_BU, SUM(70_BU) M_BU, SUM(75_BU) N_BU, SUM(80_BU) O_BU
										FROM (	
											SELECT sort,kat_ikan,nm_ikan,CASE WHEN bu='10 BU' THEN jum END '10_BU'
										,CASE WHEN bu='15 BU' THEN jum END '15_BU'
										,CASE WHEN bu='20 BU' THEN jum END '20_BU'
										,CASE WHEN bu='25 BU' THEN jum END '25_BU'
										,CASE WHEN bu='30 BU' THEN jum END '30_BU'
										,CASE WHEN bu='35 BU' THEN jum END '35_BU'
										,CASE WHEN bu='40 BU' THEN jum END '40_BU'
										,CASE WHEN bu='45 BU' THEN jum END '45_BU'
										,CASE WHEN bu='50 BU' THEN jum END '50_BU'
										,CASE WHEN bu='55 BU' THEN jum END '55_BU'
										,CASE WHEN bu='60 BU' THEN jum END '60_BU'
										,CASE WHEN bu='65 BU' THEN jum END '65_BU'
										,CASE WHEN bu='70 BU' THEN jum END '70_BU'
										,CASE WHEN bu='75 BU' THEN jum END '75_BU'
										,CASE WHEN bu='80 BU' THEN jum END '80_BU'
											FROM (
												SELECT kat_ikan,nm_ikan,getAliasBu(ukuran)  BU,COUNT(*) jum,sort
												FROM  ms_kategoriikan b
												LEFT JOIN (SELECT * FROM tb_ikan WHERE tb_peserta_id IN (SELECT id_inc FROM tb_peserta WHERE checkout=1)) a ON a.ms_kat_id=b.id_inc
												GROUP BY kat_ikan,nm_ikan,BU,sort
											) a
											GROUP BY kat_ikan,nm_ikan,bu,sort
										) b
										GROUP BY kat_ikan,nm_ikan,sort
										ORDER BY sort ASC")->result();

		$this->template->load('blank','rekapkontes',$data);

	}
 

	 function nomorUnidan(){

	 	$data['nomor']=$this->db->query("SELECT no_ikan FROM tb_ikan a JOIN tb_peserta b  ON a.tb_peserta_id=b.id_inc WHERE ms_kontes_id=19 AND no_ikan IS  NOT NULL ORDER BY no_ikan ASC")->result();

			$html     =$this->load->view('kontes/nomorUndian',$data,true);
			$this->load->library('M_pdf');
			$this->m_pdf->pdf->WriteHTML($html);
			$filePath ="tmp/undian.pdf";
			$this->m_pdf->pdf->Output($filePath, 'F');
			$this->load->helper('download');
			force_download($filePath, null);   

	 }

}



