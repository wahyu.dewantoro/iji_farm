<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Result extends CI_Controller {

     function __construct(){
        parent::__construct();
        $this->kontes_id=19;  
        $this->load->model('Mkontes');

    }

     function ceklogin(){
        $ses=$this->session->userdata('kontes');
        if($ses==1){
            $asd=$this->session->userdata('wob_role');
            if($asd==4){
                redirect('landing');
            }else{
                redirect('welcome');
            }
        }
    }

	function index(){
        $this->ceklogin();
        
        $mostpoin=$this->db->query("SELECT * FROM (
                                    SELECT TRIM(nama) nama_owner,TRIM(kota) kota,SUM(getpoin(ukuran,ms_juara_id)) poin 
                                    FROM tb_juara_kontes a 
                                    JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc
                                    JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id
                                    JOIN ms_peserta d ON d.id_inc=c.ms_peserta_id
                                    GROUP BY nama_owner,kota
                                    ORDER BY poin DESC,nama_owner ASC) asr LIMIT 1")->row();
        $mostentry=$this->db->query("SELECT * FROM (
                                    SELECT TRIM(nama) nama_owner,TRIM(kota) kota,COUNT(1) ikan
                                    FROM tb_ikan b 
                                    JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id
                                    JOIN ms_peserta d ON d.id_inc=c.ms_peserta_id
                                    GROUP BY nama_owner,kota
                                    ORDER BY ikan DESC,nama_owner ASC ) asr LIMIT 1")->row();
        $mosthandling=$this->db->query("SELECT * FROM(
                                            SELECT TRIM(nama) nama_handling,TRIM(kota) kota,COUNT(1) ikan
                                            FROM tb_ikan b 
                                            JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id
                                            JOIN ms_handling d ON d.id_inc=c.ms_handling_id
                                            GROUP BY nama_handling,kota
                                            ORDER BY ikan DESC,nama_handling ASC ) awe LIMIT 1")->row();
        $ikan=$this->db->query("SELECT COUNT(1) ikan FROM tb_ikan  WHERE no_ikan IS NOT NULL")->row();

        $peserta=$this->db->query("SELECT COUNT(1) jum FROM (
                                    SELECT DISTINCT getowner(ms_peserta_id) FROM tb_peserta ) we")->row();

        $poin=$this->db->query("SELECT * FROM (
                                SELECT TRIM(nama) nama_owner,TRIM(kota) kota,SUM(getpoin(ukuran,ms_juara_id)) poin 
                                FROM tb_juara_kontes a 
                                JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc
                                JOIN tb_peserta c ON c.id_inc=b.tb_peserta_id
                                JOIN ms_peserta d ON d.id_inc=c.ms_peserta_id
                                GROUP BY nama_owner,kota
                                ORDER BY poin DESC,nama_owner ASC) asr LIMIT 15")->result();
        $data=array(
            'mostpoin'     =>$mostpoin,
            'mostentry'    =>$mostentry,
            'mosthandling' =>$mosthandling,
            'ikan'         =>$ikan,
            'peserta'      =>$peserta,
            'poin'=>$poin
        );

        $this->template->load('depan/halaman_depan','hasil/rekap',$data);

    }

    function rank(){
        $this->ceklogin();
        $vsize    =rapikan($this->input->get('size',true));
        $vsize    =$vsize<>''?$vsize:'0_10';
         $qwe=rapikan($this->input->get('class',true));
        $vclass   =$qwe;//<>''?$qwe:'all';
        $vvariety =urldecode($this->input->get('variety'));//<>''?urldecode($this->input->get('variety')):'iamp' ;
        

        $ukuran  =$this->db->query("SELECT min,max,CONCAT( IF(MIN=0,'up to ',CONCAT(MIN,'-')),MAX,' cm') label FROM ms_biayakontes")->result();
        $kelas   =$this->db->query("SELECT DISTINCT kat_ikan,alias FROM ms_kategoriikan")->result();

        $sd=$vclass<>'all'?"WHERE kat_ikan ='".$vclass."'":"";
        


        $variety =$this->db->query("SELECT nm_ikan FROM ms_kategoriikan $sd ORDER BY sort ASC")->result();

        $data=array(
            'size'     =>$ukuran,
            'kelas'    =>$kelas,
            'variety'  =>$variety,
            'script'   =>'hasil/js_rank',
            'vsize'    =>$vsize,
            'vclass'   =>$vclass,
            'vvariety' =>$vvariety,
        );
        $this->template->load('depan/halaman_depan','hasil/rank',$data);
    }

    function bis(){
        $data=[];
        $this->template->load('depan/halaman_depan','hasil/vbis',$data);   
    }

    function champion(){
        $res=$this->db->query("SELECT * FROM ref_champion ORDER BY urut DESC")->result();
        $this->template->load('depan/halaman_depan','hasil/vchmapion',array('res'=>$res));
    }

}



