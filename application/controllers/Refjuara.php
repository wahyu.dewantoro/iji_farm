<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Refjuara extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mjuara');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $refjuara = $this->Mjuara->get_all();

        $data = array(
            'refjuara_data' => $refjuara
        );

        $this->template->load('blank','refjuara/Refjuara_list', $data);
    }

    

    public function create() 
    {
        $data = array(
            'button'     => 'Form Tambah',
            'action'     => site_url('refjuara/create_action'),
            'id_inc'     => set_value('id_inc'),
            'kode_juara' => set_value('kode_juara'),
            'nama_juara' => set_value('nama_juara'),
            'alias'      => set_value('alias'),
          
	);
        $this->template->load('blank','refjuara/Refjuara_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
        'kode_juara' => $this->input->post('kode_juara',TRUE),
        'nama_juara' => $this->input->post('nama_juara',TRUE),
        'alias'      => $this->input->post('alias',TRUE),
        
	    );

           $this->Mjuara->insert($data);
            
            redirect(site_url('refjuara'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Mjuara->get_by_id($id);

        if ($row) {
            $data = array(
                'button'     => 'Form Edit',
                'action'     => site_url('refjuara/update_action'),
                'id_inc'     => set_value('id_inc', $row->id_inc),
                'kode_juara' => set_value('kode_juara', $row->kode_juara),
                'nama_juara' => set_value('nama_juara', $row->nama_juara),
                'alias'      => set_value('alias', $row->alias),
	    );
            $this->template->load('blank','refjuara/Refjuara_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('refjuara'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_inc', TRUE));
        } else {
            $data = array(
        'kode_juara' => $this->input->post('kode_juara',TRUE),
        'nama_juara' => $this->input->post('nama_juara',TRUE),
        'alias'      => $this->input->post('alias',TRUE),
    
	    );

            $this->Mjuara->update($this->input->post('id_inc', TRUE), $data);
            
            redirect(site_url('refjuara'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Mjuara->get_by_id($id);

        if ($row) {
            $this->Mjuara->delete($id);
            
            
            redirect(site_url('refjuara'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('refjuara'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('kode_juara', 'kode  ', 'trim|required');
	$this->form_validation->set_rules('nama_juara', '  juara', 'trim|required');
	$this->form_validation->set_rules('alias', 'alias', 'trim|required');
	$this->form_validation->set_rules('id_inc', 'id_inc', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }


    function poin(){
         $data=array(
            'data'=>$this->db->query("SELECT a.`id_inc`,nama_juara,ukuran_min,ukuran_max,poin
                                        FROM ms_juara_poin a
                                        JOIN ms_juara b ON a.`ms_juara_id`=b.id_inc")->result()
            );
        $this->template->load('blank','refjuara/poin', $data);
    }


    function createPoin(){
          $data = array(
            'button'      => 'Form Poin',
            'action'      => site_url('refjuara/createpoinaction'),
            'id_inc'      => set_value('id_inc'),
            'ms_juara_id' => set_value('ms_juara_id'),
            'ukuran_min'  => set_value('ukuran_min'),
            'ukuran_max'  => set_value('ukuran_max'),
            'poin'        => set_value('poin'),
            'juara'       => $this->Mjuara->get_all()
        );

        $this->template->load('blank','refjuara/Refjuarapoinform', $data);
    }


    function editpoin($id){
        $this->db->where('id_inc',$id);
        $rk=$this->db->get('ms_juara_poin')->row();
        if($rk){
               $data = array(
            'button'      => 'Form Poin',
            'action'      => site_url('refjuara/updatepoinaction'),
            'id_inc'      => set_value('id_inc',$rk->id_inc),
            'ms_juara_id' => set_value('ms_juara_id',$rk->ms_juara_id),
            'ukuran_min'  => set_value('ukuran_min',$rk->ukuran_min),
            'ukuran_max'  => set_value('ukuran_max',$rk->ukuran_max),
            'poin'        => set_value('poin',$rk->poin),
            'juara'       => $this->Mjuara->get_all()
        );

        $this->template->load('blank','refjuara/Refjuarapoinform', $data);       
        }else{
            redirect(site_url('refjuara/poin'));
        }
    }


    function createpoinaction(){

         $ms_juara_id =$this->input->post('ms_juara_id');
         $ukuran_min  =$this->input->post('ukuran_min');
         $ukuran_max  =$this->input->post('ukuran_max');
         $poin        =$this->input->post('poin');

         $data=array(
            'ms_juara_id'=>$ms_juara_id,
            'ukuran_min'=>$ukuran_min,
            'ukuran_max'=>$ukuran_max,
            'poin'=>$poin,
         );

         $this->db->insert('ms_juara_poin',$data);
            redirect(site_url('refjuara/poin'));
    }

    function updatepoinaction(){
        $ms_juara_id =$this->input->post('ms_juara_id');
         $ukuran_min  =$this->input->post('ukuran_min');
         $ukuran_max  =$this->input->post('ukuran_max');
         $poin        =$this->input->post('poin');
         $id        =$this->input->post('id_inc');

         $data=array(
            'ms_juara_id'=>$ms_juara_id,
            'ukuran_min'=>$ukuran_min,
            'ukuran_max'=>$ukuran_max,
            'poin'=>$poin,
         );

         $wh=array(
            'id_inc'=>$id
         );
         $this->db->update('ms_juara_poin',$data,$wh);
            redirect(site_url('refjuara/poin'));
    }


    function depoin($id){
        $this->db->where('id_inc',$id);
        $this->db->delete('ms_juara_poin');
        redirect(site_url('refjuara/poin'));
    }
}

/* End of file Refjuara.php */
/* Location: ./application/controllers/Refjuara.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-12-02 14:23:01 */
/* http://harviacode.com */