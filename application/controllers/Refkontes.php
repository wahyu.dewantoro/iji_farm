<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Refkontes extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mkontes');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $refkontes = $this->Mkontes->get_all();

        $data = array(
            'refkontes_data' => $refkontes
        );

        $this->template->load('blank','refkontes/Refkontes_list', $data);
    }

    

    public function create() 
    {
        $data = array(
            'button'          => 'Form Tambah',
            'action'          => site_url('refkontes/create_action'),
            'id_inc'          => set_value('id_inc'),
            'nama_kontes'     => set_value('nama_kontes'),
            'tanggal_mulai'   => set_value('tanggal_mulai'),
            'tanggal_selesai' => set_value('tanggal_selesai'),
            'tempat_kontes'   => set_value('tempat_kontes'),
            'logo_kontes'     => set_value('logo_kontes'),
            'status_kontes'   => set_value('status_kontes'),
            'biaya'           =>$this->db->query("SELECT * FROM `ms_refukuran`")->result(),
            'bak'             =>$this->db->query("SELECT * FROM ms_refbak")->result()
	);
        $this->template->load('blank','refkontes/Refkontes_form', $data);
    }
    
    public function create_action() 
    {
       
        $this->db->trans_start();
            // insert kontes
            if(!empty($_FILES['logo_kontes']['tmp_name'])){
                $name =$_FILES['logo_kontes']['name'];
                $tmp  =$_FILES['logo_kontes']['tmp_name'];
                $exp=explode('.',$name);
                $logo='dokumen/logo/'.sha1(date('YmdHis')).'.'.$exp[count($exp)-1];
            }else{
                $logo=null;
            }

            $this->db->set('nama_kontes',$_POST['nama_kontes']);
            $this->db->set('tanggal_mulai',$_POST['tanggal_mulai']);
            $this->db->set('tanggal_selesai',$_POST['tanggal_selesai']);
            $this->db->set('tempat_kontes',$_POST['tempat_kontes']);
            $this->db->set('logo_kontes',$logo);
            $this->db->set('status_kontes',$_POST['status_kontes']);
            $this->db->set('pengguna_id',$this->session->userdata('wob_pengguna'));
            $rk=$this->db->insert('ms_kontes');

            if(!empty($_FILES['logo_kontes']['tmp_name'])){
                move_uploaded_file($tmp,$logo);
            }

            // get max kontes
            $rg=$this->db->query("SELECT MAX(id_inc) id_kontes FROM ms_kontes")->row();
            $id_kontes=$rg->id_kontes;

            // insert bak
            if(count($_POST['biayabak'])>0){
                for($i=0;$i<count($_POST['biayabak']);$i++){
                    if(!empty($_POST['biayabak'][$i])){
                        $this->db->set('ms_kontes_id',$id_kontes);
                        $this->db->set('nama_bak',$_POST['nama_bak'][$i]);
                        $this->db->set('jumlah',$_POST['jumlah'][$i]);
                        $this->db->set('biaya',$_POST['biayabak'][$i]);
                        $this->db->insert('ms_bakkontes');
                    }
                }
            }

            // insert biaya
            if(count($_POST['biayaukuran'])>0){
                for($a=0;$a<count($_POST['biayaukuran']);$a++){
                    if(!empty($_POST['biayaukuran'][$a])){
                        $this->db->set('ms_kontes_id',$id_kontes);
                        $this->db->set('min',$_POST['minbiaya'][$a]);
                        $this->db->set('max',$_POST['maxbiaya'][$a]);
                        $this->db->set('biaya',$_POST['biayaukuran'][$a]);
                        $this->db->insert('ms_biayakontes');
                    }
                }
            }
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                 $this->session->set_flashdata('msg', 
                        '<div class="note note-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Oppss</h4>
                            <p>Data gagal disimpan.</p>
                        </div>');    
        }else{
                $this->db->trans_commit();
                   $this->session->set_flashdata('msg', 
                        '<div class="note note-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4>Berhasil </h4>
                            <p>Data telah disimpan.</p>
                        </div>');               
        }
         redirect(site_url('refkontes'));

      
    }
    
    public function update($id) 
    {
        $row = $this->Mkontes->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Form Edit',
                'action' => site_url('refkontes/update_action'),
		'id_inc' => set_value('id_inc', $row->id_inc),
		'nama_kontes' => set_value('nama_kontes', $row->nama_kontes),
		'tanggal_mulai' => set_value('tanggal_mulai', $row->tanggal_mulai),
		'tanggal_selesai' => set_value('tanggal_selesai', $row->tanggal_selesai),
		'tempat_kontes' => set_value('tempat_kontes', $row->tempat_kontes),
		'logo_kontes' => set_value('logo_kontes', $row->logo_kontes),
		'status_kontes' => set_value('status_kontes', $row->status_kontes),
		'tanggal_insert' => set_value('tanggal_insert', $row->tanggal_insert),
		'pengguna_id' => set_value('pengguna_id', $row->pengguna_id),
	    );
            $this->template->load('blank','refkontes/Refkontes_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('refkontes'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_inc', TRUE));
        } else {
            $data = array(
		'nama_kontes' => $this->input->post('nama_kontes',TRUE),
		'tanggal_mulai' => $this->input->post('tanggal_mulai',TRUE),
		'tanggal_selesai' => $this->input->post('tanggal_selesai',TRUE),
		'tempat_kontes' => $this->input->post('tempat_kontes',TRUE),
		'logo_kontes' => $this->input->post('logo_kontes',TRUE),
		'status_kontes' => $this->input->post('status_kontes',TRUE),
		'tanggal_insert' => $this->input->post('tanggal_insert',TRUE),
		'pengguna_id' => $this->input->post('pengguna_id',TRUE),
	    );

            $this->Mkontes->update($this->input->post('id_inc', TRUE), $data);
            
            redirect(site_url('refkontes'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Mkontes->get_by_id($id);

        if ($row) {
            $this->Mkontes->delete($id);
            
            
            redirect(site_url('refkontes'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('refkontes'));
        }
    }

  function read($id){
    $rk=$this->Mkontes->getKontesId($id);
    $data['kontes'] =$rk['kontes'];
    $data['biaya']  =$rk['biaya'];
    $data['bak']    =$rk['bak'];
    $this->template->load('blank','refkontes/Refkontes_read',$data);
  }

  function prosesupdate(){
            if(!empty($_FILES['logo_kontes']['tmp_name'])){
                $name =$_FILES['logo_kontes']['name'];
                $tmp  =$_FILES['logo_kontes']['tmp_name'];
                $exp=explode('.',$name);
                $logo='dokumen/logo/'.sha1(date('YmdHis')).'.'.$exp[count($exp)-1];
                move_uploaded_file($tmp,$logo);
                @unlink($_POST['lawas']);
            }else{
                $logo=$_POST['lawas'];
            }

            if(!empty($_FILES['ttd_headjudge']['tmp_name'])){
                $namehj =$_FILES['ttd_headjudge']['name'];
                $tmphj  =$_FILES['ttd_headjudge']['tmp_name'];
                $exphj=explode('.',$namehj);
                $logohj='dokumen/ttd/ttdhj'.sha1(date('YmdHis')).'.'.$exphj[count($exphj)-1];
                move_uploaded_file($tmphj,$logohj);
                @unlink($_POST['lawasttdhj']);
            }else{
                $logohj=$_POST['lawasttdhj'];
            }

            if(!empty($_FILES['ttd_chairman']['tmp_name'])){
                $namech =$_FILES['ttd_chairman']['name'];
                $tmpch  =$_FILES['ttd_chairman']['tmp_name'];
                $expch=explode('.',$namech);
                $logoch='dokumen/ttd/ttdch'.sha1(date('YmdHis')).'.'.$expch[count($expch)-1];
                move_uploaded_file($tmpch,$logoch);
                @unlink($_POST['lawasttdcm']);
            }else{
                $logoch=$_POST['lawasttdcm'];
            }


            if(!empty($_FILES['ttd_commite']['tmp_name'])){
                $namecom =$_FILES['ttd_commite']['name'];
                $tmpcom  =$_FILES['ttd_commite']['tmp_name'];
                $expcom=explode('.',$namecom);
                $logocom='dokumen/ttd/ttdcom'.sha1(date('YmdHis')).'.'.$expcom[count($expcom)-1];
                move_uploaded_file($tmpcom,$logocom);
                @unlink($_POST['lawasttdcmc']);
            }else{
                $logocom=$_POST['lawasttdcmc'];
            }


        $this->db->where('id_inc',$_POST['id_inc']);

        $this->db->set('nama_headjudge',$_POST['nama_headjudge']);
        $this->db->set('nama_chairman',$_POST['nama_chairman']);
        $this->db->set('nama_commite',$_POST['nama_commite']);
        $this->db->set('ttd_headjudge',$logohj);
        $this->db->set('ttd_chairman',$logoch);
        $this->db->set('ttd_commite',$logocom);

        $this->db->set('nama_kontes',$_POST['nama_kontes']);
        $this->db->set('tanggal_mulai',$_POST['tanggal_mulai']);
        $this->db->set('tanggal_selesai',$_POST['tanggal_selesai']);
        $this->db->set('tempat_kontes',$_POST['tempat_kontes']);
        $this->db->set('status_kontes',$_POST['status_kontes']);
        $this->db->set('logo_kontes',$logo);
        $this->db->update('ms_kontes');

        redirect('refkontes/read/'.$_POST['id_inc']);
  }

  function formeditbak($id){
    $data['rk']=$this->db->query("SELECT * FROM ms_bakkontes WHERE id_inc=$id")->row();
    $this->load->view('refkontes/vformeditbak',$data);
  }

  function proseseditbak(){
    $this->db->where('id_inc',$_POST['id_inc']);
    $this->db->set('nama_bak',$_POST['nama_bak']);
    $this->db->set('jumlah',$_POST['jumlah']);
    $this->db->set('biaya',$_POST['biaya']);
    $this->db->update('ms_bakkontes');
    redirect('refkontes/read/'.$_POST['ms_kontes_id']);
  }

  function formeditkontes($id){
    $rk=$this->Mkontes->getKontesId($id);
    $data['kontes'] =$rk['kontes'];
    $this->load->view('refkontes/vformeditkontes',$data);
  }

}

/* End of file Refkontes.php */
/* Location: ./application/controllers/Refkontes.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-12-05 13:26:32 */
/* http://harviacode.com */