<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Summary</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for fixed footer option" name="description" />
        <meta content="" name="author" />
      <link href="<?= base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
      <link href="<?= base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
      <link href="<?= base_url()?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      <link href="<?= base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
      
      <link href="<?= base_url() ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
      <link href="<?= base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
      <link href="<?= base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
      
      <link href="<?= base_url()?>assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css" />
      <link href="<?= base_url()?>assets/global/css/plugins.css" rel="stylesheet" type="text/css" />
      <link href="<?= base_url()?>assets/layouts/layout/css/layout.css" rel="stylesheet" type="text/css" />
      <link href="<?= base_url()?>assets/layouts/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color" />
      <link href="<?= base_url()?>assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css" />
      <link href="<?= base_url()?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
      <link href="<?= base_url()?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
      <link href="<?= base_url()?>assets/global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css" />
       <link href="<?= base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />

        <!-- BEGIN CORE PLUGINS -->
        <script src="<?= base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?= base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>

         <script src="<?= base_url()?>assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="<?= base_url()?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="<?= base_url()?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="<?= base_url()?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="<?= base_url()?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
       
       <script src="<?= base_url() ?>assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
       <script src="<?= base_url() ?>assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
       <script src="<?= base_url() ?>assets/layouts/layout/scripts/demo.js" type="text/javascript"></script> 
       <script src="<?= base_url() ?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
       <script src="<?= base_url() ?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
      
      <script src="<?= base_url() ?>assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
      <script src="<?= base_url() ?>assets/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
      <script src="<?= base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <style type="text/css">
            #notifications {
                cursor: pointer;
                position: fixed;
                right: 0px;
                z-index: 9999;
                bottom: 0px;
                margin-bottom: 22px;
                margin-right: 15px;
                min-width: 300px; 
                max-width: 800px;  
            }
        </style>
</head>
    <body class="page-header-fixed page-footer-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <div class="page-header navbar navbar-fixed-top">
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a style="text-decoration:none;" href="<?php echo base_url('summary')?>">
                            <h3><b><?php echo $this->session->userdata('labelkontes')?></b></h3> </a> 
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->

                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <!-- <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-user">
                                
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="fa fa-trophy"></i> Summary   
                                </a>
                            </li>
                         
                        </ul>
                    </div> -->
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- BEGIN SIDEBAR MENU -->
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <?php if($this->session->userdata('kontes_id')==''){?>
                                <li class='nav-item active'><a class='nav-link' href="#"><i class='fa fa-horn'></i> <span class='title'>List Kontes</span></a>  </li>
                            <?php }else{ ?>
                            <li class='nav-item '><a class='nav-link' href="<?php echo base_url().'summary'?>"><i class='fa fa-file-text'></i> <span class='title'>Summary</span></a></li>
                            <li class="nav-item start  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-cubes"></i>
                                    <span class="title">Reguler Winner</span>
                                    <!-- <span class="selected"></span> -->
                                    <span class="arrow open"></span>
                                </a>
                                <ul class="sub-menu">
                                    <?php  
                                       $sql=$this->db->query("SELECT CONCAT(MIN,MAX) iduk, CASE WHEN MIN=0 THEN 'Up to 15 cm' ELSE  CONCAT(MIN,' - ',MAX,' cm') END label FROM ms_biayakontes WHERE ms_kontes_id='".$this->session->userdata('kontes_id')."'")->result(); 
                                       foreach($sql as $ss){
                                    ?>
                                    <li class="nav-item start">
                                        <a href="<?php echo base_url().'summary/reguler/'.$ss->iduk?>" class="nav-link ">
                                            <i class="fa fa-angle-double-right"></i>
                                            <span class="title"><?php echo $ss->label?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <li class="nav-item start  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-trophy"></i>
                                    <span class="title">Main Winner</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item start">
                                        <a href="<?php echo base_url().'summary/bis'?>" class="nav-link ">
                                            <i class="fa fa-angle-double-right"></i>
                                            <span class="title">Best In Size</span>
                                        </a>
                                    </li>
                                    <li class="nav-item start">
                                        <a href="<?php echo base_url().'summary/champion'?>" class="nav-link ">
                                            <i class="fa fa-angle-double-right"></i>
                                            <span class="title">Champion</span>
                                        </a>
                                    </li>
                                   <!--  <li class="nav-item start">
                                        <a href="<?php echo base_url().'summary/biv'?>" class="nav-link ">
                                            <i class="fa fa-angle-double-right"></i>
                                            <span class="title">Best In Variety</span>
                                        </a>
                                    </li> -->
                                </ul>
                            </li>
                            <?php } ?>
                        </ul>
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <div class="page-content">
                         <?=  $contents; ?>
                         <div id="notifications"><?php echo $this->session->flashdata('msg'); ?></div> 
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> 2018 &copy; W.O.B Production
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
     
        
        <!-- END THEME LAYOUT SCRIPTS -->
        <script>   
            $('#notifications').slideDown('slow').delay(3000).slideUp('slow');
            $(function() {
              $('.page-sidebar-menu a[href~="' + location.href + '"]').parents('li').addClass('active');
            });


        </script>
    </body>

</html>
