        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    Master
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Poin</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div class="btn-group pull-right">
                                    <?php echo anchor(site_url('refjuara/createPoin'), '<i class="fa fa-plus"></i> Tambah','class="btn purple btn-sm btn-outline"'); ?>
                                </div>
                            </div>
                        </div>
        <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-list"></i>Data</div>
                                    </div>
                                    <div class="portlet-body">
                                        
        <table class="table table-striped table-bordered table-hover" id="tb" >
            <thead>
                <tr>
                    <th rowspan="2" width="5%">No</th>
                    <th  rowspan="2" >Award</th>
                    <th colspan="2">Ukuran</th>
                    <th rowspan="2" >Poin</th>
                    <th rowspan="2" > </th>
                </tr>
                <tr>
                    
                    <th>Min</th>
                    <th>Max</th>
                    
                </tr>
            </thead>
	       <tbody>
               <?php $no=1; foreach($data as $rk){?>
                <tr>
                    <td align="center"><?= $no ?></td>
                    <td><?= $rk->nama_juara ?></td>
                    <td><?= $rk->ukuran_min ?></td>
                    <td><?= $rk->ukuran_max ?></td>
                    <td><?= $rk->poin ?></td>
                    <td align="center">
                        <?= anchor('refjuara/editpoin/'.$rk->id_inc,'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"');?>
                        <?= anchor('refjuara/depoin/'.$rk->id_inc,'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger"');?>
                    </td>
                </tr>
               <?php $no++; } ?>
           </tbody>
        </table>
       
    </div>
</div>
       