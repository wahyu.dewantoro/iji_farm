        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    Master
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Poin</span>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span><?php echo $button ?></span>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-green-haze">
                                    <i class="fa fa-file-text font-green-haze"></i>
                                    <span class="caption-subject bold"> <?php echo $button ?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
        <form action="<?php echo $action; ?>" method="post" class="form-horizontal">
            <div class="form-group form-md-line-input has-success">
                <label class="col-md-2 control-label">Juara</label>
                <div class="col-md-10">
                    <select required name="ms_juara_id" id="ms_juara_id" class="form-control" >
                        <?php foreach($juara as $j){?>
                            <option <?php if($ms_juara_id==$j->id_inc){echo "selected";} ?> value="<?= $j->id_inc?>"><?= $j->nama_juara?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group form-md-line-input has-success">
                <label class="col-md-2 control-label">Min</label>
                <div class="col-md-10">
                    <input type="text" required name="ukuran_min" id="ukuran_min" class="form-control" value="<?= $ukuran_min ?>">
                </div>
            </div>
            <div class="form-group form-md-line-input has-success">
                <label class="col-md-2 control-label">Max</label>
                <div class="col-md-10">
                    <input type="text" required name="ukuran_max" id="ukuran_max" class="form-control" value="<?= $ukuran_max ?>">
                </div>
            </div>
            <div class="form-group form-md-line-input has-success">
                <label class="col-md-2 control-label">Poin</label>
                <div class="col-md-10">
                    <input type="text" required name="poin" id="poin" class="form-control" value="<?= $poin ?>">
                </div>
            </div>
 
	   <div class="form-group"><div class="col-md-10 col-md-offset-2"> <input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" /> 
	    <button type="submit" class="btn btn-sm blue"><i class="fa fa-save"></i> Simpan</button> 
	    <a href="<?php echo site_url('refjuara/poin') ?>" class="btn btn-sm red"><i class="fa fa-close"></i> Batal</a> </div></div>
	</form>
        </div>
</div>