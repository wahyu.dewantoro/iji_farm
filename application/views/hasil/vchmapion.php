<div class="my-3 p-3 bg-white rounded box-shadow">
 	<p class="text-center"><b >Champion</b></p>
	<div class="row">
	 	<?php 
	 	foreach($res as $list){?>
	 		<div class="col-12">
	 			<hr><b><?=  ucwords(strtolower($list->alias)) ?></b><hr>
	 		</div>
	 		<?php
	 			$aa=$this->db->query("SELECT DISTINCT alias FROM ms_kategoriikan ORDER BY kat_ikan ASC")->result();
	 			foreach($aa as $aa){

	 				$ikan=$this->db->query("SELECT b.*,getownerkota(ms_peserta_id) owner,gethandlinkota(ms_handling_id) handling
											FROM tb_juara_kontes a
											JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc
											JOIN ms_kategoriikan c ON c.id_inc=b.ms_kat_id
											join tb_peserta d on d.id_inc=b.tb_peserta_id
											WHERE ms_juara_id=".$list->id_juara." AND ukuran > ".$list->vmin." AND ukuran <= ".$list->vmax." AND c.alias='".$aa->alias."'")->row();
	 				if($ikan){
	 					$gbr=$ikan->gambar_ikan;
	 				}else{
	 					$gbr='noimage.png';
	 				}

	 				if($list->id_juara==11){
	 		 ?>		

	 		 <div class="col-6 col-md-3">
					<div class="text-center">
					  <img src="<?= base_url().$gbr ?>" class="img-thumbnail img-fluid" alt="<?= $aa->alias ?>">
					  <table>
					  		<tr>
					  			<td><?= $aa->alias ?></td>
					  		</tr>
					  		<tr>
					  			<td>No Ikan: <?php echo !empty($ikan->no_ikan)?$ikan->no_ikan:'-';?></td>
					  		</tr>
					  		<tr>
					  			<td><b>owner</b><br>
					  				<?php  echo $ikan->owner?></td>
					  		</tr>
					  		<tr>
					  			<td><b>handling</b><br>
					  				<?php echo  $ikan->handling?></td>
					  		</tr>
					  </table>
					</div>
				</div>
	 	<?php } }
	 	if($list->id_juara==10){
	 		$ikano=$this->db->query("SELECT b.*,getownerkota(ms_peserta_id) owner,gethandlinkota(ms_handling_id) handling
											FROM tb_juara_kontes a
											JOIN tb_ikan b ON a.tb_ikan_id=b.id_inc
											JOIN ms_kategoriikan c ON c.id_inc=b.ms_kat_id
											join tb_peserta d on d.id_inc=b.tb_peserta_id
											WHERE ms_juara_id=".$list->id_juara." AND ukuran > ".$list->vmin." AND ukuran <= ".$list->vmax)->row();
	 				if($ikano){
	 					$gbro=$ikano->gambar_ikan;
	 				}else{
	 					$gbro='noimage.png';
	 				}
	 		?>
<div class="col-6 col-md-3">
					<div class="text-center">
					  <img src="<?= base_url().$gbro ?>" class="img-thumbnail img-fluid" alt="<?= $aa->alias ?>">
					  <table>
					  		
					  		<tr>
					  			<td>No Ikan: <?php echo !empty($ikano->no_ikan)?$ikano->no_ikan:'-';?></td>
					  		</tr>
					  		<tr>
					  			<td><b>owner</b><br>
					  				<?php echo $ikano->owner?></td>
					  		</tr>
					  		<tr>
					  			<td><b>handling</b><br>
					  				<?php echo $ikano->handling?></td>
					  		</tr>
					  </table>
					</div>
				</div>
	 	<?php }

	 	 } ?>
	</div>	
</div>
 