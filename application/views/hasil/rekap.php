<div class="my-3 p-3 bg-white rounded box-shadow">

  <h4 class="text-left page-title">Summary</h4>

	<hr>

	<table style="font-size: 12px">

		<tr valign="top">

			<td>Most Poin</td>

			<td width="1%">:</td>

			<td><?php echo  $mostpoin->nama_owner.' / '.$mostpoin->kota.' - '.$mostpoin->poin.' poin' ?></td>

		</tr>

		<tr valign="top">

			<td>Most Entry</td>

			<td width="1%">:</td>

			<td><?php echo  $mostentry->nama_owner.' / '.$mostentry->kota.' - '.$mostentry->ikan.' fish' ?></td>

		</tr>

		<tr valign="top">

			<td>Most Handling</td>

			<td width="1%">:</td>

			<td><?php echo $mosthandling->nama_handling.' / '.$mosthandling->kota.' - '.$mosthandling->ikan.' fish' ?></td>

		</tr>

		<tr valign="top">

			<td>Fish Entry</td>

			<td width="1%">:</td>

			<td><?php echo  $ikan->ikan.' fish' ?></td>

		</tr>

		<tr valign="top">

			<td>Participants</td>

			<td width="1%">:</td>

			<td><?= $peserta->jum.' participant'?></td>

		</tr>

	</table>

</div>

<div class="my-3 p-3 bg-white rounded box-shadow">

  <h4 class="text-left page-title">List Points</h4>

	<hr>

	<table class="table table-striped" style="font-size: 12px">

		<thead>

			<tr>

				<td width="5%">#</td>

				<td><b>Nama</b></td>

				<td><b>Kota</b></td>

				<td><b>Points</b></td>

			</tr>

		</thead>

		<tbody>

			<?php $no=1; foreach($poin as $poin){?>

				<tr>

					<td align="center"><?= $no ?></td>

					<td><?= $poin->nama_owner ?></td>

					<td><?= $poin->kota ?></td>

					<td align="right"><?= number_format($poin->poin,0,'','.') ?></td>

				</tr>

			<?php $no++;}?>

		</tbody>

	</table>

</div>



<!--  -->