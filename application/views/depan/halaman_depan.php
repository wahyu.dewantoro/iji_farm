<!doctype html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://getbootstrap.com/favicon.ico">

    <title>Koi Show</title>

    <link rel="canonical" href="index.html">

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url('cssdepan')?>/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- font awesome -->
    <link href="<?= base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Custom styles for this template -->
    <link href="<?= base_url('cssdepan')?>/offcanvas.css" rel="stylesheet">

    
  </head>

  <body class="bg-light">

    <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
      <a class="navbar-brand" href="<?= base_url()?>">ZNA Tulungagung</a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item  ">
            <a class="nav-link" href="<?= base_url() ?>"><i class="fa fa-home"></i> Home</a>
          </li>
          <?php 
            $pov=$this->Mkontes->get_by_id(19)->status_kontes;

            if($pov==1){
          ?>

          <li class="nav-item  ">
            <a class="nav-link" href="<?= base_url().'home/signup'?>"><i class="fa fa-user-plus"></i> Registrasi</a>
          </li>
          <li class="nav-item  ">
            <a class="nav-link" href="<?= base_url().'auth'?>"><i class="fa  fa-sign-in"></i> Log In</a>
          </li>
        <?php }else{ ?>
          <li class="nav-item  ">
            <a class="nav-link" href="<?= base_url().'result'?>"><i class="fa  fa-file-text"></i> Summary</a>
          </li>
          <li class="nav-item  ">
            <a class="nav-link" href="<?= base_url().'result/rank'?>"><i class="fa fa-sort-numeric-asc"></i> Rank</a>
          </li>
          <li class="nav-item  ">
            <a class="nav-link" href="<?= base_url().'result/bis'?>"><i class="fa fa-star"></i> BIS</a>
          </li>
          <li class="nav-item  ">
            <a class="nav-link" href="<?= base_url().'result/champion'?>"><i class="fa  fa-trophy"></i> Champion</a>
          </li>
          <!-- <li class="nav-item  ">
            <a class="nav-link" href="<?php // base_url().'result/other'?>"><i class="fa  fa-cubes"></i> Other Prize</a>
          </li> -->

        <?php } ?>
        </ul>
      </div>
    </nav>
 

    <main role="main" class="container">
        <?=  $contents; ?>
       
 


    </main>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?= base_url('cssdepan')?>/assets/js/vendor/jquery-slim.min.js"></script>
    <script src="<?= base_url('cssdepan')?>/assets/js/vendor/popper.min.js"></script>
    <script src="<?= base_url('cssdepan')?>/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url('cssdepan')?>/assets/js/vendor/holder.min.js"></script>
    <script src="<?= base_url('cssdepan')?>/offcanvas.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    
    <script type="text/javascript">
        $("#btn").prop('disabled', true);
        $('#password, #confirm').on('keyup', function () {

          
          if($('#password').val()!='' && $('#confirm').val()!=''){
              if ($('#password').val() == $('#confirm').val()) {
                $('#message').html('Matching').css('color', 'green');
                $("#btn").prop('disabled', false);
              } else {
                $('#message').html('Not Matching').css('color', 'red');
                 $("#btn").prop('disabled', true);
              }

            }else{
              $("#btn").prop('disabled', true);
            }

        });


        $(function() {
            $('#email').change(function() {
              
               $.ajax({
                    url: "<?= base_url().'home/cekemail'?>",
                    type: 'post',
                    data: 'email='+$('#email').val(),
                    success: function(data) {
                      // alert(data);
                      if(data>0){
                        $('#notifemail').html('email sudah di gunakan').css('color', 'red');
                        $("#btn").prop('disabled', true);
                      }else{
                        $('#notifemail').html('').css('color', 'red');
                        $("#btn").prop('disabled', false);
                      }
                      
                    }
                    
                });
            });
        });


    function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
    </script>
    <?php if (isset($script)) { $this->load->view($script); } ?>
  </body>

</html>
