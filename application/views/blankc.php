<!DOCTYPE html>
<html>

<!-- Mirrored from adminlte.io/themes/AdminLTE/pages/layout/top-nav.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 14 Oct 2018 08:16:03 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>1st Madiun Young Koi Show</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= base_url().'lte/' ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <!-- <link rel="stylesheet" href="<?= base_url().'lte/' ?>bower_components/font-awesome/css/font-awesome.min.css"> -->
  <link href="<?= base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= base_url().'lte/' ?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url().'lte/' ?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url().'lte/' ?>dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style type="text/css">
       .img-fluid {

   height:140px;
   width:100%;
     margin: auto;
    
}

.item {
    margin: auto;
    position:relative;
    padding-top:10px;
    display:inline-block;
}

.notify-badge{
    position: absolute;
    right:0px;
    top:10px;
    background:grey;
    text-align: center;
    border-radius: 3px 3px 3px 3px;
    color:white;
    padding:2px 4px;
    font-size:12px;
}
  </style>
  <!-- jQuery 3 -->
<script src="<?= base_url().'lte/' ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url().'lte/' ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?= base_url().'lte/' ?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url().'lte/' ?>bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url().'lte/' ?>dist/js/adminlte.min.js"></script>
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?=  base_url().'summary' ?>" class="navbar-brand"><b><b>1<sup>st</sup> </b>Madiun Karismatik Young Koi Show</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-align-justify"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li ><a  href="<?php echo base_url().'summary'?>"><i class='fa fa-file-text'></i> <span class='title'>Summary</span></a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa  fa-sort-numeric-asc"></i> Reguler Winner <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <?php $sql=$this->db->query("SELECT CONCAT(MIN,MAX) iduk, CASE WHEN MIN=0 THEN 'Up to 10 cm' ELSE  CONCAT(MIN,' - ',MAX,' cm') END label FROM ms_biayakontes WHERE ms_kontes_id='19'")->result(); 
                     foreach($sql as $ss){        ?>
                          <li><a href="<?php echo base_url().'summary/reguler/'.$ss->iduk?>"><span class="title"><?php echo $ss->label?></span></a></li>
                <?php } ?>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-trophy"></i>  Main Winner <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li> <a href="<?php echo base_url().'summary/bis'?>" ><span class="title">Best In Size</span></a></li>
                <li><a href="<?php echo base_url().'summary/champion'?>"><span class="title">Champion</span></a></li>
              </ul>
            </li>
          </ul>
          
        </div>
               
      </div>
      
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h4>
          <?= $title ?>
        </h4>
      </section>

      <!-- Main content -->
      <section class="content">
          <?=  $contents; ?>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      
      <strong>&copy; 2018   <a href="#">W.O.B. Code</a>.</strong> All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->


<!-- AdminLTE for demo purposes -->
<script src="<?= base_url().'lte/' ?>dist/js/demo.js"></script>
<script type="text/javascript">
  $( document ).ready(function() {
    var heights = $(".panel").map(function() {
        return $(this).height();
    }).get(),

    maxHeight = Math.max.apply(null, heights);

    $(".panel").height(maxHeight);
});

</script>
</body>


<!-- Mirrored from adminlte.io/themes/AdminLTE/pages/layout/top-nav.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 14 Oct 2018 08:16:10 GMT -->
</html>
