        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    Bank Data
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Kontes</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div class="btn-group pull-right">
                                    <?php echo anchor(site_url('refkontes/create'), '<i class="fa fa-plus"></i> Tambah','class="btn purple btn-sm btn-outline"'); ?>
                                </div>
                            </div>
                        </div>
        <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-list"></i>Data</div>
                                    </div>
                                    <div class="portlet-body">
                                        
        <table class="table table-striped table-bordered table-hover" id="tb" >
            <thead>
                <tr>
                    <th width="5%">No</th>
		    <th> </th>
		    <th>Tanggal Mulai</th>
		    <th> Selesai</th>
		    <th>Tempat </th>
		    <th>Pendaftaran</th>
		    <th>Aksi</th>
                </tr>
            </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($refkontes_data as $refkontes)
            {
                ?>
                <tr>
		    <td align='center'><?php echo ++$start ?></td>
		    <td><?php echo $refkontes->nama_kontes ?></td>
		    <td><?php echo $refkontes->tanggal_mulai ?></td>
		    <td><?php echo $refkontes->tanggal_selesai ?></td>
		    <td><?php echo $refkontes->tempat_kontes ?></td>
		    <td><?php echo $refkontes->pendaftaran ?></td>
		    <td style="text-align:center" width="10%"><?php 
			echo anchor(site_url('refkontes/read/'.$refkontes->id_inc),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-success"'); 
			echo anchor(site_url('refkontes/delete/'.$refkontes->id_inc),'<i class="fa fa-trash"></i>','onclick="javasciprt: return confirm(\'apakah anda yakin hapus ?\')" class="btn btn-xs btn-danger"'); 
			?></td>
	        </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
       
    </div>
</div>
       