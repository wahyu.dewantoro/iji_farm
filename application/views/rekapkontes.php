<div class="page-bar">
    <h1 class="page-title"> <?= $nama_kontes?>
        <!-- <small>fixed footer option</small> -->
    </h1>
</div>
<div class="row  widget-row">
    <div class="col-md-3"> 
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Fish Entry</th>
                    <th width="30%">Jumlah</th>
                </tr>
            </thead>
            <tbody>
                <?php $dd=0; foreach($fish as $cc){?>
                    <tr>
                        <td><?= $cc->status_bayar ?></td>
                        <td align="center"><?= $cc->jum ?></td>
                    </tr>
                <?php $dd+= $cc->jum;} ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>Jumlah</td>
                    <td align="center"><?= $dd ;?></td>
                </tr>
            </tfoot>
        </table>
            
    </div>
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-2">
                <a href="<?= base_url().'kontes/cetakceklistikan'?>" target='_blank'>
                    <div class="portlet box green">
                        <div class="portlet-body">
                            <img width="100px" class="img-responsive" alt="Responsive image" src="<?= base_url().'icon/list.png'?>">Data Ikan        
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-2">
                <a href="<?= base_url().'kontes/cetakfotoikan'?>" target='_blank'>
                    <div class="portlet box green">
                        <div class="portlet-body">
                            <img width="100px" class="img-responsive" alt="Responsive image" src="<?= base_url().'icon/foto.png'?>">Foto Ikan        
                        </div>
                    </div>
                </a>
            </div>
     
            <div class="col-md-2">
                <a href="<?= base_url().'kontes/'?>" target='_blank'>
                    <div class="portlet box green">
                        <div class="portlet-body">
                            <img width="100px" class="img-responsive" alt="Responsive image" src="<?= base_url().'icon/blufish.png'?>">Ikan Besar       
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-2">
                <a href="<?= base_url().'welcome/nomorUnidan'?>" target='_blank'>
                    <div class="portlet box green">
                        <div class="portlet-body">
                            <img width="100px" class="img-responsive" alt="Responsive image" src="<?= base_url().'icon/angka.png'?>">Nomor Undian
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-2">
                <a href="<?= base_url().'laporan/cetaktandaterimaserti'?>" target='_blank'>
                    <div class="portlet box green">
                        <div class="portlet-body">
                            <img width="100px" class="img-responsive" alt="Responsive image" src="<?= base_url().'icon/tandaterima.png'?>">Tanda terima serti
                        </div>
                    </div>
                </a>
            </div>

        </div>
    </div>
    
    
</div>
<hr>
<div class="row  widget-row">
    <div class="col-md-12">
        <div class="table-responsive">
        	 <table class="table table-striped table-bordered">
               <thead>
                    <tr>
                        <th colspan="2">Variety</th>
                        
                        <th>10 BU</th>
                        <th>15 BU</th>
                        <th>20 BU</th>
                        <th>25 BU</th>
                        <th>30 BU</th>
                        <th>35 BU</th>
                        <th>40 BU</th>
                        <th>45 BU</th>
                        <th>50 BU</th>
                        <th>55 BU</th>
                        <!-- <th>60 BU</th>
                        <th>65 BU</th> -->
                        <!-- <th>70 BU</th>
                        <th>75 BU</th>
                        <th>80 BU</th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($rekap as $rekap){ ?>
                        <tr>
                            <td><?= $rekap->kat_ikan ?></td>
                            <td><?= $rekap->nm_ikan ?></td>
                            <td <?php if($rekap->A_BU!=null ){ echo "style='background-color:#b3ccf2;'"; }?>><?= $rekap->A_BU ?></td>
                            <td <?php if($rekap->B_BU!=null ){ echo "style='background-color:#b3ccf2;'"; }?>><?= $rekap->B_BU ?></td>
                            <td <?php if($rekap->C_BU!=null ){ echo "style='background-color:#b3ccf2;'"; }?>><?= $rekap->C_BU ?></td>
                            <td <?php if($rekap->D_BU!=null ){ echo "style='background-color:#b3ccf2;'"; }?>><?= $rekap->D_BU ?></td>
                            <td <?php if($rekap->E_BU!=null ){ echo "style='background-color:#b3ccf2;'"; }?>><?= $rekap->E_BU ?></td>
                            <td <?php if($rekap->F_BU!=null ){ echo "style='background-color:#b3ccf2;'"; }?>><?= $rekap->F_BU ?></td>
                            <td <?php if($rekap->G_BU!=null ){ echo "style='background-color:#b3ccf2;'"; }?>><?= $rekap->G_BU ?></td>
                            <td <?php if($rekap->H_BU!=null ){ echo "style='background-color:#b3ccf2;'"; }?>><?= $rekap->H_BU ?></td>
                            <td <?php if($rekap->I_BU!=null ){ echo "style='background-color:#b3ccf2;'"; }?>><?= $rekap->I_BU ?></td>
                            <td <?php if($rekap->J_BU!=null ){ echo "style='background-color:#b3ccf2;'"; }?>><?= $rekap->J_BU ?></td>
                            <!-- <td <?php if($rekap->K_BU!=null ){ echo "style='background-color:#b3ccf2;'"; }?>><?= $rekap->K_BU ?></td>
                             <td <?php if($rekap->L_BU!=null ){ echo "style='background-color:#b3ccf2;'"; }?>><?= $rekap->L_BU ?></td> -->
                            <!-- <td><?= $rekap->M_BU ?></td>
                            <td><?= $rekap->N_BU ?></td>  -->
                        </tr>
                    <?php } ?>
                </tbody>
             </table>
         </div>
    </div>
</div>                    
