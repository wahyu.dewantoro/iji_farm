<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Detail Ikan</h4>
  </div>
  <div class="modal-body">
  		<p class="text-center">
  			<img style="max-height:150px; margin: auto;"   src="<?php echo base_url().$rk->gambar_ikan?>">	
  			<table style="margin:auto;">
  				<tr>
  					<td colspan="2" align="center"><?= $rk->nm_ikan?></td>
  				</tr>
  				<tr>
  					<td align="center" width="50%">
  						<span class="badge"><?= $rk->no_ikan?></span>
  					</td>
  					<td align="center">
  						<?= $rk->ukuran?> cm
  					</td>
  				</tr>
  				<tr>
  					<td align="center">
  						<?= $rk->asal?>
  					</td>
  					<td align="center">
  						<?= $rk->gender?>
  					</td>
  				</tr>
  				<tr>
  					<td colspan="2" align="center">
  						Handling: <?= $rk->nama_handling?> - <?= $rk->kota_handling?> <br>
  						Owner :<?= $rk->nama_peserta?> - <?= $rk->kota_peserta?>
  						
  					</td>
  				</tr>
  			</table>
  		</p>
  </div>
</div>