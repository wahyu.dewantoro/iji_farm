<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            Kontes
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Form Cetak Data</span>
        </li>
    </ul>
    <div class="page-toolbar">
      
    </div>
</div>
<div class="row">
    <div class="col-md-4">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-list"></i>List Ikan</div>
                </div>
                <div class="portlet-body">
                    <form target="_blank" action="<?= base_url().'kontes/cetaklistikanbyfilter'; ?>" method="get">
                        <div class="form-group">
                          <label>Kelas Variety</label>
                          <select name="kelas" class="form-control">
                            <option value="">Semua kelas</option>
                            <?php foreach($kelas as $kelas){?>
                              <option value="<?= $kelas?>"><?= $kelas?></option>
                            <?php } ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Ukuran Minimal</label>
                          <input type="number" name="ukuran_min" class="form-control">
                        </div>
                        <div class="form-group">
                          <label>Ukuran Max</label>
                          <input type="number" name="ukuran_max" class="form-control">
                        </div>
                        <div class="form-group">
                          <label>Breder</label>
                          <select class="form-control" name="breder">
                            <option value="">Semua</option>
                            <option value="lokal">Lokal</option>
                            <option value="import">Import</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Gender</label>
                          <select class="form-control" name="gender">
                            <option value="">Semua</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                          </select>
                        </div>
                        <button value="btn btn-sm btn-success"><i class="fa fa-print"></i> Cetak</button>
                    </form>
                </div>
            </div>
      </div>
    <div class="col-md-4">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-list"></i>Foto Ikan</div>
                </div>
                <div class="portlet-body">
                    <form target="_blank" action="<?= base_url().'kontes/cetakfotoikan'; ?>" method="get">
                        <div class="form-group">
                          <label>Kelas Variety</label>
                          <select name="kelas" class="form-control">
                            <option value="">Semua kelas</option>
                            <?php foreach($kelass as $kelass){?>
                              <option value="<?= $kelass?>"><?= $kelass?></option>
                            <?php } ?>
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Ukuran Minimal</label>
                          <input type="number" name="ukuran_min" class="form-control">
                        </div>
                        <div class="form-group">
                          <label>Ukuran Max</label>
                          <input type="number" name="ukuran_max" class="form-control">
                        </div>
                        <div class="form-group">
                          <label>Breder</label>
                          <select class="form-control" name="breder">
                            <option value="">Semua</option>
                            <option value="lokal">Lokal</option>
                            <option value="import">Import</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Gender</label>
                          <select class="form-control" name="gender">
                            <option value="">Semua</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                          </select>
                        </div>
                        <button value="btn btn-sm btn-success"><i class="fa fa-print"></i> Cetak</button>
                    </form>
                </div>
            </div>
      </div>
      
</div>

