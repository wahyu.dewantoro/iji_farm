<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            Data Kontes
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Data Ikan</span>
        </li>
    </ul>
    <div class="page-toolbar">
         
    </div>
</div>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-list"></i>Data</div>
    </div>
    <div class="portlet-body">
     
        <table class="table table-striped table-bordered table-hover" id="tb" >
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th>No Pendaftaran</th>
                    <th>No Ikan</th>

                    <th>Variety</th>
                    <th>Ukuran</th>
                    <th>Gender</th>
                    <th>Asal  </th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php $no=1; foreach($rk as $rk){?>
                <tr>
                    <td align="center"><?php echo $no?></td>
                    <td><?php echo $rk->no_pendaftaran;?></td> 
                    <td align="center"><span class="badge">   <?php echo $rk->no_ikan;?> </span></td>
                    
                    <td><?php echo $rk->nm_ikan;?></td>
                    <td><?php echo $rk->ukuran;?> cm</td>
                    <td><?php echo $rk->gender;?></td>
                    <td><?php echo $rk->asal  ;?></td>
                    <td align="center">
                        <a class="btn btn-xs blue" href="<?php echo base_url().'kontes/viewikandetail/'.$rk->id_inc?>" data-target="#ajax" data-toggle="modal"><i class="fa fa-search"></i> </a>
                        
                        <a class="btn btn-xs purple" href="<?php echo base_url().'kontes/editikan/'.$rk->id_inc?>" data-target="#ajax" data-toggle="modal"><i class="fa fa-edit"></i> </a>
                        
                    </td>
                </tr>
                <?php $no++; }?>
            </tbody>
        </table>
       
    </div>
</div>
       
<div class="modal fade" id="ajax" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                
                <span><i class="fa fa-refresh"></i> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>

 
<script type="text/javascript">
    document.getElementById("gambar_ikan").onchange = function () {
    var reader = new FileReader();

    reader.onload = function (e) {
        // get loaded data and render thumbnail.
        document.getElementById("image").src = e.target.result;
    };

    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
};
</script>