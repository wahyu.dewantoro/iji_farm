<html>
<head>
	<title><?php echo $rk->no_kwitansi?></title>
	 <style type="text/css">
	 	.center {
		    margin-left: auto;
		    margin-right: auto;
		}
		
		#customers {
		    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		    border-collapse: collapse;
		    width: 100%;
		    font-size: 12px;
		}

		#customers td, #customers th {
		    border: 1px solid #ddd;
		    padding: 3px;
		}

		#customers tr:nth-child(even){background-color: #f2f2f2;}

		#customers tr:hover {background-color: #ddd;}

		#customers th {
		    padding-top: 3px;
		    padding-bottom: 3px;
		    text-align: center;
		    background-color: #ccffcc;
		    color: black;
		    border:0;
		}

		
		#customersas {
		    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		    border-collapse: collapse;
		    width: 100%;
		    font-size: 12px;
		}

		#customersas td, #customersas th {
		    border: 1px solid #ddd;
		    padding: 3px;
		}

		#customersas tr:nth-child(even){background-color: #f2f2f2;}

		#customersas tr:hover {background-color: #ddd;}

		#customersas th {
		    padding-top: 3px;
		    padding-bottom: 3px;
		    text-align: center;
		    background-color: #99ffcc;
		    color: black;
		    border:0;
		}
	 </style>
</head>
<body>
 
	 <table border="0" width="100%" >
	 		<tr>
	 			<td colspan="3">
	 					 <table id="customers"  >
					       
					            <tr  >
					                <th width="5%">No</th>
					                <th>Variety</th>
					                <th>Size</th>
					                <th width="15%">L/I</th>
					                
					                <th width="15%">Prize</th>
					                
					            </tr>
					   		 
					            <?php 
					            if(count($kwitansi)>0){
					            $total=0;
					             $no=1; foreach($kwitansi as $li){?>
					            
					            <tr >
					                <td align="center"><?= $no; ?></td>
					                <td><?= $li->nm_jenis; ?>  (<small><?= $li->gender; ?></small>)</td>
					                <td align="center"><?= $li->ukuran_cm; ?> cm</td>
					                <td><?= $li->asal; ?></td>                                                            
					                
					                <td align="right"><?php echo number_format($li->biaya,'0','','.')?></td>
					                
					            </tr>
					            <?php $total+=$li->biaya;  $no++; }  } else{ $total=0;?>
					            <tr>
					               <td align="center" colspan="5">Tidak ada data !</td> 
					            </tr>
					            <?php } ?>
					         
					     </table>
	 			</td>
	 		</tr>
	 		<tr>
	 			<td colspan="3"><br><br></td>
	 		</tr>
	 		<tr>
	 			<td align="center" valign="top">
	 				<table id="customersas">
	 					<tr>
	 						<th>Plastik</th>
	 						<th>Jumlah</th>
	 					</tr>
	 					<?php foreach($pls as $pls){ ?>
	 					<tr>
	 						<td><?php echo $pls->plastik?></td>
	 						<td><?php echo $pls->jumlah?></td>
	 					</tr>
	 					<?php }?>
	 				</table>
	 			</td>
	 			<td  width="35%" valign="top" align="center">
	 				 
	 			</td>
	 			<td width="30%" align="right" valign="top">
	 				 <table border="0" cellpadding="3" cellspacing="0">
                        <tr>
                            <td>Total</td>
                            <td>:</td>
                            <td  ><?php echo number_format($total,'0','','.')?></td>
                        </tr>
                        <tr>
                        	<td colspan="3"><br><br><br></td>

                        </tr>
                        <tr>
                        	<td colspan="3" align="center">
                        			<?php echo $kota ?>, <?php echo $tanggal?>
                        			<br>
                        			<br>
                        			<br>
                        			<br>
                        			Bendahara
                        	</td>
                        </tr>
                    </table>

                    
	 			</td>
	 		</tr>
	 </table>
 

     	 

</body>
</html>