

<h3 style="text-align: center;" >Invoice 2<sup>nd</sup> ZNA Tulungagung</h3>
<?php if($peserta->no_kwitansi<>''){ echo 'No Kwitansi :'.$peserta->no_kwitansi.'<br>';}?>

No Pendaftaran : <?= $peserta->no_pendaftaran ?> ,<b>Handling</b> : <?= $peserta->nama_handling.' - '.$peserta->kota_handling ?> / <b>Owner</b> : <?= $peserta->nama_owner.' - '.$peserta->kota_owner ?>

<table width="100%" border="1" cellspacing="0" cellpadding="3">
	<thead>
		<tr>
			<th>#</th>
			<th>Ikan</th>
			<th>ID</th>
			<th>Keterangan</th>
			<th>Biaya</th>
		</tr>
	</thead>
	<tbody>
		<?php $no=1; $jum=0; foreach($ikan as $ri){ $jum+=$ri->nominal;?>
			<tr>
				<td align="center" width="5%"><?= $no ?></td>
				<td><?= $ri->nm_jenis?></td>
				<td align="center"><?= $ri->no_ikan ?></td>
				<td><?= $ri->ukuran.' cm , '.$ri->gender.', '.$ri->asal ?></td>
				<td style="text-align: right;" width="15%" align="right"><?= number_format($ri->nominal,0,'','.')?></td>
			</tr>
		<?php $no++;} ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4">Total</td>
			<td  style="text-align: right;"><?= number_format($jum,0,'','.')?></td>
		</tr>
	</tfoot>
</table>


	<table width="100%" class="visible-print">
		<tr>
			<td width="70%">
				<table >
					<tbody>
						<?php foreach($plastik as $plastik){?>
							<tr>
								<td  width="10%" align="left"><?= $plastik->plastik?></td>
								<td align="left">:<?= $plastik->jumlah ?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</td>
			<td align="left">
				Tulungagung, <?= date('d - m - Y'); ?><br>
				Bendahara
				<br>
				<br>
				<br>
				<br>
				Panitia
			</td>
		</tr>
	</table>
 