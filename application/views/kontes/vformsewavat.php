 <div class="modal-content">
  <form method="post" class="form-horizontal" action="<?php echo $action?>">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title">Sewa Vat</h4>
    </div>
    <div class="modal-body">
          <div class="form-group">
              <label class="control-label col-md-3">Jenis Vat<small>*</small></label>
              <div class="col-md-9">
                <!-- <input type="text"> -->
                <select  name="jenis" id="jenis" required class="form-control select2">
                    <option value=""></option>
                    <?php $r=array('sewa','bawa pulang');
                    foreach($r as $r){?>
                      <option value="<?= $r ?>"><?= $r ?></option>
                    <?php } ?>
                     </select>
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-3">Penyewa <small>*</small></label>
              <div class="col-md-9">
                <input type="text" name="penyewa" id="penyewa" required class="form-control" value="<?php echo $penyewa?>">
              </div>
          </div>
         
          <div class="form-group">
              <label class="control-label col-md-3">Jumlah  <small>*</small></label>
              <div class="col-md-9">
                <input type="number" name="jumlah" class="form-control" value="<?= $jumlah?>" required>
              </div>
          </div>
    </div>
    <input type="hidden" name="id_inc" value="<?php echo $id_inc?>">
    <div class="modal-footer">
      <button type="submit"  class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Submit</button>
    </div>
  </form>
</div> 
<script src="<?php echo base_url()?>assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>