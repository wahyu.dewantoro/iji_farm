<html>
<head>
	<title>Kartu ikan</title>
	 <style type="text/css">
	 	.center {margin-left: auto; margin-right: auto; }
	 	.responsive {width: auto; /*max-width: */ height: 150px; }
	 </style>

</head>
<body>

 	<h3 style="text-align: center;">2<sup>nd</sup> ZNA Tulungagung Koi Show 2019<br>Kartu Ikan</h3>
 	<small><b>#<?= $rk->no_pendaftaran ?> /  <?= $rk->handling ?> , <?= $rk->owner ?></b></small>
	 <?php 
			$kolom  = 5; 
			$gambar = array_chunk($rn,$kolom);
		echo '<table  cellpadding="10">';
		foreach ($gambar as $chunk) {
		    echo '<tr >';
		    foreach ($chunk as $ikan) {
		        echo '<td valign="top" width="100px">';?>
		        <table  style=" font-size:12px">
		        	<tr>
		        		<td valign="top"   ><img class="responsive" src="<?php echo base_url().$ikan->gambar_ikan?>"></td>
		        	</tr>
		        	<tr>
		        		<td>No Ikan : <b><?php echo  $ikan->no_ikan //sprintf("%04d",$ikan->no_ikan)?></b></td>
		        	</tr>
		        	<tr>
		        		<td  valign="top" ><?php echo $ikan->nm_ikan?> / <?php echo $ikan->ukuran?> cm </td>
		        	</tr>
		        	<tr>
		        		<td  valign="top" ><?php echo $ikan->gender?> / <?php echo $ikan->asal?> </td>
		        	</tr>
		        </table>
		        <?php echo '</td>';
		    }
		    echo '</tr>';
		}
		echo '</table>';

	 ?>

 

</body>

</html>