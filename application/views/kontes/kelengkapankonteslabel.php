<html>
<head>
	<title><?php echo $rk->no_kwitansi?></title>
	 <style type="text/css">
	 	.center {
		    margin-left: auto;
		    margin-right: auto;
		}
	 
		.kecil{
			font-size: 14px;
		}

	 </style>
</head>
<body>
	 <?php 
	 	$kolom = 1;
	 	$gambar = array_chunk($rn,$kolom);
		echo '<table class="center"  >';
		foreach ($gambar as $chunk) {
		    echo '<tr >';
		    foreach ($chunk as $ikan) {
		        echo '<td valign="top" align="center" >';?>
		        <table class="center"  width="100%"> 
		        	<tr>
		        		<td valign="top"  colspan="3"><img style="height:100px;width:75px;"  src="<?php echo base_url().$ikan->gambar_ikan?>"></td>
		        		<td valign="top" align="left">
		        			<table >
		        					<tr>
						        		<td valign="top" width="60px"><h1>#<?php echo  $ikan->no_ikan; ?></h1></td>
						        	</tr>
						        	<tr>
						        		<td valign="top" ><p class="kecil"><?php echo $ikan->ukuran?> cm</p> </td>
						        	</tr>
						        	<tr valign="top">
						        		<td valign="top" ><p class="kecil"><?php echo $ikan->nm_ikan?></p></td>
						        	</tr>
						        	<tr>
						        		<td valign="top" ><b class="kecil"><?php echo $ikan->gender.', '.$ikan->asal ?></b></td>
						        	</tr>
						        	
		        			</table>
		        		</td>

		        	</tr>
		        </table>
		         - - - - - - - - - - - - - - - - - - - -
		        <?php echo '</td>';
		    }
		    echo '</tr>';
		}
		echo '</table>';

	 ?>
 
</body>
</html>