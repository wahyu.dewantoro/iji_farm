<style type="text/css">
    .ribbon {
   position: absolute;
   right: 15px; 
   top: -15px;
   z-index: 1;
   overflow: hidden;
   width: 75px; height: 75px; 
   /*width: auto;*/
   text-align: right;
}
.ribbon span {
   font-size: 10px;
   color: #fff; 
   /*text-transform: capitalize; */
   text-align: center;
   font-weight: bold; 
   line-height: 20px;
   /*transform: rotate(0deg);*/
   width: 100%; 
   display: block;
   background: #79A70A;
   background: linear-gradient(#9BC90D 0%, #79A70A 100%);
   box-shadow: 0 3px 10px -5px rgba(0, 0, 0, 1);
   position: absolute;
   top: 19px; 
   /*right: -21px;*/
}
 

</style>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            Data Kontes
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Cek Foto Ikan</span>
        </li>
    </ul>
    <div class="page-toolbar">
         <div class="pull-right">
          <div class="btn-group">
              <?php echo anchor('kontes/cetakceklistikan/','<i class="fa fa-print"></i> Cetak Checklist','class="btn btn-sm btn-outline green" target="_blank"')?>
              <?php echo anchor('kontes/cetakfotoikanukuranbesar/','<i class="fa fa-print"></i> Cetak Ukuran Besar','class="btn btn-sm btn-outline purple" target="_blank"')?>
              <?php echo anchor('kontes/cetakfotoikan/','<i class="fa fa-print"></i> Cetak','class="btn btn-sm btn-outline blue" target="_blank"')?>
          </div>
             
         </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php foreach($kontes as $rk){?>
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list"></i>Kategori <?php echo $rk->kat_ikan?></div>
                </div>
                <div class="portlet-body">
                   <div class="row">
                        <div class="col-md-3">
                            <!-- left menu -->
                            <ul class="nav nav-tabs tabs-left">
                                <?php $as=1; foreach($ukuran as $ru ){ if($ru->kat_ikan==$rk->kat_ikan){?>
                                <li <?php if($as==1){echo ' class="active"';}?> >
                                    <a href="#<?php echo str_replace(' ','', $rk->kat_ikan).str_replace(' ', '', $ru->range_ukuran)?>" data-toggle="tab"> <?php echo $ru->range_ukuran?></a>
                                </li>
                                <?php $as++; } } ?>
                            </ul>
                        </div>
                        <div class="col-md-9">
                            <!-- right conten -->
                            <div class="tab-content">
                                <?php $asa=1; foreach($ukuran as $ruu){ if($ruu->kat_ikan==$rk->kat_ikan){ ?>
                                    <div class="tab-pane <?php if($asa==1){echo 'active';}?> " id="<?php echo str_replace(' ','', $rk->kat_ikan).str_replace(' ', '', $ruu->range_ukuran)?>">
                                        <!-- konten -->
                                        <div class="row">
                                            <?php foreach($ikan as $ri){  if($ri->kat_ikan==$rk->kat_ikan && $ri->range_ukuran==$ruu->range_ukuran ){?>
                                                <div class="col-md-2">
                                                     <div class="thumbnail">
                                                        <img  style="margin: auto;   height:142.5px;max-width:95px;width: expression(this.width > 95 ? 95: true);"  src="<?php echo base_url().$ri->gambar_ikan?>">
                                                       <div class="caption">
                                                            <table style="font-size:10px">
                                                                <tbody>
                                                               
                                                                    <tr>
                                                                        <td>ID : <?= $ri->no_ikan?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td ><?= $ri->nm_ikan?>,<?= $ri->ukuran?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><?= $ri->asal ?> / <?= $ri->gender?></td>
                                                                    </tr>
                                                                    
                                                                </tbody>
                                                            </table>
                                                       </div>
                                                    </div>
                                                </div>
                                            <?php } } ?>
                                        </div>
                                    </div>
                                <?php $asa++; } } ?>
                            </div>

                        </div>
                   </div>
                </div>
            </div>        
        <?php } ?>
    </div>
</div>



       