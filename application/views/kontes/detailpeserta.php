        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    Data kontes
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Data Peserta</span>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Detail</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div class="btn-group pull-right">
                                    <?php if($hand==false){?>
                                    <a class="btn btn-sm blue btn-outline" href="<?php echo base_url().'kontes/formeditpeserta/'.$rp->id_inc?>" data-target="#ajax" data-toggle="modal"><i class="fa fa-edit"></i> Edit</a>
                                    <?php 
                                }
                                    echo anchor(site_url('kontes/pesertabykontes'), '<i class="fa fa-list"></i> list','class="btn purple btn-sm btn-outline"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-list"></i>Data Peserta
                                        </div>
                                    </div>

                                    <div class="portlet-body">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>No Pendaftaran</td>
                                                    <td>:</td>
                                                    <td><?= $rp->no_pendaftaran?></td>
                                                </tr>
                                                <tr>
                                                    <td>Daftar</td>
                                                    <td>:</td>
                                                    <td><?= $rp->tanggal_daftar?></td>
                                                </tr>
                                                <tr>
                                                    <td>Handling</td>
                                                    <td>:</td>
                                                    <td><?= $rp->handling?> - <?= $rp->kota_handling?></td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>Owner</td>
                                                    <td>:</td>
                                                    <td><?= $rp->pemilik_ikan?> - <?= $rp->kota_pemilik?></td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                          
                            <div class="col-md-8">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-list"></i> Data Ikan</div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                               <div class="table-responsive">
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <td width="5%">No</td>
                                                            <td>No Ikan</td>
                                                            <td>Variety</td>
                                                            <td>Ukuran</td>
                                                            <td>Gender</td>
                                                            <td>Asal</td>
                                                            <!-- <td>Biaya</td> -->
                                                            <td width="10%">Aksi</td>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <?php 
                                                        if(count($listikan)>0){
                                                        // $total=0;
                                                         $no=1; foreach($listikan as $li){?>
                                                        
                                                        <tr>
                                                            <td align="center"><?= $no; ?></td>
                                                            <td align="center"><?= $li->no_ikan; ?></td>
                                                            <td><?= $li->nm_jenis; ?></td>
                                                            <td><?= $li->ukuran_cm; ?> cm</td>
                                                            <td><?= $li->gender; ?></td>
                                                            <td><?= $li->asal; ?></td>
                                                            
                                                            <!-- <td align="right"><?php //echo number_format($li->biaya,'0','','.')?></td> -->
                                                            <td align="center">
                                                                    <a class="btn btn-xs btn-warning" href="<?php echo base_url().'daftar/viewikan/'.$li->id_inc?>" data-target="#ajax" data-toggle="modal"><i class="fa fa-image"></i> </a>
                                                            </td>
                                                        </tr>
                                                        <?php  $no++; } } else{  ?>
                                                        <tr>
                                                           <td align="center" colspan="5">Tidak ada data !</td> 
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                   <!--  <tfoot>
                                                        <tr>
                                                            <th colspan="6">Total</th>
                                                            <td align="right"><?php //echo number_format($total,'0','','.')?></td>
                                                            <td></td>
                                                        </tr>
                                                    </tfoot> -->
                                                </table>
                                                </div>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
<div class="modal fade" id="ajax" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                
                <span><i class="fa fa-refresh"></i> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>

 
<script type="text/javascript">
    document.getElementById("gambar_ikan").onchange = function () {
    var reader = new FileReader();

    reader.onload = function (e) {
        // get loaded data and render thumbnail.
        document.getElementById("image").src = e.target.result;
    };

    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
};
</script>