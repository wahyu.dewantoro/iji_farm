<div class="page-bar">

    <ul class="page-breadcrumb">

        <li>

            Data Kontes

            <i class="fa fa-circle"></i>

        </li>

        <li>

            <span>Data Pembayaran</span>

        </li>

    </ul>

    <div class="page-toolbar">

        <button class="btn btn-sm btn-primary" id="bayar_banyak"><i class="fa fa-money"></i> Bayar</button>

        <button class="btn btn-sm btn-primary" id="kartu"><i class="fa fa-file-text"></i> Kartu</button>

        <button class="btn btn-sm btn-success" id="cetak_banyak"><i class="fa fa-print"></i> kwitansi</button>

         <?php echo anchor('kontes/pembayaran','<i class="fa fa-refresh"></i> Refresh','class="btn btn-sm purple btn-outline"');?>

    </div>

</div>

<!-- Nav tabs -->

<ul class="nav nav-tabs portlet box green">

  <li class="active"><a href="#tagihan" data-toggle="tab">Tagihan</a></li>

  <li><a href="#payletter" data-toggle="tab">Utang</a></li>

  <li><a href="#bayar" data-toggle="tab">Lunas</a></li>

</ul>

<!-- Tab panes -->

<div class="tab-content portlet-body">

  <div class="tab-pane active" id="tagihan">

    <table class="table table-striped table-bordered table-hover tb">

        <thead>

            <tr>

                <th rowspan="2" width="5%">No</th>

                <th rowspan="2">No Pendaftaran</th>

                <th colspan="2">Handling</th>

                <th colspan="2">Owner</th>

                <th rowspan="2">Ikan</th>

                <th rowspan="2">Total</th>

                <th rowspan="2" width="15%"></th>

            </tr>

            <tr>

                <th>Nama</th>

                <th>Kota</th>

                <th>Nama</th>

                <th>Kota</th>

            </tr>

        </thead>

        <tbody>

            <?php $no=1; foreach($tagihan as $rt){?>

                <tr  id="<?= $rt->no_pendaftaran ?>">

                    <td align="center"><input type="checkbox" id="cekbox" name="cekbox[]" value="<?= $rt->no_pendaftaran ?>"/> <?= $no ?></td>

                    <td><a href="#"> <?= $rt->no_pendaftaran ?></a><span class="badge <?php if($rt->st_entri=='online'){ echo " badge-success";}else{echo " badge-primary";}?>"><?= $rt->st_entri ?></span></td>

                    <td><?= $rt->nama_handling ?></td>

                    <td><?= $rt->kota_handling ?></td>

                    <td><?= $rt->nama_owner?></td>

                    <td><?= $rt->kota_owner?></td>

                    <td><?= $rt->ikan?></td>

                    <td align="right"><?= number_format($rt->jumlah,0,'','.');?></td>

                    <td align="center">

                        <!-- <div class="btn-group"> -->

                            <button data-id="<?= $rt->no_pendaftaran ?>" class="bayar btn btn-xs btn-success"><i class="fa fa-money"></i></button>

                            <?= anchor('kontes/ngutang/'.$rt->no_pendaftaran,'<i class="fa fa-handshake-o"></i>','class="btn btn-xs btn-warning" onclick="return confirm(\'Yakin utang?\')"');?>

                            <button data-id="<?= $rt->no_pendaftaran ?>" class="cetak btn btn-xs btn-info"><i class="fa fa-print"></i></button>

                            <?=  anchor('kontes/kelengkapankontes/'.$rt->no_pendaftaran,'<i class="fa fa-file-text"></i>','class="btn btn-xs btn-warning" data-togle="tooltips" title="Kartu lomba" target="_blank"') ; ?>

                        <!-- </div> -->

                    </td>

                </tr>

            <?php $no++; } ?>

        </tbody>

    </table>



  </div>

  <div class="tab-pane" id="payletter">

        

        <table width="100%" class="table table-striped table-bordered table-hover tb">

            <thead>

                <tr>

                    <th rowspan="2" width="5%">No</th>

                    <th rowspan="2">No Pendaftaran</th>

                    <th colspan="2">Handling</th>

                    <th colspan="2">Owner</th>

                    <th rowspan="2">Ikan</th>

                    <th rowspan="2">Total</th>

                    <th rowspan="2" width="15%"></th>

                </tr>

                <tr>

                    <th>Nama</th>

                    <th>Kota</th>

                    <th>Nama</th>

                    <th>Kota</th>

                </tr>

            </thead>

            <tbody>

                <?php $ano=1; foreach($utang as $rtu){?>

                    <tr id="<?= $rtu->no_pendaftaran ?>">

                        <td align="center">

                            <input type="checkbox" id="cekbox" name="cekbox[]" value="<?= $rtu->no_pendaftaran ?>"/>

                            <?= $ano ?></td>

                        <td><a href="#"><?= $rtu->no_pendaftaran ?></a><span class="badge <?php if($rtu->st_entri=='online'){ echo " badge-success";}else{echo " badge-primary";}?>"><?= $rtu->st_entri ?></span></td>

                        <td><?= $rtu->nama_handling ?></td>

                        <td><?= $rtu->kota_handling ?></td>

                        <td><?= $rtu->nama_owner?></td>

                        <td><?= $rtu->kota_owner?></td>

                        <td><?= $rtu->ikan?></td>

                        <td align="right"><?= number_format($rtu->jumlah,0,'','.');?></td>

                        <td>

                            <div class="btn-group">

                                <button data-id="<?= $rtu->no_pendaftaran ?>" class="bayar btn btn-xs btn-success"><i class="fa fa-money"></i></button>

                                <button data-id="<?= $rtu->no_pendaftaran ?>" class="cetak btn btn-xs btn-info"><i class="fa fa-print"></i></button>

                            </div>

                        </td>

                    </tr>

                <?php $ano++; } ?>

            </tbody>

        </table>

  </div>

  <div class="tab-pane" id="bayar">

    <table width="100%" class="table table-striped table-bordered table-hover tb">

            <thead>

                <tr>

                    <th>#</th>

                    <th>Keterangan</th>

                    <th>NO Kwitansi</th>

                    <th>Ikan</th>

                    <th>Nominal</th>

                    <th></th>

                </tr>

             

            </thead>

            <tbody>

                <?php $anao=1; foreach($lunas as $rtuo){?>

                    <tr  id="<?= $rtuo->no_kwitansi ?>">

                        <td align="center">

                            <?= $anao ?>

                        </td>

                        <td><?= $rtuo->keterangan?></td>

                        <td><?= $rtuo->no_kwitansi?></td>

                        <td align="center"><?= $rtuo->ikan?></td>

                        <td align="right"><?= number_format($rtuo->jumlah,0,'','.') ?></td>

                        <td>

                            <div class="btn-group">

                                <button data-id="<?= $rtuo->no_kwitansi ?>"  class="kwitansi_lampiran btn btn-xs btn-success"><i class="fa fa-print"></i></button>

                                <button data-id="<?= $rtuo->no_kwitansi ?>"  class="kwitansi_bayar btn btn-xs btn-info"><i class="fa fa-file-text"></i></button>

                            </div>

                        </td>

                        

                    </tr>

                <?php $anao++; } ?>

            </tbody>

        </table>

  </div>

</div>



 <!-- Modal -->

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

            <div class="modal-dialog">

                <div class="modal-content">

                    <form method="post" action="#">

                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                            <h4 class="modal-title" id="myModalLabel">Form pembayaran</h4>

                        </div>

                        <div class="modal-body" id="modal-body">

                        </div>

                        <div class="modal-footer">

                            

                            <button type="button" id="submitBayar" class="simpan btn btn-primary">Bayar</button>

                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>

                        </div>

                    </form>

                </div>

            </div>

        </div>

        <div class="modal fade" id="myModalbanyak" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

            <div class="modal-dialog">

                <div class="modal-content">

                    <form method="post" action="#">

                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                            <h4 class="modal-title" id="myModalLabel">Form pembayaran</h4>

                        </div>

                        <div class="modal-body" id="modal-bodybanyak">

                        </div>

                        <div class="modal-footer">

                            <button type="button" id="submitBayarbanyak" class="simpan btn btn-primary">Bayar</button>

                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>

                        </div>

                    </form>

                </div>

            </div>

        </div>





        <div  class="visible-print" id="cetak"></div>





 <script type="text/javascript">

     $( document ).ready(function() {

            $(document).on('click','#bayar_banyak',function(e){

                e.preventDefault();

                var val = [];

                $(':checkbox:checked').each(function(i){

                  val[i] = $(this).val();

                });



                if(val!=''){

                    // alert(val);



                    $("#modal-bodybanyak").html('');

                    $("#myModalbanyak").modal('show');

                    $.post("<?= base_url().'kontes/formbayarbanyak'?>",

                        {nomer:val},

                        function(html){

                            $("#modal-bodybanyak").html(html);

                        }

                    );



                }

            });





            $(document).on('click','.bayar',function(e){

                e.preventDefault();

                $("#modal-body").html('');

                $("#myModal").modal('show');

                $.post("<?= base_url().'kontes/formbayar'?>",

                    {nomer:$(this).attr('data-id')},

                    function(html){

                        $("#modal-body").html(html);

                    }

                );

            });

      

        

        $(document).on('click','.kwitansi_bayar',function(e){

                e.preventDefault();

                $("#cetak").html('');

                $.get("<?= base_url().'kontes/cetakkwitansi_bayar'?>",

                    {no:$(this).attr('data-id')},

                    function(resw){

                        $("#cetak").html(resw).hide();

                        printDiv('cetak');

                    }

                );

        });



        $(document).on('click','.cetak',function(e){

                e.preventDefault();

                $("#cetak").html('');

                $.get("<?= base_url().'kontes/cetakkwitansi'?>",

                    {no:$(this).attr('data-id')},

                    function(resw){

                        $("#cetak").html(resw).hide();

                        printDiv('cetak');

                    }

                );

        });





        $(document).on('click','#cetak_banyak',function(e){

            e.preventDefault();

            $("#cetak").html('');



            var val = [];

            $(':checkbox:checked').each(function(i){

              val[i] = $(this).val();

            });



            if(val!=''){

                valu=val.join();

                $.get("<?= base_url().'kontes/cetakkwitansibanyak'?>",

                    {no:valu},

                    function(resw){

                        $("#cetak").html(resw).hide();

                        printDiv('cetak');

                    }

                );

            }

        });

        



        

        $(document).on('click','.kwitansi_lampiran',function(e){

            e.preventDefault();

            $("#cetak").html('');



            $.get("<?= base_url().'kontes/cetakkwitansibanyak_lampiran'?>",

                    {no:$(this).attr('data-id')},

                    function(resw){

                        $("#cetak").html(resw).hide();

                        printDiv('cetak');

                    }

                );

 

        });



        $(document).on('click','#submitBayar',function(e){

                e.preventDefault();

                nomer =$('#no_pendaftaran').val()

                idn   ='#'+nomer;

                $("#cetak").html('');

                $("#myModal").modal('hide');

                $.post("<?= base_url().'kontes/aksibayarkontes'?>",

                    {no_pendaftaran:$('#no_pendaftaran').val()},

                    function(resw){

                            if(resw==0){

                                alert('Data tidak di proses!');

                            }else if(resw==2){

                                alert('Data tidak di temukan!');

                            }else{

                                $(idn).hide();

                                $("#cetak").html(resw).hide();

                                printDiv('cetak');

                            }

                    }

                );

            });



        $(document).on('click','#submitBayarbanyak',function(e){

                e.preventDefault();

               

                var valu = [];

                $('input[name="no_pendaftaran_banyak[]"]').each(function(i){

                  valu[i] = $(this).val();

                });

                $("#cetak").html('');

                $("#myModalbanyak").modal('hide');

                $.post("<?= base_url().'kontes/aksibayarkontesbanyak'?>",

                    {no_pendaftaran:valu},

                    function(resw){

                            if(resw==0){

                                alert('Data tidak di proses!');

                            }else if(resw==2){

                                alert('Data tidak di temukan!');

                            }else{

                                // 

                                $('input[name="no_pendaftaran_banyak[]"]').each(function(i){

                                  idn ='#'+$(this).val();

                                  $(idn).hide();



                                });

                                $("#cetak").html(resw).hide();

                                printDiv('cetak');

                            }

                    }

                );



           

            });



         $(document).on('click','#kartu',function(e){

                e.preventDefault();

                var val = [];

                $(':checkbox:checked').each(function(i){

                  val[i] = $(this).val();

                });



                if(val!=''){

                    // alert(val);

                    var url = "<?php echo base_url() ; ?>"+"kontes/kelengkapankontesbanyak?nomer="+val;

                    window.open(url, '_blank');

                }

            });

    });



    function printDiv(divName){

        var printContents = document.getElementById(divName).innerHTML;

        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;

    }      



/*  window.onafterprint = function(){

      window.location.reload(true);

 }*/

 </script>



