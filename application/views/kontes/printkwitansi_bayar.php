<style type="text/css">
	#watermark {
        position: fixed;
        text-align: center;
        /*bottom:   27cm;*/
        left:     5cm;
        font-size: 100px;
        opacity: 0.1;
        /** Change image dimensions**/
/*        width:    8cm;
        height:   8cm;
*/
        /** Your watermark should be behind every content**/
        z-index:  -1000;
           transform: rotate(0deg);
    }
</style>
<h2 style="text-align: center;" >2<sup>nd</sup> ZNA Tulungagung Koi Show 2019</h2><hr>
<h3  style="text-align: center;"><b>Bukti Pembayaran</b></h3>
<div id="watermark">
 <p>ASLI</p>           
 </div>
<table border="0">
	<tr valign="top">
		<td>No Kwitansi</td>
		<td>: <?= $rk->no_kwitansi?></td>
	</tr>
	<tr valign="top">
		<td>Tanggal</td>
		<td>: <?= longdate_indo(date('Y-m-d',strtotime($rk->tgl_bayar)))?></td>
	</tr>
	<tr valign="top">
		<td>Terima dari</td>
		<td>: <?= $rk->keterangan?></td>
	</tr>
	<tr valign="top">
		<td>Keterangan</td>
		<td>: biaya kontes sebanyak  <?= $rk->ikan ?> ekor </td>
	</tr>
	<tr valign="top">
		<td>Jumlah</td>
		<td>: Rp. <?= number_format($rk->jumlah,0,'','.')?></td>
	</tr>
	<tr valign="top">
		<td>Terbilang</td>
		<td>: <b><?= terbilang($rk->jumlah)  ?> Rupiah</b></td>
	</tr>
</table>
<table width="100%">
	<tr valign="bottom">
		<td width="50%">
			<i>Noted: Kwitansi sah apabila di terdapat tanda tangan panitia dan  ber 	stempel.
			</i>
		</td>
		<td align="center">
			Tulungagung, <?= date_indo(date('Y-m-d'))?><br>
			Panitia<br><br><br>

			Bendahara
		</td>
	</tr>
</table>
<br><br><br>
<h3 style="text-align: center;">------------------------------------------------------------------------------------</h3>
<br>

<h2 style="text-align: center;" >2<sup>nd</sup> ZNA Tulungagung Koi Show 2019</h2><hr>
<h3  style="text-align: center;"><b>Bukti Pembayaran</b></h3>
<div id="watermark">
 <p>COPY</p>           
 </div>
<table border="0">
	<tr valign="top">
		<td>No Kwitansi</td>
		<td>: <?= $rk->no_kwitansi?></td>
	</tr>
	<tr valign="top">
		<td>Tanggal</td>
		<td>: <?= longdate_indo(date('Y-m-d',strtotime($rk->tgl_bayar)))?></td>
	</tr>
	<tr valign="top">
		<td>Terima dari</td>
		<td>: <?= $rk->keterangan?></td>
	</tr>
	<tr valign="top">
		<td>Keterangan</td>
		<td>: biaya kontes sebanyak  <?= $rk->ikan ?> ekor </td>
	</tr>
	<tr valign="top">
		<td>Jumlah</td>
		<td>: Rp. <?= number_format($rk->jumlah,0,'','.')?></td>
	</tr>
	<tr valign="top">
		<td>Terbilang</td>
		<td>: <b><?= terbilang($rk->jumlah)  ?> Rupiah</b></td>
	</tr>
</table>
<table width="100%">
	<tr valign="bottom">
		<td width="50%">
			<i>Noted: Kwitansi sah apabila di terdapat tanda tangan panitia dan  ber 	stempel.
			</i>
		</td>
		<td align="center">
			Tulungagung, <?= date_indo(date('Y-m-d'))?><br>
			Panitia<br><br><br>

			Bendahara
		</td>
	</tr>
</table>