<!DOCTYPE html>
<html>
<head>
	<title></title>
	 <style type="text/css">
	 	.center {
		    margin-left: auto;
		    margin-right: auto;
		}
		
		#customers {
		    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		    border-collapse: collapse;
		    width: 100%;
		    font-size: 12px;
		}

		#customers td, #customers th {
		    border: 1px solid #ddd;
		    padding: 3px;
		}

		#customers tr:nth-child(even){background-color: #f2f2f2;}

		#customers tr:hover {background-color: #ddd;}

		#customers th {
		    padding-top: 3px;
		    padding-bottom: 3px;
		    text-align: center;
		    background-color: #ccffcc;
		    color: black;
		    border:0;
		}

		
		#customersas {
		    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		    border-collapse: collapse;
		    width: 100%;
		    font-size: 12px;
		}

		#customersas td, #customersas th {
		    border: 1px solid #ddd;
		    padding: 3px;
		}

		#customersas tr:nth-child(even){background-color: #f2f2f2;}

		#customersas tr:hover {background-color: #ddd;}

		#customersas th {
		    padding-top: 3px;
		    padding-bottom: 3px;
		    text-align: center;
		    background-color: #99ffcc;
		    color: black;
		    border:0;
		}
	 </style>
</head>
<body>
	 <table id="customers"  >
	 	   <thead>
                <tr>
                    <th width="5%">No</th>
                    <th>Kwitansi</th>
                    <th>Handling</th>
                    <th>Pemilik</th>
                    <th>Ikan</th>
                    <th>Status</th>
                    <th>Biaya</th>
                    
                    
                </tr>
            </thead>
            <tbody>
                <?php $ja=0; $jl=0; $jbb=0; $no=1; foreach($rk as $rk){?>
                <tr>
                    <td align="center"><?php echo $no?></td>
                    <td align="center"><?php echo $rk->no_kwitansi;?> </td>
                    <td><?php echo $rk->handling;?> </td>
                    <td><?php echo $rk->pemilik_ikan;?> </td>
                    <td align="center"><?php echo $rk->ikan;?> </td>
                    
                    <td align="center">
                        <div id="label<?php echo $rk->id_inc?>"> 
                        <?php if($rk->kd_bayar=='1'){
                                echo "Lunas";
                                $jl+=$rk->biaya;
                                $jbb+=0;
                            }else{
                                $jl+=0;
                                $jbb+=$rk->biaya;
                                echo "-";
                                } ?>
                        </div>
                    </td>
                    <td align="right"><?php echo  number_format($rk->biaya,'0','','.');?> </td>
                </tr>
                <?php $no++; $ja+=$rk->biaya;  }?>
            </tbody>
              <tfoot>
            <tr>
                <td align=" " colspan="6">Jumlah Lunas</td>
                <td align="right"><?php echo number_format($jl,'0','','.');?></td>
             
            </tr>
            <tr>
                <td align=" " colspan="6">Jumlah Belum Lunas</td>
                <td align="right"><?php echo number_format($jbb,'0','','.');?></td>
             
            </tr>
            <tr>
                <td align=" " colspan="6"><b>Total</b></td>
                <td align="right"><b><?php echo number_format($ja,'0','','.');?></b></td>
             
            </tr>
        </tfoot>
	 </table>
</body>
</html>