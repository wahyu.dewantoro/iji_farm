<div class="register-box-body">
    <p class="login-box-msg">Daftar akun handling</p>
    <?= $this->session->flashdata('msg') ?>
    <form action="<?= base_url().'auth/regishandingshow'?>" method="post">
      <div class="form-group has-feedback">
        <span class="fa fa-user form-control-feedback"></span>
        <input type="text" name="nama" class="form-control" id="nama" required="" placeholder="Nama Handling">
        
      </div>
      <div class="form-group has-feedback">
        <span class="fa fa-building form-control-feedback"></span>
        <input type="text" name="kota" class="form-control" id="kota" required="" placeholder="Kota">
        
      </div>
      <div class="form-group has-feedback">
        <span class="fa fa-phone form-control-feedback"></span>
        <input type="number" name="telp" class="form-control" id="telp" required="" placeholder="Nomor HP">
        
      </div>
      <div class="form-group has-feedback">
        <span class="fa fa-lock form-control-feedback"></span>
        <input type="password" name="password" id="password" required="" class="form-control" placeholder="Password">
        
      </div>
      <div class="form-group has-feedback">
        
        <span class="fa fa-refresh form-control-feedback"></span>
        <input type="password" name="ulangi_password" id="ulangi_password" required="" class="form-control" placeholder="Retype password">
        
      </div>
      <div class="row">
        <div class="col-xs-6">
          <div class="checkbox icheck">
            <label>
              <a href="<?= base_url().'auth'?>" class="text-center"><i class="fa fa-sign-in"></i> Login</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-edit"></i> Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    
  </div>
  <!-- /.form-box -->
</div>


