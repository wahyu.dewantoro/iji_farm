<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Kelas yang diperlombakan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong style="text-align: center;"><i class="fa fa-book margin-r-5"></i>Kelas A</strong>

              <ul>
              <?php $a=explode(',',$ikan->a); for($aa=0;$aa<count($a);$aa++){echo "<li>".$a[$aa]."</li>";}  ?>
              </ul>
              <hr>
              <strong style="text-align: center;"><i class="fa fa-book margin-r-5"></i>Kelas B</strong>
              <ul>
              <?php $b=explode(',',$ikan->b); for($bb=0;$bb<count($b);$bb++){echo "<li>".$b[$bb]."</li>";}  ?>
              </ul>
              <hr>
              <strong style="text-align: center;"><i class="fa fa-book margin-r-5"></i>Kelas C</strong>
			<?php $c=explode(',',$ikan->c);  echo '<ul>';for($cc=0;$cc<count($c);$cc++){echo "<li>".$c[$cc]."</li>";} echo '</ul>'; ?>

              <hr>
              <strong style="text-align: center;"><i class="fa fa-book margin-r-5"></i>Kelas D</strong>

              <p class="">
                <?php $d=explode(',',$ikan->d);  echo '<ul>';for($dd=0;$dd<count($d);$dd++){echo "<li>".$d[$dd]."</li>";} echo '</ul>'; ?>
              </p>

              <hr>
              <strong style="text-align: center;"><i class="fa fa-book margin-r-5"></i>Kelas E</strong>

              <p class="">
                <?php $e=explode(',',$ikan->e);  echo '<ul>';for($ee=0;$ee<count($e);$ee++){echo "<li>".$e[$ee]."</li>";} echo '</ul>'; ?>
              </p>
            </div>
            <!-- /.box-body -->
          </div>
<div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Biaya Pendaftaran</h3>
            </div>
            <div class="box-body">
            	<table class="table table-bordered">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>Ukuran</th>
                          <th>Biaya</th>
                      </tr>

                  </thead>
                  <tbody>
                      <?php $no=1; foreach($biaya as $rb){?>
                      <tr <?php if($no%2 == 0){ echo "class='danger'";}else{echo "class='warning'";}?> >
                          <td align="center"><?= $no?></td>
                          <td><?php echo $rb->ukuran?></td>
                          <td align="right"><?php echo number_format($rb->biaya,'0','','.')?></td>
                      </tr>
                      <?php $no++;}?>
                  </tbody>
              </table>    
            </div>
</div>
