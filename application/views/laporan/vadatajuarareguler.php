<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            Report
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Reguler Winner</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
          <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Data</div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="tb" >
                    <thead>
                        <tr>
                            <th width ="5%">No</th>
                            <th>ID Ikan</th>
                            <th>Variety</th>
                            <th>Size</th>
                            <th>Prize</th>
                           
                            <th width="5%">Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $start = 0;
                        foreach ($rk as $rk)
                        {
                            ?>
                            <tr>
                                <td align='center'><?php echo ++$start ?></td>
                                <td><?= $rk->no_ikan ?></td>
                                <td><?= $rk->nm_ikan ?></td>
                                <td><?= $rk->ukuran ?></td>
                                <td><?= ucwords($rk->prize) ?></td>
                                <td align="center"><a data-target="#ajax" data-toggle="modal" href="<?php echo base_url().'kontes/viewikandetail/'.$rk->id_ikan?>" class="btn btn-xs purple"><i class="fa fa-search"></i></a></td>
                            </tr>
                            <?php   } ?>
                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ajax" role="basic" aria-hidden="true">
    <div class="modal-dialog  ">
        <div class="modal-content">
            <div class="modal-body">
                
                <span><i class="fa fa-refresh"></i> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>