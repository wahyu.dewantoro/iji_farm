<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            Report
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Sertifikat</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="pull-right">
                <a href="<?php echo base_url('laporan/cetaktandaterimaserti')?>"   class="btn btn-sm green"><i class="fa fa-file-text"></i>Tanda Terima</a>
                <a href="<?php echo base_url('laporan/cetaklabelserti')?>" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-file-text"></i> Label</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
          <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Data</div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="tb" >
                    <thead>
                        <tr>
                            <th width ="5%">No</th>
                            <th>Handling</th>
                            <th>Kota</th>
                            <th>Ikan</th>
                            <th width="10%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $start = 0;
                        foreach ($rn as $rk)
                        {
                            ?>
                            <tr>
                                <td align='center'><?php echo ++$start ?></td>
                                <td><?php echo $rk->handling ?></td>
                                <td><?php echo $rk->kota ?></td>
                                <td align="center"><?php echo $rk->jumlah ?> ekor</td>
                                <td align="center"><?php echo anchor('laporan/cetaksertifikat/'.urlencode(urlencode($rk->handling)),'<i class="fa fa-print"></i> Cetak','target="_blank" class="btn btn-xs btn-info"');?></td>
                            </tr>
                            <?php   } ?>
                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>