<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            Report
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Perolehan Poin Owner</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <?php  //echo anchor(site_url('laporan/cetakpoin'), '<i class="fa fa-print"></i> cetak','class="btn purple btn-sm btn-outline"'); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
          <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Data</div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="tb" >
                    <thead>
                        <tr>
                            <th width ="5%">No</th>
                            <th>Owner</th>
                            <th>Kota</th>
                            <th>Poin</th>
                            <th width="5%">Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $start = 0;
                        foreach ($rk as $rk)
                        {
                            ?>
                            <tr>
                                <td align='center'><?php echo ++$start ?></td>
                                <td><?php echo $rk->nama ?></td>
                                <td><?php echo $rk->kota ?></td>
                                <td align="center"><?php echo number_format($rk->poin,'0','','.') ?></td>
                                <td align="center"><a data-target="#ajax" data-toggle="modal" href="<?php echo base_url().'laporan/detailpoinowner/'.$rk->id_inc?>" class="btn btn-xs purple"><i class="fa fa-binoculars"></i></a></td>
                            </tr>
                            <?php   } ?>
                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ajax" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                
                <span><i class="fa fa-refresh"></i> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>