 
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            Report
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Pembayaran Per Handling</span>
            <i class="fa fa-circle"></i>
        </li>
        <li>
          <span>Detail</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group">
          <a href="<?php echo base_url('laporan/cetakbayarbyhandling/'.$handling)?>" class="btn btn-sm btn-warning" target="_blank"><i class="fa fa-print"></i> Cetak</a>
          <?php echo anchor('laporan/bayarbyhandling','<i class="fa fa-list"></i> Kembali',' class="btn btn-sm blue"');?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php $all=0; $urut=1; foreach($owner as $ro){?>
            <table >
                <tr>
                  <td><b>No Kwitansi </b></td>
                  <td>: <?php echo $ro->no_kwitansi.' ('.$ro->bayar.')'?></td>
                  <td width="100px"></td>
                  <td><b>Owner</b></td>
                  <td>: <?php echo $ro->owner?></td>
                </tr>
                
            </table>
            <?php 
            $listikan=$this->Mkontes->listikanbypeserta($ro->id_inc); 
            // $listbak =$this->Mkontes->listbakbypeserta($ro->id_inc); ?>
          
             <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <td width="5%">No</td>
                        <td>ID ikan</td>
                        <td>Jenis Ikan</td>
                        <td>Ukuran</td>
                        <td>Asal</td>
                        <td>Gender</td>
                        <td>Biaya</td>
                    </tr>
                </thead>

                <tbody>
                    <?php 
                    if(count($listikan)>0){
                    $total=0;
                     $no=1; foreach($listikan as $li){?>
                    
                    <tr>
                        <td align="center"><?= $no; ?></td>
                        <td align="center"><?= $li->no_ikan; ?></td>
                        <td><?= $li->nm_jenis; ?></td>
                        <td><?= $li->ukuran; ?> cm</td>
                        <td><?= $li->asal; ?></td>
                        <td><?= $li->gender; ?></td>
                        <td align="right"><?php echo number_format($li->biaya,'0','','.')?></td>
                    </tr>
                    <?php $total+=$li->biaya;  $no++; }  ?>
                    
                 <?php    } else{ $total+=0;?>
                    <tr>
                       <td align="center" colspan="7">Tidak ada data !</td> 
                    </tr>
                    <?php }  $all+=$total; ?>
                </tbody>
                
                  <tfoot>
                    <tr>
                      <th colspan="6">Sub Total</th>
                      <td align="right"><b><?php echo number_format($total,'0','','.')?></b></td>
                    </tr>
                  <?php if($urut==count($owner)){?>
                    <tr>
                      <th colspan="6">Total</th>
                      <td align="right"><?php echo number_format($all,'0','','.');?></td>
                    </tr>
                  <?php } ?>
                  </tfoot>
                
            </table>

        <?php    $urut++;} ?>
    </div>
</div>