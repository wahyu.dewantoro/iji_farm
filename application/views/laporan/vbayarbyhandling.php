<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            Report
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Pembayaran Per Handling</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <?php echo anchor(site_url('laporan/cetakrekapbayarhandling'), '<i class="fa fa-print"></i> Cetak','class="btn purple btn-sm btn-outline" target="_blank"'); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
          <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Data</div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="tb" >
                    <thead>
                        <tr>
                            <th width ="5%">No</th>
                            <th>Handling</th>
                            <th>Kota</th>
                            <th>Owner</th>
                            <th>Ikan</th>
                   
                            <th>Biaya Ikan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $start = 0;
                        foreach ($rk as $rk)
                        {
                            ?>
                            <tr>
                                <td align='center'><?php echo ++$start ?></td>
                                <td><?= $rk->handling?></td>
                                <td><?= $rk->kota_handling?></td>
                                <td align="center"><?= $rk->owner?> orang</td>
                                <td align="center"><?= $rk->ikan?> ekor</td>
               
                                <td align="right"><?= number_format($rk->biaya_ikan,'0','','.');?></td>
                                <td align="center">
                                    <a href="<?php echo base_url('laporan/cetakbayarbyhandling/'.$rk->id_handling)?>" class="btn btn-xs btn-warning" target="_blank"><i class="fa fa-print"></i> Cetak</a>
                                    <?php echo anchor('laporan/detailbayarhandling/'.$rk->id_handling,'<i class="fa fa-binoculars"></i>','class="btn btn-xs purple"');?>
                                </td>
                            </tr>
                            <?php   } ?>
                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>