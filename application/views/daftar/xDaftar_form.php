        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    Daftar
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Form Pendaftaran</span>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span><?php echo $button ?></span>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-green-haze">
                                    <i class="fa fa-file-text font-green-haze"></i>
                                    <span class="caption-subject bold"> <?php echo $button ?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form action="<?php echo $action; ?>" method="post" class="form-horizontal">
                        	    <div class="form-group   has-success">
                                    <label class="col-md-2 control-label" for="int">Kontes<span class="required"> * </span></label>
                                    <div class="col-md-10">
                                        <input type="hidden" class="form-control" name="ms_kontes_id" id="ms_kontes_id"  value="<?php echo $ms_kontes_id; ?>" />
                                        <input type="text" disabled class="form-control"    value="<?php echo $kontes; ?>" />                                        
                                    </div>
                                </div>
                        	    <div class="form-group   has-success">
                                    <label class="col-md-2 control-label" for="int">Handling<span class="required"> * </span></label>
                                    <div class="col-md-10">
                                        <!-- <input type="text" value="<?php echo $ms_handling_id; ?>" /> -->
                                        <select  required class="form-control select2" name="ms_handling_id" id="ms_handling_id" >
                                            <option value=""></option>
                                            <?php foreach($handling as $rh){?>
                                            <option value="<?php echo $rh->id_inc?>"><?php echo $rh->nama.' / '.$rh->kota?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                        	    <div class="form-group   has-success">
                                    <label class="col-md-2 control-label" for="varchar">Pemilik Ikan <span class="required"> * </span></label>
                                    <div class="col-md-10">
                                        <input type="text" required class="form-control" name="pemilik_ikan" id="pemilik_ikan"  value="<?php echo $pemilik_ikan; ?>" />
                                    </div>
                                </div>
                        	    <div class="form-group   has-success">
                                    <label class="col-md-2 control-label" for="varchar">Kota<span class="required"> * </span></label>
                                    <div class="col-md-10">
                                        <input type="text" required class="form-control" name="kota_pemilik" id="kota_pemilik"  value="<?php echo $kota_pemilik; ?>" />
                                    </div>
                                </div>
                                <?php if(count($bak)>0){?>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Sewa bak</label>
                                </div>
                                <?php foreach($bak as $bak){?>
                                <div class="form-group has-success">
                                    <label class="col-md-2 control-label"><?php echo $bak->nama_bak?></label>
                                    <div class="col-md-10">
                                        <input type="hidden" name="ms_bak_id[]" value="<?php echo $bak->id_inc?>">
                                        <input type="number" name="jumlahbak[]" class="form-control">
                                    </div>
                                </div>
                                <?php } } ?>
                        	   <div class="form-group"><div class="col-md-10 col-md-offset-2"> <input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" /> 
                        	    <button type="submit" class="btn btn-sm blue"><i class="fa fa-save"></i> Simpan</button> 
                        	    <a href="<?php echo site_url('daftar') ?>" class="btn btn-sm red"><i class="fa fa-close"></i> Batal</a> </div></div>
                        	</form>
                        </div>
                </div>