        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    Assessment
                                    <i class="fa fa-circle"></i>
                                </li>
                                
                                <li>
                                    <span><?php echo $button ?></span>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-green-haze">
                                    <i class="fa fa-file-text font-green-haze"></i>
                                    <span class="caption-subject bold"> <?php echo $button ?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <form action="<?php echo $action; ?>" method="post" class="form-horizontal">
                                            <div class="form-group form-md-line-input  has-success">
                                                <input type="hidden" class="form-control" name="ms_kontes_id" id="ms_kontes_id"  value="<?php echo $ms_kontes_id; ?>" />
                                            </div>
                                            <div class="form-group    has-success">
                                                <label class="col-md-4 control-label" for="int">ID Ikan <span class="required"> * </span></label>
                                                <div class="col-md-8">
                                                    <select class="form-control select2" required name="tb_ikan_id" id="tb_ikan_id" >
                                                        <option value=""></option>   
                                                        <?php foreach($no_ikan as $rn){?>
                                                        <option value="<?php echo $rn->id_inc?>"><?php echo $rn->no_ikan?> - <?php echo $rn->nm_ikan.' ['.$rn->range_uk.' cm] '?></option>
                                                        <?php }?> 
                                                    </select>
                                                    <?php echo form_error('tb_ikan_id') ?>
                                                </div>
                                            </div>
                                         
                                            <div class="form-group  t  has-success">
                                                <label class="col-md-4 control-label" for="int">Juara <span class="required"> * </span></label>
                                                <div class="col-md-8">
                                                    <!-- <input type="text"   value="<?php echo $ms_juara_id; ?>" /> -->
                                                    <select class="form-control select2" required  name="ms_juara_id" id="ms_juara_id">
                                                        <option value=""></option>
                                                        <?php foreach($juara as $rj){?>
                                                        <option value="<?php echo $rj->id_inc?>"><?php echo ucwords($rj->nama_juara)?></option>
                                                        <?php }?>
                                                    </select>
                                                    <?php echo form_error('ms_juara_id') ?>
                                                </div>
                                            </div>
                                           <div class="form-group"><div class="col-md-8 col-md-offset-4"> <input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" /> 
                                            <button type="submit" class="btn btn-sm blue"><i class="fa fa-save"></i> Simpan</button> 
                                            <a href="<?php echo site_url('juarakontes') ?>" class="btn btn-sm red"><i class="fa fa-close"></i> Batal</a> </div></div>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                           <div id="reviewikan"></div>
                                    </div>
                                </div>


                            
                            </div>
                    </div>
 <script type="text/javascript">
    $('#tb_ikan_id').change(function() {
        $.ajax ({
                type: 'POST',
                url: '<?php echo base_url()."juarakontes/priviewikan"?>',
                data: { id:  $(this).val() },
                success : function(data) {
                    // alert(data);    
                    $('#reviewikan').html(data);
                }
            });
    });
 </script>