<div class="page-bar">
    <h1 class="page-title"> <?= $nama_kontes?>
        <!-- <small>fixed footer option</small> -->
    </h1>
</div>
<div class="row  widget-row">
	 <div class="col-md-3 col-sm-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white  margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading">Total Ikan</h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-green icon-layers"></i>
                <div class="widget-thumb-body">
                    <span class="widget-thumb-body-stat" data-counter="counterup" data-value="7,644"><?php echo $ikan?></span>
                    <span class="widget-thumb-subtitle">Ekor</span>
                </div>
            </div>
        </div>
        <!-- END WIDGET THUMB -->
    </div>
     <div class="col-md-3 col-sm-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white  margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading">Total Peserta</h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-red icon-users"></i>
                <div class="widget-thumb-body">
                    <span class="widget-thumb-body-stat" data-counter="counterup" data-value="1,293"><?php if(!empty($peserta)){echo $peserta;}else{echo "0";}?></span>
                    <span class="widget-thumb-subtitle">Orang</span>
                </div>
            </div>
        </div>
        <!-- END WIDGET THUMB -->
    </div>
    <div class="col-md-3 col-sm-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white  margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading">Peserta Terbanyak <?php if(!empty($peserta_terbanyak)){echo ' : '.$peserta_terbanyak->nama.' / '.$peserta_terbanyak->kota;}?></h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-purple fa fa-black-tie"></i>
                <div class="widget-thumb-body">
                     <span class="widget-thumb-body-stat" data-counter="counterup" data-value="815"><?php if(count($peserta_terbanyak)>0){echo $peserta_terbanyak->ikan;}else{echo "0";}?></span>
                     <span class="widget-thumb-subtitle">Ikan</span>
                </div>
            </div>
        </div>
     </div>
      <div class="col-md-3 col-sm-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white  margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Kota Peserta Terbanyak  <?php if(!empty($kota_terbanyak)){ echo ': '. $kota_terbanyak->kota;}?></h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-blue icon-bar-chart"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="5,071"><?php if(!empty($kota_terbanyak)){ echo $kota_terbanyak->ikan;}else{echo "0";}?></span>
                        <span class="widget-thumb-subtitle">Ekor</span>
                        
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
       
</div>    
<div class="row   widget-row">
     <div class="col-md-3 col-sm-3">
        <!-- BEGIN WIDGET THUMB -->
        <div class="widget-thumb widget-bg-color-white  margin-bottom-20 bordered">
            <h4 class="widget-thumb-heading">Handling Terbanyak <?php if(!empty($hb)){echo ' : '.$hb->nama.' / '.$hb->kota;}?></h4>
            <div class="widget-thumb-wrap">
                <i class="widget-thumb-icon bg-purple fa fa-black-tie"></i>
                <div class="widget-thumb-body">
                     <span class="widget-thumb-body-stat" data-counter="counterup" data-value="815"><?php if(count($hb)>0){echo $hb->ikan;}else{echo "0";}?></span>
                     <span class="widget-thumb-subtitle">Ekor</span>
                </div>
            </div>
        </div>
     </div>
</div>                    
