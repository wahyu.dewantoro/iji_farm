<form class="form-horizontal" method="post" action="<?php echo base_url().'pendaftaran/proseseditikan'?>" enctype="multipart/form-data">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Ikan</h4>
</div>
<div class="modal-body">

    <input type="hidden" name="id_inc" value="<?php echo $rk->id_inc?>" class="form-control" required>
    <input type="hidden" name="tb_peserta_id" value="<?php echo $rk->tb_peserta_id?>" class="form-control" required>
    
    <div class="form-group">
        <label class="col-md-3">Jenis Ikan <small>*</small></label>
        <div class="col-md-9">
            <select class="form-control select2" name="ms_kat_id" required>
                <option value=""></option>
                <?php foreach($ikan as $ikan){?>
                <option <?php if($ikan->id_inc==$rk->ms_kat_id){echo "selected";}?> value="<?php echo $ikan->id_inc?>"><?php echo $ikan->nm_ikan?></option>
                <?php }?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3">Ukuran <small>*</small></label>
        <div class="col-md-9">
            <div class="input-group">
                <input type="number" name="ukuran" value="<?php echo $rk->ukuran?>" class="form-control" required>    
                 <span class="input-group-addon" id="sizing-addon1">cm</span>
            </div>
            
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3">Gender <small>*</small></label>
        <div class="col-md-9">
            
                <label class="mt-radio">
                    <input type="radio" name="gender" id="gender" value="Male"  <?php if($rk->gender=='Male'){echo "checked";}?> > Male
                    <span></span>
                </label>
                <label class="mt-radio">
                    <input type="radio" name="gender" id="gender" value="Female" <?php if($rk->gender=='Female'){echo "checked";}?>  > Female
                    <span></span>
                </label>
        </div>
    </div>    
    <div class="form-group">
        <label class="col-md-3">Asal <small>*</small></label>
        <div class="col-md-9">
            
                <label class="mt-radio">
                    <input type="radio" name="asal" id="asal" value="Lokal"  <?php if($rk->asal=='Lokal'){echo "checked";}?> > Lokal
                    <span></span>
                </label>
                <label class="mt-radio">
                    <input type="radio" name="asal" id="asal" value="Import" <?php if($rk->asal=='Import'){echo "checked";}?>  > Import
                    <span></span>
                </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3">Gambar <small>*</small></label>
        <div class="col-md-9">
            <div class="fileinput fileinput-new" data-provides="fileinput">
            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 120px; height: 160px;"><img id="blah2"  width="100px"  src="<?php echo base_url().$rk->gambar_ikan ?>" /> </div>
                 <small> <span   class="fileinput-filename"></span></small>
                <div>
                    <span class="btn-file">
                        <input type="file" id="imgInp2" name="gambar_ikan"> </span>
                </div>
            </div>
            <input type="hidden" name="lawas" value="<?php echo $rk->gambar_ikan?>">
             
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-9 col-md-offset-3">
            <button class="btn btn-sm btn--success" type="submit"><i class="fa fa-save"></i> Submit</button> 
        </div>
    </div>
                                        
</div>

</form>

 <script type="text/javascript">
     function readURL(input) {

          if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
              $('#blah2').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
          }else{
            
              $('#blah2').attr('src',"<?php echo base_url().$rk->gambar_ikan ?>");
            
          }
    }

$("#imgInp2").change(function() {
        // alert('asdf');
  readURL(this);
});
 </script>
