<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title> Madiun Young Koi Show</title>

  <!-- Tell the browser to be responsive to screen width -->

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.7 -->

  <link rel="stylesheet" href="<?= base_url().'lte/' ?>bower_components/bootstrap/dist/css/bootstrap.min.css">

  <!-- Font Awesome -->

  <!-- <link rel="stylesheet" href="<?= base_url().'lte/' ?>bower_components/font-awesome/css/font-awesome.min.css"> -->

  <link href="<?= base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

  <!-- Ionicons -->

  <link rel="stylesheet" href="<?= base_url().'lte/' ?>bower_components/Ionicons/css/ionicons.min.css">

  <!-- Theme style -->

  <link rel="stylesheet" href="<?= base_url().'lte/' ?>dist/css/AdminLTE.min.css">

  <!-- AdminLTE Skins. Choose a skin from the css/skins

       folder instead of downloading all of them to reduce the load. -->

  <link rel="stylesheet" href="<?= base_url().'lte/' ?>dist/css/skins/_all-skins.min.css">



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <![endif]-->



  <!-- Google Font -->

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <style type="text/css">

h4{
  text-align: center;
  /*ketebalan font*/
  /*font-weight: 300;*/
}
 
.tulisan_login{
  text-align: center;
  /*membuat semua huruf menjadi kapital*/
  text-transform: uppercase;
}
 
.kotak_login{
  width: 350px;
  background: white;
  /*meletakkan form ke tengah*/
  margin-left:  auto;
  margin-right: auto;
  padding: 30px;
}
 
label{
  font-size: 11pt;
}
 
.form_login{
  /*membuat lebar form penuh*/
  box-sizing   : border-box;
  width        : 100%;
  padding      : 10px;
  font-size    : 11pt;
  margin-bottom: 20px;
}
 
.tombol_login{
  background: #46de4b;
  color: white;
  font-size: 11pt;
  width: 100%;
  border: none;
  border-radius: 3px;
  padding: 10px 20px;
}
 
.link{
  color: blue;
  text-decoration: none;
  font-size: 10pt;
}

  </style>

  <!-- jQuery 3 -->

<script src="<?= base_url().'lte/' ?>bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url().'lte/' ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- SlimScroll -->

<script src="<?= base_url().'lte/' ?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->

<script src="<?= base_url().'lte/' ?>bower_components/fastclick/lib/fastclick.js"></script>

<!-- AdminLTE App -->

<script src="<?= base_url().'lte/' ?>dist/js/adminlte.min.js"></script>

</head>

<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->

<body class="hold-transition skin-blue layout-top-nav">

<div class="wrapper">



  <header class="main-header">

    <nav class="navbar navbar-static-top">

      <div class="container">

        <div class="navbar-header">

          <a href="<?=  base_url().'summary' ?>" class="navbar-brand">MUKC</a>

          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">

            <i class="fa fa-align-justify"></i>

          </button>

        </div>

        <div class="collapse navbar-collapse pull-right" id="navbar-collapse">

            <ul class="nav navbar-nav">
            <li ><a  href="<?php echo base_url().'auth'?>"><i class='fa fa-lock'></i> <span class='title'>Login / Sign Up</span></a></li>

          </ul>

        </div>

      </div>

      

    </nav>

  </header>

  <!-- Full Width Column -->

  <div class="content-wrapper">

    <div class="container">
 

      <!-- Main content -->

      <section class="content">
          <h4>Selamat Datang <br>1<sup>ST</sup> Madiun Kharismatik Young Koi Show</h4>

  <div class="kotak_login">
    <!-- <p class="tulisan_login">Silahkan login</p> -->
    <form action="<?= $action ?>" method="post">
      <label>Username</label>
      <input type="text" type="text" autocomplete="off" placeholder="Username" name="username" class="form_login" placeholder="Username">
      <label>Password</label>
      <input type="password" autocomplete="off" placeholder="Password" name="password" class="form_login" placeholder="Password ..">
      <input type="submit" class="tombol_login" value="LOGIN">
      <br/>
      <br/>
      <center>
        Belum punya akun? <a class="link" href="<?= base_url().'auth/handling'?>">Daftar</a>
      </center>
    </form>
            <?= $this->session->flashdata('notif')?>
            <?= $this->session->flashdata('msg')?>
  </div>
      </section>

      <!-- /.content -->

    </div>

    <!-- /.container -->

  </div>

  <!-- /.content-wrapper -->

  <footer class="main-footer">

    <div class="container">

      

      <strong>&copy; 2018   <a href="#">W.O.B. Code</a>.</strong>  

    </div>

    <!-- /.container -->

  </footer>

</div>

<!-- ./wrapper -->





<!-- AdminLTE for demo purposes -->

<script src="<?= base_url().'lte/' ?>dist/js/demo.js"></script>

<script type="text/javascript">

  $( document ).ready(function() {

    var heights = $(".panel").map(function() {

        return $(this).height();

    }).get(),



    maxHeight = Math.max.apply(null, heights);



    $(".panel").height(maxHeight);

});



</script>

</body>





<!-- Mirrored from adminlte.io/themes/AdminLTE/pages/layout/top-nav.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 14 Oct 2018 08:16:10 GMT -->

</html>

