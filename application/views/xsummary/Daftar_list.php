<style type="text/css">
    .responsive {
        width: auto;
          /*max-width: */
          height: 200px;
        }
</style>
<div class="row">
  
    <div class="col-md-12">
            <div class="box box-success">
            <div class="box-header with-border">
                    
                    Data Tagihan
                    
                    <div class="pull-right">
                        <?= anchor('pendaftaran/ownerbyhandling','<i class="fa fa-user"></i> Tambah Owner','class="btn btn-xs btn-success"')?>
                        
                    </div>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                           <table class="table table-striped table-bordered table-hover" id="tb" >

            <thead>

                <tr>

                    <th width="5%">No</th>

                    <th>Kode</th>
                    <th>Owner</th>
                    <th>Ikan</th>
                    <th>Biaya</th>
                  
                </tr>

            </thead>

        <tbody>

            <?php

            $start = 0;

            $jum=0;
            $bb=0;

            foreach ($daftar_data as $daftar)

            {

                ?>

                <tr>

            <td align='center'><?php echo ++$start ?></td>

            <td><?= anchor(site_url('pendaftaran/daftarfromkontes/'.$daftar->kh),$daftar->kode);  ?></td>
            <td><?= $daftar->pemilik_ikan?></td>

            <td><?= $daftar->jum_ikan?> ekor</td>           
            <td align="right"><?= number_format($daftar->biaya,'0','','.');?></td>
            

            </tr>

                <?php

                $bb+=$daftar->biaya;

                $jum+=$daftar->jum_ikan;

            }

            ?>

            </tbody>

            <tfoot>

                <tr>

                    <td colspan="3">Total  </td>

                    <td colspan="1" align="center"><?php echo $jum;?></td>
                    <td align="right"><?= number_format($bb,'0','','.');?></td>

                </tr>

            </tfoot>

        </table>

                    </div>
                </div>
            </div>
    </div>
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                    <div class="caption">
                    Preview Ikan
                    </div>
                </div>
                <div class="box-body">
                    <?php  $no=1; foreach($pp as $dd){ ?>
                         <div class="col-xs-12 col-sm-6 col-md-2 ">
                            <div class="col-md-12">
                                <div style=" text-align: center;">
                                    <a  href="<?php echo base_url().'daftar/viewikan/'.$dd->id_inc?>" data-target="#ajax<?= $no ?>" data-toggle="modal">
                                        <!-- <span class="notify-badge"><?php echo ucwords($dd->prize)?></span>        -->
                                        <img class="img-fluid responsive"  src="<?php echo base_url().$dd->gambar_ikan;?>" />
                                    </a>
                                </div>
                                <div style="font-size:10pt;  text-align: center;">
                                <a class="btn btn-xs btn-primary" href="<?php echo base_url().'pendaftaran/formeditikan/'.$dd->id_inc?>" data-target="#ajaxb<?= $no ?>" data-toggle="modal"><i class="fa fa-edit"></i> </a>
                                <?php  echo anchor('pendaftaran/hapusikan/'.$dd->id_inc,'<i class="fa fa-close"></i>','class="btn btn-xs btn-danger" onclick="return confirm(\'Apakah anda yakin?\')"');?><br>
                                <span ><b><?= $dd->ukuran_cm.' cm';?></b></span><br>
                                <span ><b><?= $dd->nm_jenis;?></b></span><br>
                                <span ><b><?= $dd->gender.','.$dd->asal;?></b></span><br>
                                    <b><u>Owner</u></b>
                                    <p><?php echo ucwords($dd->owner);?></p>
                                    
                               </div>
                            </div>
                        </div>
                    <?php $no++; } ?>
                </div>
            </div>
    </div>
</div>

<?php for($d=1;$d<=count($pp);$d++){ ?>
<div class="modal fade" id="ajax<?= $d ?>" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <span><i class="fa fa-refresh"></i> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ajaxb<?= $d ?>" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <span><i class="fa fa-refresh"></i> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<!-- <div class="modal fade" id="ajax" role="basic" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-body">

                <span><i class="fa fa-refresh"></i> &nbsp;&nbsp;Loading... </span>

            </div>

        </div>

    </div>

</div>
  -->