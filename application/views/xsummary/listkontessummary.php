<div class="row">
    <div class="col-xs-12">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-green"></i>
                    <span class="caption-subject font-green bold uppercase">Kontes</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                	<?php 
                	$warna=array('default','info','danger','success','warning');

                	$no=1;
                	foreach($data as $rk){ ?>
                        <a href="<?php echo base_url().'summary/prosespilihkontes/'.$rk->id_inc?>">
                		<div class="col-lg-4 col-md-6 col-xs-12">
                				<div class="portlet box green ">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i> <?php echo $rk->nama_kontes?> </div>
                                
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-md-6  col-xs-12">
                                                <img style="height:120px;max-width:80px;width: expression(this.width > 80 ? 80: true);"  src="<?php echo base_url().$rk->logo_kontes?>">
                                            </div>
                                            <div class="col-md-6 col-xs-12">
                                                <div class="table-responsive">
                                                    
                                                
                                                <table class="table" style="font-size:10pt">
                                                    <tbody>
                                                        <tr>
                                                            <td><?= $rk->mulai.' sd '.$rk->selesai?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><?= $rk->lokasi?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Reg :<?= $rk->pendaftaran?></td>
                                                        </tr>
                                                         
                                                         
                                                    </tbody>
                                                </table>        
                                                </div>
                                            </div>
                                        </div>
                                    	
                                    </div>
                                </div>
                		</div>
	                       </a>
                	<?php $no++;} ?>
 
                </div>
            </div>
        </div>
    </div>
</div>


		

