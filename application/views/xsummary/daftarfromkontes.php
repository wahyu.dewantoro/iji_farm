<style type="text/css">
    .responsive {
        width: auto;
          /*max-width: */
          height: 200px;
        }
</style>
<div class="row">
    <div class="col-md-4">
    <form method="post" action="<?php echo base_url().'pendaftaran/prosessimpanikan'?>" enctype="multipart/form-data">
        <input type="hidden" name="ms_peserta_id" value="<?php echo $rk->ms_peserta_id?>">  
        <input type="hidden" name="ms_handling_id" value="<?php echo $rk->ms_handling_id?>">  
           <div class="box box-warning">
            <div class="box-header with-border">
                    <div class="caption">
                        Handling :<?= $rk->handling?> / owner : <?= $rk->peserta?>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12" style="font-size:12px">
                            <div class="" style="margin: auto; width:50%">
                                <img id="blah" alt="your image"  height="120px" max-width="100%" src="<?= base_url().'noimage.png'?>" >
                                <input type="file" required accept="image/*" id="imgInp" name="gambar_ikan"> </span>
                            </div>
                            <div class="form-group">
                                <label>Variety</label>
                                <div class="">
                                    <select name="ms_kat_id" class="form-control" required>
                                      <option value="">pilih</option>

                                        <?php foreach($kat as $kat){?>
                                        <optgroup label="Kategori <?= $kat->kat_ikan ?>">
                                            <?php foreach($ikan as $rr){ if($rr->kat_ikan==$kat->kat_ikan){?>
                                                  <option value="<?php echo $rr->id_inc?>"><?php echo $rr->nm_ikan?></option>
                                            <?php } } ?>

                                        </optgroup>
                                        <?php } ?>

                                      
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Size</label>
                                <div class="input-group">
                                  <input type="number" name="ukuran" class="form-control" required>
                                  <span class="input-group-addon">cm</span>
                                </div>
                                
                            </div>
                            <div class="form-group">
                                <label>Breeder</label>
                                <div class="">
                                    <select class="form-control" name="asal" required>
                                        <option value="Lokal">Lokal</option>
                                        <option value="Import">Import</option>
                                    </select>
                                </div>
                            </div>
                             <div class="form-group">
                                <label>Gender</label>
                                <div class="">
                                    <select class="form-control" name="gender" required>
                                        <option value="Female">Female</option>
                                        <option value="Male">Male</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2 text-right">
                                    <span id="count"><button type="submit"  class="btn btn-sm btn-success "><i class="fa fa-save"></i> Submit</button></span> 
                                    <input type="hidden" id="vc" value="1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-8">
            <div class="box box-success">
            <div class="box-header with-border">
                    <div class="caption">
                    Data Ikan
                    </div>
                </div>
                <div class="box-body">
                    <?php  $no=1; foreach($listikan as $dd){ ?>
                         <div class="col-xs-12 col-sm-6 col-md-3 ">
                            <div class="col-md-12">
                                <div style=" text-align: center;">
                                    <a  href="<?php echo base_url().'daftar/viewikan/'.$dd->id_inc?>" data-target="#ajax<?= $no ?>" data-toggle="modal">
                                        <!-- <span class="notify-badge"><?php echo ucwords($dd->prize)?></span>        -->
                                        <img class="img-fluid responsive"  src="<?php echo base_url().$dd->gambar_ikan;?>" />
                                    </a>
                                </div>
                                <div style="font-size:10pt;  text-align: center;">
                                <a class="btn btn-xs btn-primary" href="<?php echo base_url().'pendaftaran/formeditikan/'.$dd->id_inc?>" data-target="#ajaxb<?= $no ?>" data-toggle="modal"><i class="fa fa-edit"></i> </a>
                                <?php  echo anchor('pendaftaran/hapusikan/'.$dd->id_inc,'<i class="fa fa-close"></i>','class="btn btn-xs btn-danger" onclick="return confirm(\'Apakah anda yakin?\')"');?><br>
                                <span ><b><?= $dd->ukuran_cm.' cm';?></b></span><br>
                                <span ><b><?= $dd->nm_jenis;?></b></span><br>
                                <span ><b><?= $dd->gender.','.$dd->asal;?></b></span><br>
                                    <b><u>Owner</u></b>
                                    <p><?php echo ucwords($dd->owner);?></p>
                                    
                               </div>
                            </div>
                        </div>
                    <?php $no++; } ?>
                </div>
            </div>
    </div>
</div>


<?php for($d=1;$d<=count($listikan);$d++){ ?>
<div class="modal fade" id="ajax<?= $d ?>" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <span><i class="fa fa-refresh"></i> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ajaxb<?= $d ?>" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <span><i class="fa fa-refresh"></i> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>
<?php } ?>



 <script type="text/javascript">

     function readURL(input) {



          if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = function(e) {

              $('#blah').attr('src', e.target.result);

            }

            reader.readAsDataURL(input.files[0]);

          }else{

            

              $('#blah').attr('src',"<?= base_url().'noimage.png' ?>");

            

          }

    }



$("#imgInp").change(function() {

  readURL(this);

});

 </script>

