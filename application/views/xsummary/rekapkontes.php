

<div class="row">

    <div class="col-md-6">

        <div class="portlet box green ">

            <div class="portlet-title">

                <div class="caption">

                    <i class="fa fa-sort-numeric-asc"></i> <b>Rekapitulasi</b></div>

        

            </div>

            <div class="portlet-body">

                <table class="table table-striped   table-striped"   >

                    <tbody>

                        <tr>

                            <td width="35%">Total Ikan</td>

                            <td width="1%">:</td>

                            <td><?php echo $ikan?> ekor</td>

                        </tr>

                        <tr>

                            <td>Total Peserta</td>

                            <td>:</td>

                            <td><?php if(!empty($peserta)){echo $peserta;}else{echo "0";}?> orang</td>

                        </tr>

                        <tr>

                            <td>Peserta Terbanyak</td>

                            <td>:</td>

                            <td><?php if(!empty($peserta_terbanyak->nama)){echo $peserta_terbanyak->nama.' / '.$peserta_terbanyak->kota;}?> <?php if(!empty($peserta_terbanyak->ikan)){echo '[ '.$peserta_terbanyak->ikan.' ekor ] ';}else{echo "-";}?></td>

                        </tr>

                      <!--   <tr>

                            <td>Kota Peserta Terbanyak</td>

                            <td>:</td>

                            <td><?php if(!empty($kota_terbanyak)){ echo $kota_terbanyak->kota;}?> <?php if(!empty($kota_terbanyak)){ echo '[ '.$kota_terbanyak->ikan.' ekor ]';}else{ echo "-";}?></td>

                        </tr> -->

                        <tr>

                            <td>Handling Terbanyak</td>

                            <td>:</td>

                            <td><?php if(!empty($hb->nama)){echo  $hb->nama.' / '.$hb->kota;}?> <?php if($hb->ikan > 0 ){echo '[ '.$hb->ikan.' ekor ]';}else{ echo "-";}?></td>

                        </tr>

                    </tbody>    

                </table>

            </div>

        </div>

        

    </div>

	<!-- <div class="col-md-6">

		<div class="portlet box green ">

            <div class="portlet-title">

                <div class="caption">

                    <i class="fa fa-sort-numeric-asc"></i> <b>Perolehan Poin </b></div>

        

            </div>

            <div class="portlet-body">

            	 <table class="table table-striped table-bordered table-striped"   >

                    <thead>

                        <tr>

                            <th width ="5%">No</th>

                            <th>Owner</th>

                            <th>Kota</th>

                            <th>Poin</th>

                            

                        </tr>

                    </thead>

                    <tbody>

                        <?php

                        $start = 0;

                        if(count($rk)>0){ foreach ($rk as $rk)

                        {

                            ?>

                            <tr>

                                <td align='center'><?php echo ++$start ?></td>

                                <td><?php echo $rk->nama ?></td>

                                <td><?php echo  ucwords(strtolower($rk->kota)) ?></td>

                                <td align="center"><?php echo number_format($rk->poin,'0','','.') ?></td>

                               

                            </tr>

                            <?php   } }else{ ?>

                            <tr>

                                <td colspan="4">Belum ada data</td>

                            </tr>

                        <?php } ?>

                        </tbody>

                </table>

            </div>

        </div>

	</div>

	 -->

</div>