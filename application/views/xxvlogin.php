<!DOCTYPE html>



<html lang="en">

    <!--<![endif]-->

    <!-- BEGIN HEAD -->



    <head>

        <meta charset="utf-8" />

        <title>Madiun Young Koi Show</title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta content="width=device-width, initial-scale=1" name="viewport" />

        <meta content="Preview page of Metronic Admin Theme #1 for " name="description" />

        <meta content="" name="author" />

        <link href="<?= base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url()?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url()?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?= base_url()?>assets/pages/css/login.min.css" rel="stylesheet" type="text/css" />



        <link rel="shortcut icon" href="favicon.ico" /> 



        <script src="<?= base_url()?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

        <script src="<?= base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        </head>
    <body class=" login">
        <div class="logo">
           <img src="<?= base_url()?>logo.jpg" alt="" width="120px" />
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="<?= $action ?>" method="post">
                <div class="form-group ">
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
                <div class="form-actions">
                    <button type="submit" name="submit" class="btn btn-primary"><i class="fa fa-sign-in"></i> Login</button>                    
                    <?= anchor('auth/handling',"<i class='fa fa-user'></i> Sign Up","class='btn btn-warning'");?>        
                </div>

            </form>
            
            <?= $this->session->flashdata('notif')?>
            <?= $this->session->flashdata('msg')?>

        </div>

    </body>

</html>