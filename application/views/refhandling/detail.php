<div class="page-bar">
    <ul class="page-breadcrumb">
        <li> Participants <i class="fa fa-circle"></i></li>
        <li> <span>Handling</span> <i class="fa fa-circle"></i></li>
        <li> <span><?php echo $hand->nama?></span> <i class="fa fa-circle"></i> </li>
        <li> <span><?php echo $hand->kota?></span> </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <?php  if($uh==false){ echo anchor(site_url('refhandling'), '<i class="fa fa-list"></i> List','class="btn purple btn-sm btn-outline"');} ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption"> <i class="fa fa-edit"></i>Tambah Owner</div>
            </div>
            <div class="portlet-body">
                <form class="form-horizontal" method="post" action="<?php echo base_url().'refhandling/prosesaddowner'?>"> 
                    <div class="form-group has-success">
                        <input type="hidden" name="ms_handling_id" value="<?php echo $hand->id_inc?>">
                        <label class="col-md-3 control-label">Owner <span>*</span></label>
                        <div class="col-md-9">
                            <input type="text" name="nama" id="nama" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group has-success">
                        <label class="col-md-3 control-label">Kota <span>*</span></label>
                        <div class="col-md-9">
                            <input type="text" name="kota" id="kota" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button class="btn btn-sm btn-info" type="submit"><i class="fa fa-save"></i> Simpan</button>
                            <button class="btn btn-sm btn-warning" type="reset"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-users"></i> Owner
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Nama</th>
                            <th>Kota</th>
                            <th width="30%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $nn=1; foreach($owner as $ow){?>
                        <tr>
                            <td align="center"><?php echo $nn?></td>
                            <td><?php echo  anchor('daftar/daftarfromkontes/'.$ow->id_inc, $ow->nama); ?></td>
                            <td><?php echo $ow->kota?></td>
                            <td align="center">
                                <?php echo anchor('daftar/daftarfromkontes/'.$ow->id_inc,'<i class="fa fa-plus"></i> Tambah ikan','class="btn btn-xs btn-outline purple"'); 
                                 if($this->session->userdata('wob_pengguna')==$ow->pengguna_id or $this->session->userdata('wob_role')==1){echo anchor('refhandling/hapushandlingowner/'.$ow->id_inc.'/'.$ow->ms_handling_id,'<i class="fa fa-trash"></i> hapus','class="btn btn-xs btn-outline red" onclick="return confirm(\'Apakah anda yakin hapus ?\')"');}?>
                            </td>
                        </tr>
                        <?php $nn++; }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
     // Example #2
        var countries = new Bloodhound({
          datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          limit: 10,
          prefetch: {
            url: '<?php echo base_url()."jsonkota.json" ?>',
            filter: function(list) {
              return $.map(list, function(country) { return { name: country }; });
            }
          }
        });
        countries.initialize();        

        if (App.isRTL()) {
          $('#kota').attr("dir", "rtl");  
        } 

        $('#kota').typeahead(null, {

          name: 'kota',

          displayKey: 'name',

          hint: (App.isRTL() ? false : true),

          source: countries.ttAdapter()

        });





        var ownerikan = new Bloodhound({

          datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },

          queryTokenizer: Bloodhound.tokenizers.whitespace,

          limit: 10,

          prefetch: {

            url: '<?php echo base_url()."refhandling/jsonowner" ?>',

            filter: function(list) {

              return $.map(list, function(ikanowner) { return { name: ikanowner }; });

            }

          }

        });

 

        ownerikan.initialize();

         

        if (App.isRTL()) {

          $('#nama').attr("dir", "rtl");  

        } 

        $('#nama').typeahead(null, {

          name: 'nama',

          displayKey: 'name',

          hint: (App.isRTL() ? false : true),

          source: ownerikan.ttAdapter()

        });

</script>