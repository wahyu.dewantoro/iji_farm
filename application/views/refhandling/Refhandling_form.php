        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    Bank data
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Handling</span>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span><?php echo $button ?></span>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-green-haze">
                                    <i class="fa fa-file-text font-green-haze"></i>
                                    <span class="caption-subject bold"> <?php echo $button ?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
        <form action="<?php echo $action; ?>" method="post" class="form-horizontal">
	    <div class="form-group form-md-line-input  has-success">
            <label class="col-md-2 control-label" for="varchar">Nama <span class="required"> * </span></label>
            <div class="col-md-10">
                <input type="text" class="form-control" name="nama" id="nama"  value="<?php echo $nama; ?>" />
                <?php echo form_error('nama') ?>
            </div>
        </div>
	    <div class="form-group form-md-line-input  has-success">
            <label class="col-md-2 control-label" for="varchar">Kota <span class="required"> * </span></label>
            <div class="col-md-10">
                <input type="text" class="form-control" name="kota" id="kota"  value="<?php echo $kota; ?>" />
                <?php echo form_error('kota') ?>
            </div>
        </div>
	    <div class="form-group form-md-line-input  has-success">
            <label class="col-md-2 control-label" for="varchar">Telp  </label>
            <div class="col-md-10">
                <input type="text" class="form-control" name="telp" id="telp"  value="<?php echo $telp; ?>" />
                <?php echo form_error('telp') ?>
            </div>
        </div>
	   <div class="form-group"><div class="col-md-10 col-md-offset-2"> <input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" /> 
	    <button type="submit" class="btn btn-sm blue"><i class="fa fa-save"></i> Simpan</button> 
	    <a href="<?php echo site_url('refhandling') ?>" class="btn btn-sm red"><i class="fa fa-close"></i> Batal</a> </div></div>
	</form>
        </div>
</div>